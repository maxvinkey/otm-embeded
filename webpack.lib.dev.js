const merge = require('webpack-merge');
const common = require('./webpack.lib.common.js');

module.exports = merge(common, {
   mode: 'development',
   devtool: 'inline-source-map',
   devServer: {
       historyApiFallback: true,
       contentBase: './dist'
    }
});
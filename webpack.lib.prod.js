const merge = require('webpack-merge');
const common = require('./webpack.lib.common.js');

 module.exports = merge(common, {
   mode: 'production',
 });
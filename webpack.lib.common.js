const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var basePath = __dirname;
const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
    node: {
        fs: "empty"
    },
    context: path.join(basePath, 'src'),
    entry: {
        app: './mini.js'
    },
    output: {
        filename: 'mini.js',
        path: path.resolve(__dirname, './dist'),
        library: 'mini',
        libraryTarget:'window'
    },
    module: {
        rules: [{
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader', // or MiniCssExtractPlugin.loader
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 1
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                ],
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                // whatwg-fetch use Promsie which IE11 doesn't support
                test: /\.js$/,
                include: [/whatwg-.*/],
                loader: 'babel-loader'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=./fontsAndIcons/[hash].[ext]'
            },
            {
                test: /\.(ttf|eot|svg|otf)(\?[\s\S]+)?$/,
                use: 'file-loader?name=fontsAndIcons/[name].[ext]'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'file-loader?name=images/[name].[ext]',
                    'image-webpack-loader?bypassOnDebug'
                ]
            },
            {
                test: /\.modernizrrc.js$/,
                loader: "modernizr"
            },
            {
                test: /\.modernizr(\.json)?$/,
                loader: "modernizr!json"
            }
        ]
    },
    resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, 'src', "modernizrrc.js")
        }
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new CopyPlugin([{
                from: './json/**/*',
                to: './'
            },
            {
                from: './images/**/*',
                to: './'
            }
        ]),
    ]
};
import mapboxgl from 'mapbox-gl';
import {
    CategSet
} from "./lib/categset";
import {
    getFeatureLayerName,
    getMapBoxGLHashString,
    is_touch_device
} from "./lib/utils";
import TileLayer from './tileLayer';
import ObjectsList from "./objectsList";
import ObjectCard from "./objectCard";
import IziRouteCard from "./iziRouteCard";
import debounce from 'lodash.debounce';
import {
    getShortFeature,
    getTours,
    getTour
} from "./lib/utils.promise";
import CategorsMenu from "./categorsMenu";
import Directions from "./directions";
import RoutesList from "./routesList";
import Isochrones from "./isochrones";
import BasemapsMenu from "./basemapsMenu";
import LayersState from "./layersState";
import {JL} from 'jsnlog';
import {
    config,
    sendUserEvent,
    Layouts
} from "./config";
import {
    layerGroup
} from 'leaflet';
import {
    setLayout,
    showMapSideBar,
    resizeLayout
} from "./layout";
import {global} from "./global";


function loadMap(map) {

    let categors = new CategSet();
    let categorsMenu = new CategorsMenu();

    if (!config.only_base_map) {
        categors.fromString(config.getActiveTags());
        categorsMenu.show(categors);
    }else{
        $('.routesNavigation').css('display', 'none');
        $('.mapSideBarCollapse').css('display', 'none');
    }

    let tileLayer = new TileLayer(map, categors.toString());
    let layersState = new LayersState(map);
    let objectList = undefined;
    let routeList = undefined;

    function getLists() {
        if (!objectList && !routeList) {
            objectList = new ObjectsList(map);
            routeList = new RoutesList(map);

            function addObjectsToLists() {
                objectList.addObjects();
                let bounds = map.getBounds();
                getTours(bounds).then(function (tours) {
                    routeList.addObjects(tours, {});
                });
            }
            addObjectsToLists(); //это при первоначальной загрузке списков, происходит по клику на кнопку "Куда сходить",
            //вызывается однокоратно!! Поэтому дальнейший вызов по window.tileLayer.on('changed' оставляем.
            routeList.on('click', (data_id) => {
                getTour(data_id).then(function (tour) {
                    showRouteCard(tour);
                    layersState.addRoute(tour.geojson);
                });
            });
            tileLayer.on('changed', () => {
                addObjectsToLists();
            });
            objectList.on('click', (item) => {
                if (item.mainkind) {
                    queryAndShowObjectCard(item.data_id, item.mainkind);
                } else {
                    let clusterId = item.data_id;
                    let clusterCategors = categors.toString();
                    objectList.getClusterByID(clusterId, clusterCategors).then(function (cluster) {
                        let features = objectList.getFeatures();
                        for (let i = 0; i < features.length; i++) {
                            if (clusterId == features[i].id) {
                                map.fitBounds([
                                    [
                                        cluster.properties.minx,
                                        cluster.properties.miny
                                    ],
                                    [
                                        cluster.properties.maxx,
                                        cluster.properties.maxy
                                    ]
                                ]);
                                break;
                            }
                        }
                    });
                }
            });
            objectList.on('mouseenter', (feature) => {
                if (hoveredFeature) {
                    map.setFeatureState(hoveredFeature, {
                        hover: false
                    });
                } else {
                    map.setFeatureState(feature, {
                        hover: true
                    });
                    hoveredFeature = feature;
                }
            }).on('mouseleave', (feature) => {
                if (hoveredFeature && $("#list-content").is(":visible")) {
                    map.setFeatureState(hoveredFeature, {
                        hover: false
                    });
                    hoveredFeature = null;
                }
            });
        } else {
            setTimeout(function () {
                objectList.updateScrollbar();
                routeList.updateScrollbar();
            }, 1000);
        }
    }

    if (config.getWidth() > 768 && !config.minimap) {
        setLayout(Layouts.MAP_WITH_SIDEBAR);
        getLists();
    } else {
        $('#profile-tab').on('click', function hideObjectsFilter() {
            $('.objectsFilter').css('display', 'none');
            $('.objectsFilter').off('click', hideObjectsFilter);
        });

        $('#home-tab').on('click', function showObjectsFilter() {
            if ($('#home').attr('data-layer') != 'clusters') {
                $('.objectsFilter').css('display', 'block');
            }
            $('.objectsFilter').off('click', showObjectsFilter);
        });
        setLayout(Layouts.MAP);
    }

    //тяжелая конструкция, но иногда можно использовать. лучше вешать не на боди
    $("body").on("click", ".mapSideBarCollapse__show", function () {
        if (config.minimap && config.mini_redirect && !config.only_base_map) {
            if (window.parent) {
                let pathArray = document.referrer.split('/');
                let origin = pathArray[0] + '//' + pathArray[2];
                window.parent.postMessage('opentripmap:show_full' + getMapBoxGLHashString(map), origin);
            }
        }else {
            if (config.getWidth() <= 768) {
                if (!objectList && !routeList) {
                    getLists();
                }
                setLayout(Layouts.SIDEBAR);
            } else {
                setLayout(Layouts.MAP_WITH_SIDEBAR);
                $('.mapSideBarCollapse__hide').css('display', 'block');
                if (!objectList && !routeList) {
                    getLists();
                }
            }
        }
    }).on("click", ".mapSideBarCollapse__hide", function () {
        setLayout(Layouts.MAP);
        if (config.getWidth() > 768) {
            $('.mapSideBarCollapse__hide').css('display', 'none');
        };
    });

    window.addEventListener('resize', debounce(
        function () {
            resizeLayout();
            if (config.layout !== Layouts.MAP) {
                getLists();
            }
        },
        200, false), false);

    categorsMenu.on('changed', (evt) => {
        let msg = "";
        if (categors.contains(evt.id)) {
            categors.remove(evt.id);
            msg = "-" + evt.id;
        } else {
            categors.add(evt.id);
            msg = "+" + evt.id;
        }
        sendUserEvent('change_categor', msg, 'no');
        tileLayer.setCategors(categors.toString());
    });

    tileLayer.rebuild();

    global.directions = new Directions(map);
    global.isochrones = new Isochrones(map);
    let popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });

    $("#card-content").on("click", ".objectCard__backToCards", function () {
        layersState.removeIsochrone();
        layersState.removeRoute();
        getLists();
    });

    let hoveredFeature = null;

    function showRouteCard(tour) {
        sendUserEvent('show_route', tour.url, 'no');
        let routeCard = new IziRouteCard(tour);
        routeCard.on('fitBounds', (bounds) => {
            let coordinates = bounds.split(',');
            map.fitBounds([
                [
                    coordinates[1],
                    coordinates[0]
                ],
                [
                    coordinates[3],
                    coordinates[2]
                ]
            ]);
        });
        $("#list-content").hide();
        routeCard.show();
    }

    ObjectCard.getInstance().on('open', (feature) => {
        $("#list-content").hide();
        if (hoveredFeature &&
            (hoveredFeature.sourceLayer != feature.sourceLayer || hoveredFeature.id != feature.id)) {
            map.setFeatureState(hoveredFeature, {
                hover: false
            });
        }
        map.setFeatureState(feature, {
            hover: true
        });
        hoveredFeature = feature;
        layersState.addHoveredFeature(hoveredFeature);
    }).on('close', (item) => {
        if (hoveredFeature) {
            map.setFeatureState(hoveredFeature, {
                hover: false
            });
            hoveredFeature = null;
        }
        layersState.removeRoute();
        layersState.removeHoveredFeature();
        $("#list-content").show();
    }).on('route', (geojson) => {
        layersState.addRoute(geojson);
    });


    function showObjectCard(feature) {
        sendUserEvent('show', '', feature.properties.xid);
/*
        let homeFeature = config.getHomeFeatureByXID(feature.properties.xid);
        if (homeFeature) {
            homeFeature.properties.catset = feature.properties.catset;
            homeFeature.properties.kind = feature.properties.kind;
            feature.properties = homeFeature.properties;
        }
*/
        ObjectCard.getInstance().setFeature(feature);

        let coordinates = feature.geometry.coordinates.slice();
        let pt1 = {
            "lat": config.getHomeFeature().lat,
            "lon": config.getHomeFeature().lon,
            "type": "break",
            "name": "Hotel"
        };
        let pt2 = {
            "lat": coordinates[1],
            "lon": coordinates[0],
            "type": "break",
            "name": "Attraction"
        };
        let locations = [pt1, pt2];
        global.directions.getDirections(locations, "pedestrian").then(summary => {
            ObjectCard.getInstance().setWalkingRoute(summary);
            global.directions.getDirections(locations, "bicycle").then(summary => {
                ObjectCard.getInstance().setBicycleRoute(summary);
            });
        });

        if (config.isohrones) {

            let pedestrianParams = {
                locations: [{
                    "lat": pt2.lat,
                    "lon": pt2.lon
                }],
                costing: "pedestrian",
                contours: [{
                    "time": 5
                }, {
                    "time": 15
                }, {
                    "time": 30
                }],
                polygons: true,
                denoise: .2
                //generalize: 150
            };
            global.isochrones.getIsochrones(pedestrianParams).then(isochrones => {
                // map.fitBounds(window.isochrones.getBBox(isochrones), {
                //     padding: 20
                // });
                layersState.addIsochrone(isochrones);
            });
            $('#objectRouteBicycle').on('click', function onRouteBicycle() {
                let bicylceParams = pedestrianParams;
                bicylceParams.costing = 'bicycle';
                global.isochrones.getIsochrones(bicylceParams).then(isochrones => {
                    layersState.addIsochrone(isochrones);
                });
            });
        }

        showMapSideBar();
        ObjectCard.getInstance().show();
    }

    function queryAndShowObjectCard(data_id, mainkind) {
        getShortFeature(data_id, mainkind, 'normal').then(function (feature) {
            showObjectCard(feature);
        }).catch(function (reason) {
            //$('#loading').hide();
            $("#list-content").show();
            JL().error("Error", reason);
        });
    }

    map.on('click', function (e) {

        const bbox = is_touch_device() ? [
            [e.point.x - 5, e.point.y - 5],
            [e.point.x + 5, e.point.y + 5]
        ] : [e.point.x, e.point.y];

        //let layers = config.getPOILayers().concat(['all_pois_icons', 'all_pois', 'home_icons']);
        let layers = [];
        ['all_pois_icons', 'all_pois', 'home_icons'].forEach(function (layer){
            if (map.getLayer(layer)){
                layers.push(layer);
            }
        });
        const features = map.queryRenderedFeatures(bbox, {
            layers: layers
        });

        const feature = (features && features.length > 0) ? features[0] : null;
        if (feature == null)
            return;

        popup.remove();

        if (config.minimap && config.mini_redirect && window.parent) {
            //window.parent.dispatchEvent(new CustomEvent('opentripmap', {detail: {action: 'show_full'}}));
            let pathArray = document.referrer.split('/');
            let origin = pathArray[0] + '//' + pathArray[2];
            if (config.only_base_map){
                if (feature.properties.xid) {
                    window.parent.postMessage('opentripmap:show_object#' + feature.properties.xid, origin);
                }
            }else{
                window.parent.postMessage('opentripmap:show_full'+getMapBoxGLHashString(map), origin);
            }

            return;
        }

        let coordinates = feature.geometry.coordinates.slice();
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }
        if (feature.source == "hotel"){
            feature.source_name = feature.layer.source;
            showObjectCard(feature);
            return;
        }
        if (feature.layer["source-layer"] != 'pois') {
            if (feature.layer["source-layer"] == 'poi') {
                feature.properties.xid = "POI";
                showObjectCard(feature);
            }
            return;
        }
        queryAndShowObjectCard(feature.properties.id, feature.properties.mainkind);
    });

    function showPopup(e) {
        let feature = e.features[0];
        // if (card.feature.id && card.isOpen()) {
        //     if (card.feature.id == feature.properties.id) {
        //         return;
        //     }
        // }

        map.getCanvas().style.cursor = 'pointer';

        let coordinates = feature.geometry.coordinates.slice();

        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        const properties = feature.properties;
        let name = properties.name;
        let description = "";

        if (feature.layer.source == 'clusters') {
            description = '<span class="label-tooltip-Inner-layer">' + name + '</span>';
        } else {
            feature.source_name = feature.layer.source;
            if (feature.layer["source-layer"]) {
                feature.source_layer = feature.layer["source-layer"];
            }
            feature.properties.kind = feature.properties.subclass || feature.properties.class;
            feature.properties.__id__ = feature.id;
            feature.properties.xid = 'POI';

            if (!properties.name || properties.name == '') {
                name = getFeatureLayerName(feature);
            } else {
                description = '<span class="label-tooltip-Inner">' + name + '</span><br>';
            }
            description += '<span class="label-tooltip-Inner-layer">' + getFeatureLayerName(feature) + '</span>';
        }

        popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(map);
    }

    map.on('mouseenter', 'directions-waypoint-point', function (e) {
        let feature = e.features[0];
        map.getCanvas().style.cursor = 'pointer';
        let coordinates = feature.geometry.coordinates.slice();

        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        const properties = feature.properties;
        let description = '<span class="label-tooltip-Inner">' + properties.title + '</span><br>';

        description += '<span class="label-tooltip-Inner-layer">'+LOC.txt.tours.route_point+' №' + properties.name + '</span>';

        popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(map);
    });

    map.on('mouseenter', 'clusters', function (e) {
        showPopup(e);
    });

    map.on('mouseenter', 'all_pois', function (e) {
        showPopup(e);
    });

    map.on('mouseenter', 'all_pois_icons', function (e) {
        showPopup(e);
    });

    map.on('mouseenter', 'poi_base', function (e) {
        showPopup(e);
    });
    map.on('mouseenter', 'poi_aerodrome', function (e) {
        showPopup(e);
    });
    map.on('mouseenter', 'home_icons', function (e) {
        showPopup(e);
    });

    map.on('mouseleave', 'directions-waypoint-point', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });

    map.on('mouseleave', 'clusters', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });


    map.on('mouseleave', 'all_pois', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });

    map.on('mouseleave', 'all_pois_icons', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });

    map.on('mouseleave', 'poi_base', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
    map.on('mouseleave', 'poi_aerodrome', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });
    map.on('mouseleave', 'home_icons', function (e) {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });

    map.on('pitchstart', function (e) {
        $(".compass3DMode").text("2D");
        $(".compass3DMode").css('color', 'white').css('background-color', '#212529');
    });
    map.on('pitchend', function (e) {
        if (map.getPitch() != 0) {
            $(".compass3DMode").text("2D");
            $(".compass3DMode").css('color', 'white').css('background-color', '#212529');
        } else {
            $(".compass3DMode").text("3D");
            $(".compass3DMode").css('background-color', 'white').css('color', '#212529');
        }
    });

    let basemapsFilter = $('.basemapsNavigation__basemapsFilter');
    basemapsFilter.on('click', function () {
        $(this).toggleClass("active");
        $(this).children().toggleClass("active");
        $('.basemaps').toggleClass("active");
    });
    let basemapsMenu = new BasemapsMenu();
    basemapsMenu.show();

    basemapsMenu.on('changed', (evt) => {
        let access_token = 'pk.eyJ1IjoibWF4a2V5IiwiYSI6ImEyNWQ3MGZlZjVmNmVhY2MxZWNjM2U2ZWY0OWNkZGJkIn0.GD8SIK1BQKJYIE4KlhptHw';
        mapboxgl.accessToken = access_token;
        map.setStyle(config.getBaseMaps()[evt.id].style, {
            diff: false
        });
        map.setLanguage(config.lang, true);
        config.currentBaseMap = evt.id;
    });

    map.on('style.load', function rebuildLayers() {
        tileLayer.rebuild();
        layersState.rebuild();
    });

    $('.objectCard').on('click', '.logged_link', function (event) {
        let xid = $('.objectCard').attr('data-xid');
        if (!xid)
            xid = 'NO';
        sendUserEvent('href', $(this).attr('data-id'), xid);
    });
};

export {
    loadMap
};
import Fingerprint2 from "fingerprintjs2";
import UAParser from "ua-parser-js";
import {
    configData
} from "./configData";

export const Layouts = {
    MAP_WITH_SIDEBAR: 'MAP_WITH_SIDEBAR',
    MAP: 'MAP',
    SIDEBAR: 'SIDEBAR',
    NO: 'NO'
};
//export const serverHost = "http://localhost:8082";
export const serverHost = "https://opentripmap.com";

export function sendUserEvent(event, info, xid){
    let img = document.createElement("img");
    img.src = serverHost+'/stats/x.gif?app=embeded&xid=' + xid +'&event='+event +'&f='+config.fingerprint + '&info='+info;
}

function postUserEvent(data){
    let xhr = new XMLHttpRequest();
    xhr.open('POST', serverHost+'/stats?app=embeded&f='+ config.fingerprint, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.send(JSON.stringify(data));
}

const copyrights = {
    otm: {
        href: 'https://opentripmap.com/',
        text: 'OpenTripMap'
    },
    maptiler: {
        href: 'https://www.maptiler.com/copyright/',
        text: 'MapTiler'
    },
    mapbox: {
        href: 'https://www.mapbox.com/about/maps/',
        text: 'Mapbox'
    },
    osm: {
        href: 'http://www.openstreetmap.org/copyright/',
        text: 'OSM contributors'
    }
};

export let config = {
    heatmap : false,
    minimap : false,
    mini_redirect : false,
    only_base_map : false,
    homeFeatureIndex : 0,
    poi_main_filter : {value : 1, herit : 0},
    info : {},
    fingerprint : 'unknown',
    isohrones: false,
    safety_mode : false,
    tiles : configData.tiles,
    currentBaseMap : configData.currentBaseMap,
    layout: Layouts.NO,
    setFingerPrint : function(components){
        let values = components.map(function (component) { return component.value });
        this.fingerprint = Fingerprint2.x64hash128(values.join(''), 31);
        sendUserEvent('start', JSON.stringify(this.info), 'no');
        let data = {};
        for (let i = 0; i < components.length; i++) {
            let component = components[i];
            if (component.value && component.key === 'userAgent'){
                let parser = new UAParser(component.value);
                data[component.key] = parser.getResult();
            }else  if (component.value && ['userAgent', 'language', 'screenResolution', 'platform', 'touchSupport'].indexOf(component.key) !== -1){
                data[component.key] = component.value;
            }
        }
        data["size"] = { w: this.getWidth(), h : this.getHeight() };
        data["gl"] = this.safety_mode ? 'false' : 'true';
        postUserEvent(data);
    },
    lang : "unknown",
    getLang : function(){
        return this.lang;
    },
    getThemeColor : function (){
        return configData.themeColor;
    },
    getLangPriority : function (){
        if (this.getLang() === 'ru'){
            return 'ru,en';
        }else if (this.getLang() === 'en'){
            return 'en,ru';
        }
        return this.getLang() + ",en,ru";
    },
    getOTMLang : function(){
        return this.getLang() === 'ru' ? 'ru' : 'en';
    },
    getWidth : function(){
        return window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

    },
    getHeight : function(){
        return window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;

    },
    getPreviewSize : function(){
        return this.getWidth() < 800 ? 200 : 400;
    },
    /*
    getPOILayers: function(){
        let layers = map.getStyle().layers;
        let ids = [];
        for (let i = 0; i < layers.length; i++) {
            if (layers[i]["source-layer"] ===  "poi") {
                ids.push(layers[i].id);
            }
        }
        return ids;
    },*/
    getBaseMaps : function(){
        return configData.basemaps;
    },
    getBaseCurrentMap : function(){
      return configData.basemaps[this.currentBaseMap];
    },
    getMapAttribution : function (){
        let attribution = [];
        let current = this.getBaseCurrentMap().copyrights;
        for (let n = 0; n < current.length; ++n){
            attribution.push(copyrights[current[n]])
        }
        return attribution;
    },
    getLayers : function(){
      return  configData.layers;
    },
    getActiveTags : function(){
        let str = '';
        for (let i = 0; i < configData.layers.length; i++) {
            let item = configData.layers[i];
            if (item.enable) {
                str += item.id + ",";
            }
        }
        return str.length == 0 ? '' : str.substring(0, str.length-1);
    },
    getHomeFeatures : function () {
        let features = [configData.homeFeatures[this.homeFeatureIndex]];
        for (let n = 0; n < configData.homeFeatures.length; ++n){
            if ( n != this.homeFeatureIndex){
                features.push(configData.homeFeatures[n]);
            }
        }
        return features;
    },
    getHomeFeature : function () {
        return configData.homeFeatures[this.homeFeatureIndex];
    },
    getHomeFeatureByXID : function (xid) {
        for (let n = 0; n < configData.homeFeatures.length; ++n){
            if (xid == configData.homeFeatures[n].properties.xid) {
                return configData.homeFeatures[n];
            }
        }
        return null;
    }

};


export default class RadioButton {
    static add(id1, onclick1, id2, onclick2){
        $(id1).click(function (evt) {
            $(id2).removeClass('active');
            $(id2+" i").removeClass('active');
            $(id2+" div").removeClass('active');

            $(id1).addClass('active');
            $(id1+" i").addClass('active');
            $(id1+" div").addClass('active');
            onclick1();
        });
        $(id2).click(function (evt) {
            $(id1).removeClass('active');
            $(id1+" i").removeClass('active');
            $(id1+" div").removeClass('active');

            $(id2).addClass('active');
            $(id2+" i").addClass('active');
            $(id2+" div").addClass('active');
            onclick2();
        });
    }
    static isActive(id){
        return $(id).hasClass('active');
    }
}
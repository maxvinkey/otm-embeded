import './scss/main.scss';
import mapboxgl from "mapbox-gl";
import CopyrightControl from "./copyrightControl";
import OTMLogoControl from "./OTMLogoControl";
import {config, Layouts} from "./config";
import {global} from "./global";
import {loadMap} from "./main";
import initLanguageModule from "./openmaptiles-language";

var MapboxGeocoder = require('@mapbox/mapbox-gl-geocoder');

export default function renderMapboxGL() {
    let access_token = 'pk.eyJ1IjoibWF4a2V5IiwiYSI6ImEyNWQ3MGZlZjVmNmVhY2MxZWNjM2U2ZWY0OWNkZGJkIn0.GD8SIK1BQKJYIE4KlhptHw';
    initLanguageModule();

    function parseHash(hash) {
        let loc = hash.replace('#', '').split('/');
        if (loc.length >= 3) {
            return{
                center: [+loc[2], +loc[1]],
                zoom: +loc[0],
                bearing: +(loc[3] || 0),
                pitch: +(loc[4] || 0)
            }
        }
        return null;
    }

    let param = parseHash(window.location.hash);
    if (!param){
        param = {
            zoom: config.getHomeFeature().zoom,
            center: [config.getHomeFeature().lon, config.getHomeFeature().lat],
            bearing: 0,
            pitch: 0
        };
    }

    let map = new mapboxgl.Map({
        container: 'map',
        attributionControl: false,
        hash: true,
        style: config.getBaseCurrentMap().style,
        zoom: param.zoom,
        center: param.center,
        bearing : param.bearing,
        pitch : param.pitch,
        transformRequest: (url, resourceType)=> {
             if(resourceType === 'Tile' && url.startsWith('https://api.maptiler.com/tiles/v3/')) {
                     let tail = url.substring(34, url.lastIndexOf(".pbf"));
                     let parts = tail.split(/[\\\/]/);
                     let z = parseInt(parts[0]);
                     let index = 0;
                     while (index < config.tiles.length) {
                         let tile = config.tiles[index++];
                         if (tile.z === z){
                             let x = parseInt(parts[1]);
                             let y = parseInt(parts[2]);
                             if (x >= tile.xmin && x <= tile.xmax &&
                                 y >= tile.ymin && y <= tile.ymax){
                                 return {url: 'https://a.tiles.opentripmap.com/v3/'+tail+".pbf"};
                             }
                             break;
                         }
                     }
                 }
           }
    });

    global.map = map;

    let eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    window[eventMethod](eventMethod === "attachEvent" ? "onmessage" : "message",function(e) {
        if (window.parent && window.parent.origin === e.origin){
            if (e.data &&  (typeof e.data === 'string' || e.data instanceof String) &&
                e.data.indexOf('opentripmap:show') === 0){
                let parts = e.data.split("#");
                let camera = null;
                if (parts[1].indexOf('i') === 0){
                    let param = parts[1].split("=");
                    config.homeFeatureIndex = parseInt(param[1], 10);
                    camera = {
                        zoom : config.getHomeFeature().zoom,
                        center : [config.getHomeFeature().lon, config.getHomeFeature().lat],
                        bearing: 0,
                        pitch: 0
                    };
                }else{
                    camera = parseHash(parts[1]);
                }
                if (camera){
                    global.map.jumpTo(camera);
                }

            }
        }
    },false);

    map.setLanguage(config.lang, true);

    let logoControl = new OTMLogoControl();
    map.addControl(logoControl, 'bottom-left');

    let copyrightControl = new CopyrightControl();
    map.addControl(copyrightControl, 'bottom-right');

    let geocoder = new MapboxGeocoder({
        accessToken: access_token,
        placeholder: LOC.txt.search.placeholder,
        minLength: 3
        //flyTo: false
    });
    geocoder.on('result', function (ev) {
        if (config.layout === Layouts.SIDEBAR) {
            setLayout(Layouts.MAP);
        }
    });

    document.getElementById('geocoder').appendChild(geocoder.onAdd(map));

    logoControl._container.classList.add('logoControl');
    logoControl._container.parentNode.classList.add('logoAndCopyrightControls');

    $("button.geocoder-icon-close").addClass("fas fa-times");
    $("span.geocoder-icon-search").addClass("fas fa-search");
    $("span.geocoder-icon-loading").addClass("fas fa-spinner");

    let geocoderInput = document.querySelector('.mapboxgl-ctrl-geocoder input');
    geocoderInput.insertAdjacentHTML("afterend", "<div class='geocoderInput'></div>");

    //window.map = map;
    map.on('load', function () {

        function addNavigatonControls() {

            var nav = new mapboxgl.NavigationControl();
            map.addControl(nav, 'bottom-right');

            nav._container.classList.add('navigationControl');
            nav._container.parentNode.classList.add('navigationAndCopyrightControls');

            let compassArrow = $('.mapboxgl-ctrl-compass-arrow');
            let compassArrowWest = $('<div />', {
                class: 'compassArrowWest'
            });
            let compassArrowEast = $('<div />', {
                class: 'compassArrowEast'
            });
            let compass3DMode = $('<div />', {
                class: 'compass3DMode',
                text: '3D'
            });

            compassArrowEast.appendTo(compassArrow);
            compassArrowWest.appendTo(compassArrow);
            compass3DMode.appendTo(compassArrow);
            let routesFilter = $('.routesNavigation__routesFilter');
            routesFilter.on('click', function () {
                $(this).toggleClass("active");
                $(this).children().toggleClass("active");
                $('.categories').toggleClass("active");

                if (config.layout === Layouts.SIDEBAR){
                    if ($('.categories.onCard').hasClass('active')) {
                        $('.categories').removeClass("active");
                        $('.categories.onCard').addClass("active");
                    }
                }else if (config.layout === Layouts.MAP){
                    if ($('.categories').hasClass('active')) {
                        $('.categories.onCard').removeClass("active");
                    } else {
                        $('.categories').removeClass("active");
                    }
                }

                setModal();
                $('.mapSideBarCollapse').one('click', function showModal() {
                    $('.routesNavigation__routesFilter').removeClass("active")
                        .children().removeClass("active");
                    $('.categories').removeClass("active");
                    //$('.mapSideBarCollapse').off('click', showModal);
                });
            });

            function setModal() {
                if (config.layout === Layouts.MAP_WITH_SIDEBAR)
                    return;

                let vw = window.innerWidth * 0.01;
                $(".categories.onCard").css('right', `${(100*vw-160)/2}px`);
                if ($('.mapSideBar').is(':visible')) {
                    $('.mapboxgl-ctrl-geocoder').css('z-index', '0');
                };
                $('.categories__modal.onCard').css('display', 'block');
                //$('.categories__closeModal.onCard').unbind('click');
                $('.categories__closeModal.onCard').one('click', function closeModal() {
                    $('.mapboxgl-ctrl-geocoder').css('z-index', '1');
                    $('.categories__modal.onCard').css('display', 'none');
                    $('.routesNavigation__routesFilter.onCard').removeClass("active")
                        .children().removeClass("active");
                    $('.categories').removeClass("active");
                    //$('.categories__closeModal.onCard').off('click', closeModal);
                });

            }

            window.addEventListener("resize", function () {
                if ($(".categories").is(":visible") && ($('#home').attr('data-layer') == 'vpois')) {
                    setModal();
                    $('.mapboxgl-ctrl-geocoder').css('z-index', '1');
                    if (config.layout === Layouts.MAP_WITH_SIDEBAR) {
                        $('.categories__modal').css('display', 'none');
                    } else {
                        $('.categories').removeClass("active");
                        $('.routesNavigation__routesFilter').removeClass("active");
                        $('.routesNavigation__routesFilter').children().removeClass("active");
                        $('.categories.onCard').addClass("active");
                    }
                };
                if ($(".categories").is(":visible") && ($('#home').attr('data-layer') == 'clusters')) {
                    $('.categories').removeClass("active");
                    $('.categories__modal').css('display', 'none');
                    $('.routesNavigation__routesFilter.onCard').removeClass("active")
                        .children().removeClass("active");
                }

            });

            let compass = document.getElementsByClassName('compass3DMode')[0];
            compass.addEventListener('click', function () {
                if (map.getPitch() != 0) {
                    map.setPitch(0);
                } else {
                    map.setPitch(60);
                }
            });
            $('.mapboxgl-ctrl-zoom-in').append('<i class="fas fa-plus"></i>');
            $('.mapboxgl-ctrl-zoom-out').append('<i class="fas fa-minus"></i>');
        };
        window.onload = addNavigatonControls();
        loadMap(map);
    });
}

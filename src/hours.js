import {config} from "./config";
let opening_hours = require('opening_hours');

export default function getOpeningHours(prop_hours) {
    let oh = new opening_hours(prop_hours);
    let state = LOC.txt.card.opening_hours;
    if (!oh.getUnknown()) {
        state = oh.getState() ? LOC.txt.card.opening_hours_open : LOC.txt.card.opening_hours_closed;
    }
    let val = oh.prettifyValue({
        conf: {
            locale: config.getLang()
        }
    });
    if (config.getLang() === "ru"){
        val = val.replace('closed', 'закрыто');
    }

    return {state:state, val:val};
}
const EventEmitter = require( 'events' );
import RadioButton from "./radioButton";

export default class RouteCard extends EventEmitter{
    constructor(name, description) {
        super();
        this.walking = {};
        this.bicycle = {};
        let div = document.querySelector('#profile');
        div.innerHTML = this.render(name, description);
        let self = this;
        RadioButton.add(
            "#routeCardWalking",function(){
                self.emit('route', self.walking.geojson);
            }, "#routeCardBicycle",function(){
                self.emit('route', self.bicycle.geojson);
            });
    }
    isVisible() {
        return $("#profile").is(":visible");
    }
    setWalkingRoute(summary) {
        this.walking = summary;
        if (this.isVisible() && RadioButton.isActive("#routeCardWalking")){
            this.emit('route', this.walking.geojson);
        }
        let block = document.querySelector("#routeCardWalking > div");
        block.innerHTML = `${summary.time}<br>${summary.dist}`;
    }
    setBicycleRoute(summary) {
        this.bicycle = summary;
        if (this.isVisible() && RadioButton.isActive("#routeCardBicycle")){
            this.emit('route', this.bicycle.geojson);
        }
        let block = document.querySelector("#routeCardBicycle > div");
        block.innerHTML = `${summary.time}<br>${summary.dist}`;
    }
    render(name, description){
        return `<div class="routeCard">
                        <h5 class="card-title routeCard__title">${name}</h5>
                        <h6 class="card-subtitle routeCard__subtitle mb-2 text-muted">${description}</h6>
                        <div class="objectRoutes">
                            <a id="routeCardWalking" class="objectRoute">
                                <i class="fas fa-walking objectRoute__firstIcon"></i>
                                <div class="objectRoute__distance ">---<br>---</div>
                             </a>
                            <a id="routeCardBicycle" class="objectRoute">
                                <i class="fas fa-bicycle objectRoute__secondIcon"></i>
                                <div class="objectRoute__distance">---<br>---</div>
                             </a>
                        </div>
                    </div>`;
    }
}
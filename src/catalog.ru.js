let layer_catalog = [
    {
        id: 700000000,
        text: "Камеры",
        checked: false,
        children: []
    },
    {
        id: 100000000,
        text: "Интересные места",
        checked: false,
        children: [
            {
                id: 101000000,
                text: "Природа",
                checked: false,
                children: [
                    {
                        id: 101010000,
                        text: "Острова",
                        checked: false,
                        children: [
                            {
                                id: 101010100,
                                text: "приливные острова",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010200,
                                text: "озерные и речные острова",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010300,
                                text: "коралловые острова",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010400,
                                text: "необитаемые острова",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010500,
                                text: "вулканические острова",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101019900,
                                text: "прочие острова",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101020000,
                        text: "Природные источники",
                        checked: false,
                        children: [
                            {
                                id: 101020100,
                                text: "горячие источники",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101020200,
                                text: "гейзеры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101020300,
                                text: "прочие источники",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101030000,
                        text: "Геологические образования",
                        checked: false,
                        children: [
                            {
                                id: 101030100,
                                text: "горные вершины",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030200,
                                text: "вулканы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030300,
                                text: "пещеры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030400,
                                text: "каньоны",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030500,
                                text: "скальные образования",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101040000,
                        text: "Водоемы",
                        checked: false,
                        children: [
                            {
                                id: 101040100,
                                text: "кратерные озера",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040200,
                                text: "тектонические озера",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040300,
                                text: "соленые озера",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040400,
                                text: "пересыхающие озера",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040500,
                                text: "водохранилища",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040600,
                                text: "реки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040700,
                                text: "каналы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040800,
                                text: "водопады",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040900,
                                text: "лагуны",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101049900,
                                text: "прочие озера",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101050000,
                        text: "Пляжи",
                        checked: false,
                        children: [
                            {
                                id: 101050100,
                                text: "золотые песчаные пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050200,
                                text: "белые песчаные пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050300,
                                text: "черные песчаные пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050400,
                                text: "галечные пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050500,
                                text: "скалистые пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050600,
                                text: "городские пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050700,
                                text: "нудистские пляжи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101059900,
                                text: "прочие пляжи",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101060000,
                        text: "Заповедники",
                        checked: false,
                        children: [
                            {
                                id: 101060100,
                                text: "водные заповедники",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060200,
                                text: "заповедники дикой природы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060300,
                                text: "национальные парки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060400,
                                text: "прочие заповедники",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060500,
                                text: "памятники природы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101069900,
                                text: "природоохранные территории",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101070000,
                        text: "Ледники",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 102000000,
                text: "Культура",
                checked: false,
                children: [
                    {
                        id: 102010000,
                        text: "Музеи",
                        checked: false,
                        children: [
                            {
                                id: 102010100,
                                text: "национальные музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010200,
                                text: "краеведческие музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010300,
                                text: "музеи науки и техники",
                                checked: false,
                                children: [
                                    {
                                        id: 102010301,
                                        text: "морские музеи",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010302,
                                        text: "железнодорожные музеи",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010303,
                                        text: "музеи авиации",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010304,
                                        text: "музеи автомобилей",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010305,
                                        text: "компьютерные музеи",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010306,
                                        text: "исторические железные дороги",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010399,
                                        text: "прочие технические музеи",
                                        checked: false,
                                        children: []
                                    }
                                ]
                            }
                            , {
                                id: 102010400,
                                text: "естественно-научные музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010500,
                                text: "планетарии",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010600,
                                text: "военные музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010700,
                                text: "исторические музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010800,
                                text: "археологические музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010900,
                                text: "биографические музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011000,
                                text: "музеи под открытым небом",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011100,
                                text: "музеи моды",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011200,
                                text: "детские музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011300,
                                text: "исторические дома-музеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011400,
                                text: "художественные музеи и галереи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011500,
                                text: "зоопарки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011600,
                                text: "аквариумы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102019900,
                                text: "прочие музеи",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 102020000,
                        text: "Театры и концертные залы",
                        checked: false,
                        children: [
                            {
                                id: 102020100,
                                text: "летние театры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020200,
                                text: "оперные театры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020300,
                                text: "музыкальные театры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020400,
                                text: "концертные залы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020500,
                                text: "кукольные театры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020600,
                                text: "детские театры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102029900,
                                text: "прочие театры",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 102030000,
                        text: "Городская среда",
                        checked: false,
                        children: [
                            {
                                id: 102030100,
                                text: "настенная живопись",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030200,
                                text: "городские площади и улицы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030300,
                                text: "инсталляции",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030400,
                                text: "сады и парки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030500,
                                text: "фонтаны",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030600,
                                text: "скульптуры",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                ]
            }
            , {
                id: 103000000,
                text: "История",
                checked: false,
                children: [
                    {
                        id: 103010000,
                        text: "Исторические места",
                        checked: false,
                        children: [
                            {
                                id: 103010100,
                                text: "исторические районы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103010200,
                                text: "исторические поселения",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103010300,
                                text: "рыбацкие деревни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103010400,
                                text: "места сражений",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103020000,
                        text: "Оборонительные сооружения",
                        checked: false,
                        children: [
                            {
                                id: 103020100,
                                text: "крепости и замки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020200,
                                text: "городища",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020300,
                                text: "оборонительные башни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020400,
                                text: "оборонительные стены",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020500,
                                text: "бункеры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020600,
                                text: "кремли",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103029900,
                                text: "прочие сооружения",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103030000,
                        text: "Монументы и мемориалы",
                        checked: false,
                        children: [
                            {
                                id: 103030100,
                                text: "исторические знаки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103030200,
                                text: "монументы",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103040000,
                        text: "Археология",
                        checked: false,
                        children: [
                            {
                                id: 103040100,
                                text: "мегалиты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040200,
                                text: "менгиры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040300,
                                text: "древнеримские виллы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040400,
                                text: "наскальные рисунки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040500,
                                text: "поселения",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040600,
                                text: "рунические камни ",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103049900,
                                text: "археологические памятники",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103050000,
                        text: "Захоронения",
                        checked: false,
                        children: [
                            {
                                id: 103050100,
                                text: "кладбища",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050200,
                                text: "военные захоронения",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050300,
                                text: "некрополи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050400,
                                text: "дольмены",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050500,
                                text: "тумулусы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050600,
                                text: "мавзолеи",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050700,
                                text: "воинские мемориалы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050800,
                                text: "крипты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103059900,
                                text: "прочие захоронения",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                ]
            }
            , {
                id: 104000000,
                text: "Религия",
                checked: false,
                children: [
                    {
                        id: 104010000,
                        text: "церкви",
                        checked: false,
                        children: [
                            {
                                id: 104010100,
                                text: "православные церкви ",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 104010200,
                                text: "католические церкви ",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 104019900,
                                text: "прочие церкви",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 104020000,
                        text: "соборные храмы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104030000,
                        text: "мечети",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104040000,
                        text: "синагоги",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104050000,
                        text: "буддийские храмы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104060000,
                        text: "индуистские храмы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104070000,
                        text: "храмы Древнего Египта",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104990000,
                        text: "прочие храмы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104090000,
                        text: "монастыри",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 105000000,
                text: "Архитектура",
                checked: false,
                children: [
                    {
                        id: 105010000,
                        text: "Историческая архитектура",
                        checked: false,
                        children: [
                            {
                                id: 105010100,
                                text: "пирамиды",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010200,
                                text: "амфитеатры",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010300,
                                text: "триумфальные арки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010400,
                                text: "дворцы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010500,
                                text: "усадьбы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010600,
                                text: "винодельни и пивоварни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010700,
                                text: "фермы",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105019900,
                                text: "здания и сооружения",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010900,
                                text: "разрушенные объекты",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 105020000,
                        text: "Небоскрёбы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 105030000,
                        text: "Мосты",
                        checked: false,
                        children: [
                            {
                                id: 105030100,
                                text: "разводные мосты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030200,
                                text: "каменные мосты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030300,
                                text: "виадуки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030400,
                                text: "древнеримские мосты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030500,
                                text: "пешеходные мосты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030600,
                                text: "акведуки",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030700,
                                text: "висячие мосты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105039900,
                                text: "прочие мосты",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 105040000,
                        text: "Башни",
                        checked: false,
                        children: [
                            {
                                id: 105040100,
                                text: "смотровые башни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040200,
                                text: "сторожевые башни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040300,
                                text: "водонапорные башни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040400,
                                text: "телебашни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040500,
                                text: "часовые башни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040600,
                                text: "колокольни",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040700,
                                text: "минареты",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105049900,
                                text: "прочие башни",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 105050000,
                        text: "Маяки",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 106000000,
                text: "Индустриальные объекты",
                checked: false,
                children: [
                    {
                        id: 106010000,
                        text: "Ж/д станции",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106020000,
                        text: "Фабрики",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106030000,
                        text: "Монетные дворы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106040000,
                        text: "Электростанции",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106050000,
                        text: "Плотины",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106060000,
                        text: "Мельницы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106070000,
                        text: "Заброшенные ж/д станции",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106080000,
                        text: "Заброшенные шахты",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106090000,
                        text: "Шахты",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106990000,
                        text: "Прочие сооружения",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 107000000,
                text: "Разное",
                checked: false,
                children: [
                    {
                        id: 107010000,
                        text: "Солнечные часы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 107020000,
                        text: "Смотровые площадки",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 107030000,
                        text: "Красные телефонные будки",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 107040000,
                        text: "Неклассифицированные достопримечательности",
                        checked: false,
                        children: [
                            {
                                id: 107040100,
                                text: "Туристические достопримечательности",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 107040200,
                                text: "Исторические достопримечательности",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                ]
            }
        ]
    }
    , {
        id: 200000000,
        text: "Развлечения",
        checked: false,
        children: [
            {
                id: 201000000,
                text: "парки развлечений",
                checked: false,
                children: []
            }
            , {
                id: 202000000,
                text: "парки миниатюр",
                checked: false,
                children: []
            }
            , {
                id: 203000000,
                text: "открытые бассейны и аквапарки",
                checked: false,
                children: []
            }
            , {
                id: 204000000,
                text: "американские горки",
                checked: false,
                children: []
            }
            , {
                id: 205000000,
                text: "колёса обозрения",
                checked: false,
                children: []
            }
            , {
                id: 299000000,
                text: "прочие аттракционы",
                checked: false,
                children: []
            }
        ]
    }
    , {
        id: 300000000,
        text: "Спорт",
        checked: false,
        children: [
            {
                id: 301000000,
                text: "зимний спорт",
                checked: false,
                children: [
                    {
                        id: 301010000,
                        text: "горные лыжи",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 301020000,
                        text: "беговые лыжи",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 301990000,
                        text: "другие зимние виды спорта",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 302000000,
                text: "дайвинг",
                checked: false,
                children: [
                    {
                        id: 302010000,
                        text: "дайвинг-центры",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 302020000,
                        text: "места для погружений",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 302030000,
                        text: "затонувшие корабли",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 303000000,
                text: "скалодромы",
                checked: false,
                children: []
            }
            , {
                id: 304000000,
                text: "серфинг",
                checked: false,
                children: []
            }
            , {
                id: 305000000,
                text: "кайтсерфинг",
                checked: false,
                children: []
            }
        ]
    }
    , {
        id: 400000000,
        text: "18+",
        checked: false,
        children: [
            {
                id: 401000000,
                text: "стрипклубы",
                checked: false,
                children: []
            }
            , {
                id: 402000000,
                text: "казино",
                checked: false,
                children: []
            }
            , {
                id: 403000000,
                text: "публичные дома",
                checked: false,
                children: []
            }
            , {
                id: 404000000,
                text: "ночные клубы",
                checked: false,
                children: []
            }
            , {
                id: 405000000,
                text: "алкоголь",
                checked: false,
                children: []
            }
            , {
                id: 406000000,
                text: "love-отели",
                checked: false,
                children: []
            }
            , {
                id: 407000000,
                text: "секс-шопы",
                checked: false,
                children: []
            }
        ]
    }
    , {
        id: 500000000,
        text: "Туристическая инфраструктура",
        checked: false,
        children: [
            {
                id: 501000000,
                text: "Транспорт",
                checked: false,
                children: [
                    {
                        id: 501010000,
                        text: "Прокат авто",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501020000,
                        text: "Каршеринг",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501030000,
                        text: "Автомойки",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501040000,
                        text: "Зарядные станции",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501050000,
                        text: "Прокат велосипедов",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501060000,
                        text: "Боатшеринг",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501070000,
                        text: "Заправки",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 502000000,
                text: "Магазины",
                checked: false,
                children: [
                    {
                        id: 502010000,
                        text: "Супермаркеты",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502020000,
                        text: "Продуктовые магазины",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502030000,
                        text: "Товары для туризма",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502040000,
                        text: "Торговые центры",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502050000,
                        text: "Рынки",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502060000,
                        text: "Пекарни",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 503000000,
                text: "Питание",
                checked: false,
                children: [
                    {
                        id: 503010000,
                        text: "Рестораны",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503020000,
                        text: "Кафе",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503030000,
                        text: "Фастфуд",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503040000,
                        text: "Фудкорт",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503050000,
                        text: "Пабы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503060000,
                        text: "Бары",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503070000,
                        text: "Пивные сады",
                        checked: false,
                        children: []
                    },
                    , {
                        id: 503080000,
                        text: "Места для пикника",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 504000000,
                text: "Банковские услуги",
                checked: false,
                children: [
                    {
                        id: 504010000,
                        text: "Банки",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 504020000,
                        text: "Банкоматы",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 504030000,
                        text: "Обмен валют",
                        checked: false,
                        children: []
                    }
                ]
            }
        ]
    }
    , {
        id: 600000000,
        text: "Размещение",
        checked: false,
        children: [
            {
                id: 601000000,
                text: "Апартаменты",
                checked: false,
                children: []
            }
            , {
                id: 602000000,
                text: "Гостевые дома",
                checked: false,
                children: []
            }
            , {
                id: 603000000,
                text: "Кемпинги",
                checked: false,
                children: []
            }
            , {
                id: 604000000,
                text: "Курортные отели",
                checked: false,
                children: []
            }
            , {
                id: 605000000,
                text: "Мотели",
                checked: false,
                children: []
            }
            , {
                id: 699000000,
                text: "Отели",
                checked: false,
                children: []
            }
            , {
                id: 607000000,
                text: "Хостелы",
                checked: false,
                children: []
            }
            , {
                id: 608000000,
                text: "Виллы и шале",
                checked: false,
                children: []
            }
            , {
                id: 609000000,
                text: "Горные приюты",
                checked: false,
                children: []
            }
            , {
                id: 610000000,
                text: "Отели для свиданий",
                checked: false,
                children: []
            }
        ]
    }
];

let layer_names = {
    "items": {
        "101010100": {"n": 'приливный[ые] остров [а]', "i": 'park-15', "e": '🏝️'},
        "101010200": {"n": 'остров[озерные и речные острова]', "i": 'park-15', "e": '🏝️'},
        "101010300": {"n": 'коралловый[е] остров [а]', "i": 'park-15', "e": '🏝️'},
        "101010400": {"n": 'необитаемый[е] остров [а]', "i": 'park-15', "e": '🏝️'},
        "101010500": {"n": 'вулканический[е] остров [а]', "i": 'park-15', "e": '🏝️'},
        "101019900": {"n": 'остров[прочие острова]', "i": 'park-15', "e": '🏝️'},
        "101020100": {"n": 'горячий[е] источник [и]', "i": 'park-15', "e": '💧'},
        "101020200": {"n": 'гейзер [ы]', "i": 'park-15', "e": '💧'},
        "101020300": {"n": 'источник[прочие источники]', "i": 'park-15', "e": '💧'},
        "101030100": {"n": 'горная[ые] вершина[ы]', "i": 'park-15', "e": '⛰️'},
        "101030200": {"n": 'вулкан [ы]', "i": 'park-15', "e": '⛰️'},
        "101030300": {"n": 'пещера[ы]', "i": 'park-15', "e": '⛰️'},
        "101030400": {"n": 'каньон [ы]', "i": 'park-15', "e": '⛰️'},
        "101030500": {"n": 'скальное[ые] образование[я]', "i": 'park-15', "e": '⛰️'},
        "101040100": {"n": 'кратерное[ые] озеро[а]', "i": 'park-15', "e": '🌊'},
        "101040200": {"n": 'тектоническое[ие] озеро[а]', "i": 'park-15', "e": '🌊'},
        "101040300": {"n": 'соленое[ые] озеро[а]', "i": 'park-15', "e": '🌊'},
        "101040400": {"n": 'пересыхающее[ие] озеро[а]', "i": 'park-15', "e": '🌊'},
        "101040500": {"n": 'водохранилище[а]', "i": 'park-15', "e": '🌊'},
        "101040600": {"n": 'река[и]', "i": 'park-15', "e": '🌊'},
        "101040700": {"n": 'канал [ы]', "i": 'park-15', "e": '🌊'},
        "101040800": {"n": 'водопад [ы]', "i": 'park-15', "e": '🌊'},
        "101040900": {"n": 'лагуна[ы]', "i": 'park-15', "e": '🌊'},
        "101049900": {"n": 'озеро[прочие озера]', "i": 'park-15', "e": '🌊'},
        "101050100": {"n": 'золотой[ые] песчаный[е] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101050200": {"n": 'белый[е] песчаный[е] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101050300": {"n": 'черный[е] песчаный[е] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101050400": {"n": 'галечный[е] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101050500": {"n": 'скалистый[е] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101050600": {"n": 'городской[ие] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101050700": {"n": 'нудистский[е] пляж [и]', "i": 'park-15', "e": '🏖️'},
        "101059900": {"n": 'пляж[прочие пляжи]', "i": 'park-15', "e": '🏖️'},
        "101060100": {"n": 'водный[е] заповедник [и]', "i": 'park-15', "e": '🏞️'},
        "101060200": {"n": 'заповедник [и] дикой природы', "i": 'park-15', "e": '🏞️'},
        "101060300": {"n": 'национальный[е] парк [и]', "i": 'park-15', "e": '🏞️'},
        "101060400": {"n": 'заповедник[прочие заповедники]', "i": 'park-15', "e": '🏞️'},
        "101060500": {"n": 'памятник [и] природы', "i": 'park-15', "e": '🏞️'},
        "101069900": {"n": 'природоохранная[ые] территория[и]', "i": 'park-15', "e": '🏞️'},
        "101070000": {"n": 'Ледник [и]', "i": 'park-15', "e": '🏔️'},
        "102010100": {"n": 'национальный[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010200": {"n": 'краеведческий[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010301": {"n": 'морской[ие] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010302": {"n": 'железнодорожный[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010303": {"n": 'музей[и] авиации', "i": 'town-hall-15', "e": '🏛️'},
        "102010304": {"n": 'музей[и] автомобилей', "i": 'town-hall-15', "e": '🏛️'},
        "102010305": {"n": 'компьютерный[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010306": {"n": 'историческая[ие] железная[ые] дорога[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010399": {"n": 'технический музей[прочие технические музеи]', "i": 'town-hall-15', "e": '🏛️'},
        "102010400": {"n": 'естественно-научный[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010500": {"n": 'планетарий[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010600": {"n": 'военный[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010700": {"n": 'исторический[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010800": {"n": 'археологический[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102010900": {"n": 'биографический[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102011000": {"n": 'музей[и] под открытым небом', "i": 'town-hall-15', "e": '🏛️'},
        "102011100": {"n": 'музей[и] моды', "i": 'town-hall-15', "e": '🏛️'},
        "102011200": {"n": 'детский[е] музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102011300": {"n": 'исторический[е] дом [а]-музей[и]', "i": 'town-hall-15', "e": '🏛️'},
        "102011400": {"n": 'художественный музей[художественные музеи и галереи]', "i": 'town-hall-15', "e": '🏛️'},
        "102011500": {"n": 'зоопарк [и]', "i": 'town-hall-15', "e": '🏛️'},
        "102011600": {"n": 'аквариум [ы]', "i": 'town-hall-15', "e": '🏛️'},
        "102019900": {"n": 'музей[прочие музеи]', "i": 'town-hall-15', "e": '🏛️'},
        "102020100": {"n": 'летний[е] театр [ы]', "i": 'town-hall-15', "e": '🎭'},
        "102020200": {"n": 'оперный[е] театр [ы]', "i": 'town-hall-15', "e": '🎭'},
        "102020300": {"n": 'музыкальный[е] театр [ы]', "i": 'town-hall-15', "e": '🎭'},
        "102020400": {"n": 'концертный[е] зал [ы]', "i": 'town-hall-15', "e": '🎭'},
        "102020500": {"n": 'кукольный[е] театр [ы]', "i": 'town-hall-15', "e": '🎭'},
        "102020600": {"n": 'детский[е] театр [ы]', "i": 'town-hall-15', "e": '🎭'},
        "102029900": {"n": 'театр[прочие театры]', "i": 'town-hall-15', "e": '🎭'},
        "102030100": {"n": 'настенная живопись', "i": 'town-hall-15', "e": '⛲'},
        "102030200": {"n": 'улица[городские площади и улицы]', "i": 'town-hall-15', "e": '⛲'},
        "102030300": {"n": 'инсталляция[и]', "i": 'town-hall-15', "e": '⛲'},
        "102030400": {"n": 'парк[сады и парки]', "i": 'town-hall-15', "e": '⛲'},
        "102030500": {"n": 'фонтан [ы]', "i": 'town-hall-15', "e": '⛲'},
        "102030600": {"n": 'скульптура[ы]', "i": 'town-hall-15', "e": '⛲'},
        "103010100": {"n": 'исторический[е] район [ы]', "i": 'castle-15', "e": '🛡️'},
        "103010200": {"n": 'историческое[ие] поселение[я]', "i": 'castle-15', "e": '🛡️'},
        "103010300": {"n": 'рыбацкая[ие] деревня[и]', "i": 'castle-15', "e": '🛡️'},
        "103010400": {"n": 'место[а] сражения[й]', "i": 'castle-15', "e": '🛡️'},
        "103020100": {"n": 'крепость[крепости и замки]', "i": 'castle-15', "e": '🏰'},
        "103020200": {"n": 'городище[а]', "i": 'castle-15', "e": '🏰'},
        "103020300": {"n": 'оборонительная[ые] башня[и]', "i": 'castle-15', "e": '🏰'},
        "103020400": {"n": 'оборонительная[ые] стена[ы]', "i": 'castle-15', "e": '🏰'},
        "103020500": {"n": 'бункер [ы]', "i": 'castle-15', "e": '🏰'},
        "103020600": {"n": 'кремль[и]', "i": 'castle-15', "e": '🏰'},
        "103029900": {"n": 'сооружение[прочие сооружения]', "i": 'castle-15', "e": '🏰'},
        "103030100": {"n": 'исторический[е] знак [и]', "i": 'castle-15', "e": '🗿'},
        "103030200": {"n": 'монумент [ы]', "i": 'castle-15', "e": '🗿'},
        "103040100": {"n": 'мегалит [ы]', "i": 'castle-15', "e": '🏺'},
        "103040200": {"n": 'менгир [ы]', "i": 'castle-15', "e": '🏺'},
        "103040300": {"n": 'древнеримская[ие] вилла[ы]', "i": 'castle-15', "e": '🏺'},
        "103040400": {"n": 'наскальный[е] рисунок[ки]', "i": 'castle-15', "e": '🏺'},
        "103040500": {"n": 'поселение[я]', "i": 'castle-15', "e": '🏺'},
        "103040600": {"n": 'рунический[е] камень[ни ]', "i": 'castle-15', "e": '🏺'},
        "103049900": {"n": 'археологический[е] памятник [и]', "i": 'castle-15', "e": '🏺'},
        "103050100": {"n": 'кладбище[а]', "i": 'castle-15', "e": '⚱️'},
        "103050200": {"n": 'военное[ые] захоронение[я]', "i": 'castle-15', "e": '⚱️'},
        "103050300": {"n": 'некрополь[и]', "i": 'castle-15', "e": '⚱️'},
        "103050400": {"n": 'дольмен [ы]', "i": 'castle-15', "e": '⚱️'},
        "103050500": {"n": 'тумулус [ы]', "i": 'castle-15', "e": '⚱️'},
        "103050600": {"n": 'мавзолей[и]', "i": 'castle-15', "e": '⚱️'},
        "103050700": {"n": 'воинский[е] мемориал [ы]', "i": 'castle-15', "e": '⚱️'},
        "103050800": {"n": 'крипт [ы]', "i": 'castle-15', "e": '⚱️'},
        "103059900": {"n": 'захоронение[прочие захоронения]', "i": 'castle-15', "e": '⚱️'},
        "104010100": {"n": 'православная[ые] церковь[ви ]', "i": 'place-of-worship-15', "e": '⛪'},
        "104010200": {"n": 'католическая[ие] церковь[ви ]', "i": 'place-of-worship-15', "e": '⛪'},
        "104019900": {"n": 'церковь[прочие церкви]', "i": 'place-of-worship-15', "e": '⛪'},
        "104020000": {"n": 'соборный[е] храм [ы]', "i": 'place-of-worship-15', "e": '🛐'},
        "104030000": {"n": 'мечеть[и]', "i": 'place-of-worship-15', "e": '🕌'},
        "104040000": {"n": 'синагога[и]', "i": 'place-of-worship-15', "e": '🕍'},
        "104050000": {"n": 'буддийский[е] храм [ы]', "i": 'place-of-worship-15', "e": '☸️'},
        "104060000": {"n": 'индуистский[е] храм [ы]', "i": 'place-of-worship-15', "e": '🕉️'},
        "104070000": {"n": 'храм [ы] Древнего Египта', "i": 'place-of-worship-15', "e": '🛐'},
        "104990000": {"n": 'храм[прочие храмы]', "i": 'place-of-worship-15', "e": '🛐'},
        "104090000": {"n": 'монастырь[и]', "i": 'place-of-worship-15', "e": '🛐'},
        "105010100": {"n": 'пирамида[ы]', "i": 'building-alt1-15', "e": '🏡'},
        "105010200": {"n": 'амфитеатр [ы]', "i": 'building-alt1-15', "e": '🏡'},
        "105010300": {"n": 'триумфальная[ые] арка[и]', "i": 'building-alt1-15', "e": '🏡'},
        "105010400": {"n": 'дворец[цы]', "i": 'building-alt1-15', "e": '🏡'},
        "105010500": {"n": 'усадьба[ы]', "i": 'building-alt1-15', "e": '🏡'},
        "105010600": {"n": 'винодельни и пивоварни', "i": 'building-alt1-15', "e": '🏡'},
        "105010700": {"n": 'фермы', "i": 'building-alt1-15', "e": '🏡'},
        "105019900": {"n": 'сооружение[здания и сооружения]', "i": 'building-alt1-15', "e": '🏡'},
        "105010900": {"n": 'разрушенный[е] объект [ы]', "i": 'building-alt1-15', "e": '🏡'},
        "105020000": {"n": 'Небоскрёб [ы]', "i": 'building-alt1-15', "e": '🏙️'},
        "105030100": {"n": 'разводной[ые] мост [ы]', "i": 'building-alt1-15', "e": '🌉'},
        "105030200": {"n": 'каменный[е] мост [ы]', "i": 'building-alt1-15', "e": '🌉'},
        "105030300": {"n": 'виадук [и]', "i": 'building-alt1-15', "e": '🌉'},
        "105030400": {"n": 'древнеримский[е] мост [ы]', "i": 'building-alt1-15', "e": '🌉'},
        "105030500": {"n": 'пешеходный[е] мост [ы]', "i": 'building-alt1-15', "e": '🌉'},
        "105030600": {"n": 'акведук [и]', "i": 'building-alt1-15', "e": '🌉'},
        "105030700": {"n": 'висячий[е] мост [ы]', "i": 'building-alt1-15', "e": '🌉'},
        "105039900": {"n": 'мост[прочие мосты]', "i": 'building-alt1-15', "e": '🌉'},
        "105040100": {"n": 'смотровая[ые] башня[и]', "i": 'building-alt1-15', "e": '🗼'},
        "105040200": {"n": 'сторожевая[ые] башня[и]', "i": 'building-alt1-15', "e": '🗼'},
        "105040300": {"n": 'водонапорная[ые] башня[и]', "i": 'building-alt1-15', "e": '🗼'},
        "105040400": {"n": 'телебашня[и]', "i": 'building-alt1-15', "e": '🗼'},
        "105040500": {"n": 'часовая[ые] башня[и]', "i": 'building-alt1-15', "e": '🗼'},
        "105040600": {"n": 'колокольня[и]', "i": 'building-alt1-15', "e": '🗼'},
        "105040700": {"n": 'минарет [ы]', "i": 'building-alt1-15', "e": '🗼'},
        "105049900": {"n": 'башня[прочие башни]', "i": 'building-alt1-15', "e": '🗼'},
        "105050000": {"n": 'Маяк [и]', "i": 'building-alt1-15', "e": '🗼'},
        "106010000": {"n": 'Ж/д станция[и]', "i": 'industry-15', "e": '🏭'},
        "106020000": {"n": 'Фабрика[и]', "i": 'industry-15', "e": '🏭'},
        "106030000": {"n": 'Монетный[е] двор [ы]', "i": 'industry-15', "e": '🏭'},
        "106040000": {"n": 'Электростанция[и]', "i": 'industry-15', "e": '🏭'},
        "106050000": {"n": 'Плотина[ы]', "i": 'industry-15', "e": '🏭'},
        "106060000": {"n": 'Мельница[ы]', "i": 'industry-15', "e": '🏭'},
        "106070000": {"n": 'Заброшенная[ые] ж/д станция[и]', "i": 'industry-15', "e": '🏭'},
        "106080000": {"n": 'Заброшенная[ые] шахта[ы]', "i": 'industry-15', "e": '🏭'},
        "106090000": {"n": 'Шахта[ы]', "i": 'industry-15', "e": '🏭'},
        "106990000": {"n": 'Сооружение[Прочие сооружения]', "i": 'industry-15', "e": '🏭'},
        "107010000": {"n": 'Солнечные часы', "i": 'attraction-15', "e": '📸'},
        "107020000": {"n": 'Смотровая[ые] площадка[и]', "i": 'attraction-15', "e": '📸'},
        "107030000": {"n": 'Красная[ые] телефонная[ые] будка[и]', "i": 'attraction-15', "e": '📸'},
        "107040100": {"n": 'Туристическая[ие] достопримечательность[и]', "i": 'attraction-15', "e": '📸'},
        "107040200": {"n": 'Историческая[ие] достопримечательность[и]', "i": 'attraction-15', "e": '📸'},
        "201000000": {"n": 'парк [и] развлечений', "i": 'amusement-park-15', "e": '📸'},
        "202000000": {"n": 'парк [и] миниатюр', "i": 'amusement-park-15', "e": '📸'},
        "203000000": {"n": 'открытые бассейны и аквапарк [и]', "i": 'amusement-park-15', "e": '📸'},
        "204000000": {"n": 'американские горки', "i": 'amusement-park-15', "e": '📸'},
        "205000000": {"n": 'колесо[ёса] обозрения', "i": 'amusement-park-15', "e": '📸'},
        "299000000": {"n": 'аттракцион[прочие аттракционы]', "i": 'amusement-park-15', "e": '📸'},
        "301010000": {"n": 'горные лыжи', "i": 'tennis-15', "e": '📸'},
        "301020000": {"n": 'беговые лыжи', "i": 'tennis-15', "e": '📸'},
        "301990000": {"n": 'зимний спорт[другие зимние виды спорта]', "i": 'tennis-15', "e": '📸'},
        "302010000": {"n": 'дайвинг-центр [ы]', "i": 'tennis-15', "e": '📸'},
        "302020000": {"n": 'место[а] для погружений', "i": 'tennis-15', "e": '📸'},
        "302030000": {"n": 'затонувший[е] корабль[и]', "i": 'tennis-15', "e": '📸'},
        "303000000": {"n": 'скалодром [ы]', "i": 'tennis-15', "e": '📸'},
        "304000000": {"n": 'серфинг', "i": 'tennis-15', "e": '📸'},
        "305000000": {"n": 'кайтсерфинг', "i": 'tennis-15', "e": '📸'},
        "401000000": {"n": 'стрипклуб [ы]', "i": 'heart-15', "e": '📸'},
        "402000000": {"n": 'казино', "i": 'heart-15', "e": '📸'},
        "403000000": {"n": 'публичный[е] дом [а]', "i": 'heart-15', "e": '📸'},
        "404000000": {"n": 'ночной[ые] клуб [ы]', "i": 'heart-15', "e": '📸'},
        "405000000": {"n": 'алкоголь', "i": 'alcohol-shop-15', "e": '📸'},
        "406000000": {"n": 'love-отель[и]', "i": 'heart-15', "e": '📸'},
        "407000000": {"n": 'секс-шоп [ы]', "i": 'heart-15', "e": '📸'},
        "501010000": {"n": 'Прокат авто', "i": 'car-15', "e": '📸'},
        "501020000": {"n": 'Каршеринг', "i": 'car-15', "e": '📸'},
        "501030000": {"n": 'Автомойка[и]', "i": 'car-15', "e": '📸'},
        "501040000": {"n": 'Зарядная[ые] станция[и]', "i": 'fuel-15', "e": '📸'},
        "501050000": {"n": 'Прокат велосипедов', "i": 'bicycle-15', "e": '📸'},
        "501060000": {"n": 'Боатшеринг', "i": 'harbor-15', "e": '📸'},
        "501070000": {"n": 'Заправка[и]', "i": 'fuel-15', "e": '📸'},
        "502010000": {"n": 'Супермаркет [ы]', "i": 'grocery-15', "e": '📸'},
        "502020000": {"n": 'Продуктовый[е] магазин [ы]', "i": 'shop-15', "e": '📸'},
        "502030000": {"n": 'Товары для туризма', "i": 'commercial-15', "e": '📸'},
        "502040000": {"n": 'Торговый[е] центр [ы]', "i": 'commercial-15', "e": '📸'},
        "502050000": {"n": 'Рынок[ки]', "i": 'commercial-15', "e": '📸'},
        "502060000": {"n": 'Пекарня[и]', "i": 'bakery-15', "e": '📸'},
        "503010000": {"n": 'Ресторан [ы]', "i": 'restaurant-15', "e": '📸'},
        "503020000": {"n": 'Кафе', "i": 'cafe-15', "e": '📸'},
        "503030000": {"n": 'Фастфуд', "i": 'fast-food-15', "e": '📸'},
        "503040000": {"n": 'Фудкорт', "i": 'fast-food-15', "e": '📸'},
        "503050000": {"n": 'Паб [ы]', "i": 'beer-15', "e": '📸'},
        "503060000": {"n": 'Бар [ы]', "i": 'bar-15', "e": '📸'},
        "503070000": {"n": 'Пивной[ые] сад [ы]', "i": 'beer-15', "e": '📸'},
        "503080000": {"n": "Место[а] для пикника", "i": 'beer-15', "e": '📸'},
        "504010000": {"n": 'Банк [и]', "i": 'bank-15', "e": '📸'},
        "504020000": {"n": 'Банкомат [ы]', "i": 'bank-15', "e": '📸'},
        "504030000": {"n": 'Обмен валют', "i": 'bank-15', "e": '📸'},
        "601000000": {"n": 'Апартаменты', "i": 'lodging-15', "e": '📸'},
        "602000000": {"n": 'Гостевой[ые] дом [а]', "i": 'lodging-15', "e": '📸'},
        "603000000": {"n": 'Кемпинг [и]', "i": 'lodging-15', "e": '📸'},
        "604000000": {"n": 'Курортный[е] отель[и]', "i": 'lodging-15', "e": '📸'},
        "605000000": {"n": 'Мотель[и]', "i": 'lodging-15', "e": '📸'},
        "699000000": {"n": 'Отель[и]', "i": 'lodging-15', "e": '📸'},
        "607000000": {"n": 'Хостел [ы]', "i": 'lodging-15', "e": '📸'},
        "608000000": {"n": 'Вилла [Виллы и шале]', "i": 'lodging-15', "e": '📸'},
        "609000000": {"n": 'Горный[е] приют [ы]', "i": 'lodging-15', "e": '📸'},
        "610000000": {"n": 'Отель[и] для свиданий', "i": 'lodging-15', "e": '📸'},
        "700000000": {'n': 'камера[ы]'}
    },
    "groups": {
        "100000000": {"n": 'Интересные места', "i": 'park-15', "e": '🏝️'},
        "101000000": {"n": 'Природа', "i": 'park-15', "e": '🏝️'},
        "101010000": {"n": 'Острова', "i": 'park-15', "e": '🏝️'},
        "101020000": {"n": 'Природные источники', "i": 'park-15', "e": '💧'},
        "101030000": {"n": 'Геологические образования', "i": 'park-15', "e": '⛰️'},
        "101040000": {"n": 'Водоемы', "i": 'park-15', "e": '🌊'},
        "101050000": {"n": 'Пляжи', "i": 'park-15', "e": '🏖️'},
        "101060000": {"n": 'Заповедники', "i": 'park-15', "e": '🏞️'},
        "102000000": {"n": 'Культура', "i": 'town-hall-15', "e": '🏛️'},
        "102010000": {"n": 'Музеи', "i": 'town-hall-15', "e": '🏛️'},
        "102010300": {"n": 'музеи науки и техники', "i": 'town-hall-15', "e": '🏛️'},
        "102020000": {"n": 'Театры и концертные залы', "i": 'town-hall-15', "e": '🎭'},
        "102030000": {"n": 'Городская среда', "i": 'town-hall-15', "e": '⛲'},
        "103000000": {"n": 'История', "i": 'castle-15', "e": '🛡️'},
        "103010000": {"n": 'Исторические места', "i": 'castle-15', "e": '🛡️'},
        "103020000": {"n": 'Оборонительные сооружения', "i": 'castle-15', "e": '🏰'},
        "103030000": {"n": 'Монументы и мемориалы', "i": 'castle-15', "e": '🗿'},
        "103040000": {"n": 'Археология', "i": 'castle-15', "e": '🏺'},
        "103050000": {"n": 'Захоронения', "i": 'castle-15', "e": '⚱️'},
        "104000000": {"n": 'Религия', "i": 'place-of-worship-15', "e": '⛪'},
        "104010000": {"n": 'церкви', "i": 'place-of-worship-15', "e": '⛪'},
        "105000000": {"n": 'Архитектура', "i": 'building-alt1-15', "e": '🏡'},
        "105010000": {"n": 'Историческая архитектура', "i": 'building-alt1-15', "e": '🏡'},
        "105030000": {"n": 'Мосты', "i": 'building-alt1-15', "e": '🌉'},
        "105040000": {"n": 'Башни', "i": 'building-alt1-15', "e": '🗼'},
        "106000000": {"n": 'Индустриальные объекты', "i": 'industry-15', "e": '🏭'},
        "107000000": {"n": 'Разное', "i": 'attraction-15', "e": '📸'},
        "107040000": {"n": 'Неклассифицированная[ые] достопримечательность[и]', "i": 'attraction-15', "e": '📸'},
        "200000000": {"n": 'Развлечения', "i": 'amusement-park-15', "e": '📸'},
        "300000000": {"n": 'Спорт', "i": 'tennis-15', "e": '📸'},
        "301000000": {"n": 'зимний спорт', "i": 'tennis-15', "e": '📸'},
        "302000000": {"n": 'дайвинг', "i": 'tennis-15', "e": '📸'},
        "400000000": {"n": '18+', "i": 'heart-15', "e": '📸'},
        "500000000": {"n": 'Туристическая инфраструктура', "i": 'car-15', "e": '📸'},
        "501000000": {"n": 'Транспорт', "i": 'car-15', "e": '📸'},
        "502000000": {"n": 'Магазины', "i": 'grocery-15', "e": '📸'},
        "503000000": {"n": 'Питание', "i": 'restaurant-15', "e": '📸'},
        "504000000": {"n": 'Банковские услуги', "i": 'bank-15', "e": '📸'},
        "600000000": {"n": 'Размещение', "i": 'lodging-15', "e": '📸'},
    }
};

let mega_names = {
    'interesting_places' : '100000000',
    'unclassified_objects' : '107040000',
    'view_points' : '107020000',
    'amusements' : '200000000',
    'sport' : '300000000',
    'winter_sports' : '301000000',
    'adult' : '400000000',
    'tourist_facilities' : '500000000',
    'foods' : '503000000',
    'shops' : '502000000',
    'accomodations' : '600000000',
    'cameras' : '700000000',
    'beaches' : '101050000',
    'waterfalls': '101040800',
    'caves': '101030300',
    'canyons': '101030400',
    'volcanoes': '101030200' //Volcano
};

let base_layer_names = {
    aerodrome_label :{
        'airport' : {n: 'Аэропорт [ы]', i:'airport-15'},
        'international_airport': {n: 'Международный [е] аэропорт [ы]', i:'airport-15'}
    },
    poi : {
        'airport': {n: 'Аэропорт [ы]', i:'airport-15'},
        'train_station': {n: 'Станция[и]', i:'rail-15'},
        'bus_stop': {n: 'Автобусная[ые] остановка[и]', i:'bus-15'},
        'tram_stop': {n: 'Трамвайная[ые] остановка[и]', i:'rail-light-15'},
        'subway_entrance': {n: 'Вход [ы] в метро', i:'rail-metro-15'},
        'harbor_marina': {n: 'Порт [ы]', i:'harbor-15'},
        'post_office': {n: 'Почта', i:'post-15'},
        'information': {n: 'Информация', i:'information-15'},
        'police': {n: 'Полиция', i:'police-15'},
        'hospital': {n: 'Больницы[а]', i:'doctor-15'},
        'pharmacy': {n: 'Аптека[и]', i:'hospital-15'},
        'shower': {n: 'Душ', i:'water-15'},
        'toilets': {n: 'Туалет [ы]', i:'toilets-15'},
        'parking': {n: 'Парковка[и]', i:'parking-15'}
    },
    pois : {
        'airport': {n: 'Аэропорт [ы]', i:'airport-15'},
        'train_station': {n: 'Станция[и]', i:'rail-15'},
        'bus_stop': {n: 'Автобусная[ые] остановка[и]', i:'bus-15'},
        'tram_stop': {n: 'Трамвайная[ые] остановка[и]', i:'rail-light-15'},
        'subway_entrance': {n: 'Вход [ы] в метро', i:'rail-metro-15'},
        'harbor_marina': {n: 'Морской[ие] порт [ы]', i:'harbor-15'},
        'post_office': {n: 'Почта', i:'post-15'},
        'information': {n: 'Информация', i:'information-15'},
        'police': {n: 'Полиция', i:'police-15'},
        'hospital': {n: 'Больницы[а]', i:'doctor-15'},
        'pharmacy': {n: 'Аптека[и]', i:'hospital-15'},
        'shower': {n: 'Душ', i:'water-15'},
        'toilets': {n: 'Туалет [ы]', i:'toilets-15'},
        'parking': {n: 'Парковка[и]', i:'parking-15'}
    },
    place:{
        'city': {n: 'Город [а]', i:'building-alt1-15'},
        'town': {n: 'Город [а]', i:'building-alt1-15'},
        'village': {n: 'Поселок[ки]', i:'building-alt1-15'},
        'hamlet': {n: 'Поселок[ки]', i:'building-alt1-15'},
        'locality': {n: 'Местность[и]', i:'building-alt1-15'}
    }
};

exports.mega_names = mega_names;
exports.base_layer_names = base_layer_names;
exports.layer_names = layer_names;
exports.layer_catalog = layer_catalog;

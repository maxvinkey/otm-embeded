import 'babel-polyfill';
import {JL} from 'jsnlog';
import {config, serverHost} from "./config";
import Modernizr from 'modernizr';


var mapboxGlSupported = require('@mapbox/mapbox-gl-supported');
import Fingerprint2 from 'fingerprintjs2'

function init() {
    let parent_url = (window.location != window.parent.location) ?
        document.referrer :
        document.location.href;

    config.info = {
        gl: mapboxGlSupported() ? 'true' : 'false',
        url: encodeURIComponent(document.URL),
        href: parent_url ? encodeURIComponent(parent_url) : ""
    };

    if (window.requestIdleCallback) {
        requestIdleCallback(function () {
            Fingerprint2.get(function (components) {
                config.setFingerPrint(components);
            })
        })
    } else {
        setTimeout(function () {
            Fingerprint2.get(function (components) {
                config.setFingerPrint(components);
            })
        }, 500)
    }

}

$(document).ready(function () {
    let bodyStyles = document.body.style;
    bodyStyles.setProperty('--color-main-theme', config.getThemeColor());

    let appender = JL.createAjaxAppender("main");
    appender.setOptions({
        "url": serverHost + "/jslog",
        "beforeSend": function (xhr, json) {
            if (config.fingerprint === 'unknown'){
                let env = {
                    ver : navigator.appVersion,
                    agent: navigator.userAgent,
                    browser: navigator.appName
                };
                if (json && json.lg){
                    for (let n = 0; n < json.lg.length; ++n){
                        json.lg[n].env = env;
                    }
                }
            }
            xhr.setRequestHeader('fingerprint', config.fingerprint);
        }
    });

    JL().setOptions({
        "appenders": [appender,JL.createConsoleAppender('console')]
    });

    function runAPP(isGL){
        if (isGL) {
            JL().warn("Init GL");
            import(/* webpackChunkName: "mapbox" */ './renderMapboxGL').then(mapbox => {
                    config.safety_mode = false;
                    try {
                        mapbox.default();
                    }catch(reason) {
                        JL().error("Error starting MapBoxGL", reason);
                    }
                }
            ).catch(function (reason) {
                JL().error("Error loading MapBoxGL", reason);
            });
        } else {
            JL().warn("Init NOGL");
            import(/* webpackChunkName: "leaflet" */ './renderMapboxJS').then(leaflet => {
                    config.safety_mode = true;
                    try {
                        leaflet.default();
                    }catch(reason) {
                        JL().error("Error starting LeafLet", reason);
                    }
                }
            ).catch(function (reason) {
                JL().error("Error loading LeafLet", reason);
            });

        }
    }

    function getParameterByName(name) {
        try {
            let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }catch(e){
            return null;
        }
    }

    try {
        init();

        let lang = getParameterByName('lang');
        if (!lang){
            lang = 'ru';
        }
        config.lang = lang;

        let index = getParameterByName('index');
        if (!index){
            index = 0;
        }
        config.homeFeatureIndex = index;
        config.minimap = false;
        config.mini_redirect = false;

        let mode = getParameterByName('mode');
        if (mode){
            if (mode === 'mini') {
                config.minimap = true;
            }else if (mode === 'mini_redirect') {
                config.minimap = true;
                config.mini_redirect = true;
            }else if (mode === 'mini_base_redirect') {
                config.minimap = true;
                config.mini_redirect = true;
                config.only_base_map = true;
            }
        }
        //console.log(config);
        if (config.getLang() === 'ru') {
            import(/* webpackChunkName: "ru" */ './locale.ru').then(locale => {
                    try {
                        return locale.default();
                    } catch (reason) {
                        JL().error("Error starting Locale.ru", reason);
                    }
                }
            ).catch(function (reason) {
                JL().error("Error loading Locale.ru", reason);
            }).then(locale => runAPP(mapboxGlSupported()),{});
        }else{
            import(/* webpackChunkName: "en" */ './locale.en').then(locale => {
                    try {
                        locale.default();
                    }catch(reason) {
                        JL().error("Error starting Locale.en", reason);
                    }
                }
            ).catch(function (reason) {
                JL().error("Error loading Locale.en", reason);
            }).then(locale => runAPP(mapboxGlSupported()),{});
        }

    } catch(e) {
        JL().fatalException("Exception", e);
    }

});
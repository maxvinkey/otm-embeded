import * as LANG from "./lang.en";
import {layer_names, mega_names, base_layer_names, layer_catalog} from "./catalog.en";

export default function set() {
    window.LOC = {txt : LANG.langd,
        layer_names : layer_names,
        base_layer_names : base_layer_names,
        mega_names : mega_names,
        layer_catalog :layer_catalog
    };
}
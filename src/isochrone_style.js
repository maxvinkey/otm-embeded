const colorSchemes = {
    altColor: [
        [150, '#f54e5f'],
        [300, '#56b881'],//10
        [900, '#f9886c'],//20
        [1800, '#f54e5e'],//40
        [2700, '#3887be'],//50
        [3450, '#4a4a8b']//60
    ]
};

const style = [{
    'id': 'quantized',
    'type': 'line',
    'source': 'isochrones',
    'paint': {
        'line-color': {
            "property": "time",
            "type": 'interval',
            "stops": colorSchemes.altColor
        },
        'line-dasharray': [2, 1],
        'line-width': {
            base: 1,
            "stops": [[10, 1.5], [22, 15]]
        }
    },

},
    {
        'id': 'quantized-label',
        'type': 'symbol',
        'source': 'isochrones',
        'layout': {
            'text-field': '{minutes}' + LOC.txt.unitNames.minutes,
            "text-font": [
                "Open Sans SemiBold",
                "Arial Unicode MS Regular"
            ],
            'symbol-placement': 'line',
            'text-allow-overlap': true,
            'text-padding': 1,
            'text-max-angle': 90,
            'text-size': {
                base: 1.2,
                "stops": [[8, 12], [22, 30]]
            },
            'text-letter-spacing': 0.1
        },
        'paint': {
            'text-halo-color': '#F8F7EF',
            'text-color': {
                "property": "time",
                "type": 'interval',
                "stops": colorSchemes.altColor
            },
            'text-halo-width': 12
        }
    }
];

export default style;

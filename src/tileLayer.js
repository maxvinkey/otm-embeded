import {getClusterByID, getFeatureByID, throttle, getMapBoxGLHashString} from "./lib/utils";
import {pois_layers} from "./layers";
const EventEmitter = require( 'events' );
import {
    config,serverHost
} from "./config";

export default class TileLayer extends EventEmitter{

    constructor(map, tags){
        super();
        this.map = map;
        this.hash = "";
        this.tags = tags;
        this.onChangeExtent = this.onChangeExtent.bind(this);
        let self = this;
        this.map.on('moveend', () => {
            self.onChangeExtent();
        });
        this.onSourceData = this.onSourceData.bind(this);
        this.map.on('sourcedata', this.onSourceData);
        this.map.on('resize', () => {
            self.onChangeExtent();
        });
    }
    onSourceData(e) {
        //console.log("source: " + e.sourceId + " " + e.isSourceLoaded + "\n");
        if (e.isSourceLoaded && e.sourceId != "directions") {
            this.updateList(this.getHashString(), true);
        }
    }
    onChangeExtent(){
        throttle(this.updateList(this.getHashString(), false), 1000, {trailing: true});
    }
    updateList(cause, isForced){
        if (this.hash != cause || isForced){
            this.hash = cause;
            //console.log("update: " + cause + " changed\n");
            this.emit('changed', {});
        }
        //console.log("update: " + cause + " no\n");
    }
    getHashString() {
        return getMapBoxGLHashString(this.map);
    }
    static rebuildPOI(map, tags){
        if (map.getLayer("clusters")) {

            map.removeLayer('all_pois_label');
            map.removeLayer('all_pois_icons');
            map.removeLayer('all_pois_label_after');
            map.removeLayer('all_pois');
            map.removeSource('otmpoi');
            if (config.heatmap) {
                map.removeLayer('hm_shopping');
                map.removeLayer('hm_food');
                map.removeSource('heat');

                map.removeLayer('hm_pois');
                map.removeSource('heat_pois');
            }

            map.removeLayer('clusters');
            map.removeLayer('clusters_label');
            map.removeSource('clusters');
        }

        if (map.getLayer("home_icons")) {
            map.removeLayer('home_icons');
            map.removeLayer('home-point-label');
            map.removeSource('hotel');
        }

        let curConfig = config.getBaseCurrentMap();
        let before = curConfig.beforeLayer;
        let icon = curConfig.iconPrefix;


        if (tags !== '') {
            map.addSource('clusters', {
                type: "vector",
                "url": serverHost + "/src/" + config.getOTMLang() + "?l=c&&r=1&h=0&k=" + tags
            });
            map.addSource('otmpoi', {
                type: "vector",
                "url": serverHost + "/src/" + config.getOTMLang() + "?l=p&r=1&h=0&k=" + tags
            });

            if (config.heatmap) {
                map.addSource('heat', {
                    type: "vector",
                    "url": serverHost + "/src/ru?l=h&r=0&h=0&k=503000000,502000000",
                });
                map.addSource('heat_pois', {
                    type: "vector",
                    "url": serverHost + "/src/ru?l=h&r=1&h=0&k=100000000",
                });
            }

            pois_layers.pois_label.id = "all_pois_label";
            pois_layers.pois_label["minzoom"] = 14;
            pois_layers.pois_label["maxzoom"] = 17;
            pois_layers.pois_label["layout"]["icon-allow-overlap"] = true;
            pois_layers.pois_label["layout"]["text-font"] = curConfig.text_font;
            pois_layers.pois_label["paint"] = pois_layers.text_paint[curConfig.text_paint];

            if (before){
                if (config.heatmap) {
                    //map.addLayer(pois_layers.shop_heatmap, before);
                    //map.addLayer(pois_layers.food_heatmap, before);
                    map.addLayer(pois_layers.pois_heatmap, before);
                }
                map.addLayer(pois_layers.pois_label, before);
                map.addLayer(pois_layers.pois_point, before);

            }else{
                if (config.heatmap) {
                    //map.addLayer(pois_layers.shop_heatmap);
                    //map.addLayer(pois_layers.food_heatmap);
                    map.addLayer(pois_layers.pois_heatmap);
                }
                map.addLayer(pois_layers.pois_label);
                map.addLayer(pois_layers.pois_point);
            }

            pois_layers.pois_icons["layout"]["icon-image"] = icon;
            if (before) {
                map.addLayer(pois_layers.pois_icons, before);
            }else{
                map.addLayer(pois_layers.pois_icons);
            }

            pois_layers.pois_label.id = "all_pois_label_after";
            pois_layers.pois_label["minzoom"] = 17;
            pois_layers.pois_label["maxzoom"] = 24;
            pois_layers.pois_label["layout"]["icon-allow-overlap"] = false;
            pois_layers.pois_label["layout"]["text-font"] = curConfig.text_font;
            pois_layers.pois_label["paint"] = pois_layers.text_paint[curConfig.text_paint];

            if (before) {
                map.addLayer(pois_layers.pois_label, before);
            }else{
                map.addLayer(pois_layers.pois_label);
            }

            map.addLayer(pois_layers.clusters);
            map.addLayer(pois_layers.clusters_label);
        }

        map.addSource('hotel', {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: config.getHomeFeatures()

                }
            }
        );
        pois_layers["home_icons"]["layout"]["icon-image"] = icon;
        map.addLayer(pois_layers["home_icons"]);
        map.addLayer(pois_layers["home-point-label"]);
    }
    setCategors(categors){
        this.tags = categors;
        this.rebuild();
    }
    rebuild(){
        TileLayer.rebuildPOI(this.map, this.tags);
    }
    getFeature(xid, callback) {
        if ((xid +'').startsWith('REG')){
            getClusterByID(xid, function (feature) {
                callback(feature);
            });
        }else{
            getFeatureByID(xid, function (feature) {
                callback(feature);
            });
        }
    }
};

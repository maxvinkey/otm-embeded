import {
    getFeatureLayerName
} from "./lib/utils";
import {
    getADDR,
    getOSMP
} from "./lib/utils.promise";
import RadioButton from "./radioButton";
import {JL} from 'jsnlog';
const EventEmitter = require('events');
import PerfectScrollbar from 'perfect-scrollbar';
import {
    config, Layouts
} from "./config";
import {setLayout} from "./layout";

if (!('remove' in Element.prototype)) {
    Element.prototype['remove'] = function () {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

export default class ObjectCard extends EventEmitter {

    constructor() {
        if (ObjectCard.hasOwnProperty('singleton'))
            return ObjectCard.singleton;
        super();
        Object.defineProperty(ObjectCard, 'singleton',{
            value: this,
            enumerable:false,
            writable:false,
            configurable:false
        });
        this.ps = new PerfectScrollbar('#card-content', {
            wheelSpeed: 2,
            wheelPropagation: true,
            scrollingThreshold: null,
            suppressScrollX: true,
            swipeEasing: false
        });
        let self = this;
        window.addEventListener("resize", function updateScroll() {
            self.updateScrollbar();
        });
        $("#card-content").on("click", ".objectCard__backToCards", function backToCards() {
            $(".objectCard").hide();
            self.emit('close');
            $(".geocoderInput").removeClass('small');
        }).on("click", ".objectCard__showOnMap", function showOnMap() {
            setLayout(Layouts.MAP);
            $("#card-content").on("click", ".objectCard__backToCards", function () {
                $("#card-content").off("click", ".objectCard__showOnMap", showOnMap);
            });
        });
    }

    static getInstance(){
        return new ObjectCard();
    }

    setFeature(feature) {
        this.id = feature.id;
        this.feature = feature;
        this.properties = feature.properties;
        this.layerName = getFeatureLayerName(feature);
        this.name = feature.properties.name;
        if (!this.name) {
            this.name = layerName;
        }
        this.walking = {};
        this.bicycle = {};
        this.otmUrl = 'https://opentripmap.com/' + config.getOTMLang() + '/card/' + this.properties.xid;
    }
    updateScrollbar() {
        let self = this;
        setTimeout(function () {
            self.ps.update();
        }, 1000);
    }
    //Скоро закроется //Закрыто getStateString
    _renderOpeningHours() {
        if (this.properties.opening_hours) {
            import(/* webpackChunkName: "hours" */ './hours').then(opening_hours => {
                let oh = opening_hours.default(this.properties.opening_hours);
                let arr = oh.val.split(';');
                let days = [];
                let hours = [];
                for (let i = 0; i < arr.length; i++) {
                    let item = arr[i].trim();
                    let n = item.indexOf(' ');
                    if (n !== -1) {
                        let day = $('<span />', {
                            text: `${item.substring(0, n)}`
                        });
                        $('<br>').appendTo(day);
                        days.push(day);
                        let hour = $('<span />', {
                            text: `${item.substr(n + 1)}`
                        });
                        $('<br>').appendTo(hour);
                        hours.push(hour);
                    } else {
                        let day = $('<span />', {
                            text: `${item}`
                        });
                        $('<br>').appendTo(day);
                        days.push(day);
                        $('<br>').appendTo(hour);
                        hours.push(hour);
                    }
                }
                let objectOpenHours = $('<div />', {
                    class: 'objectOpenHours'
                });
                let objectOpenHours__title = $('<div />', {
                    class: 'objectOpenHours__title',
                    text: `${oh.state}`
                });
                let clock = $('<i />', {
                    class: 'fas fa-clock'
                });
                let chevron = $('<i />', {
                    class: 'fas fa-chevron-down'
                });
                let objectOpenHoursDropdown = $('<div />', {
                    class: 'objectOpenHoursDropdown'
                });
                let objectOpenHoursDropdown__days = $('<div />', {
                    class: 'objectOpenHoursDropdown__days'
                });
                days.forEach(function (element) {
                    element.appendTo(objectOpenHoursDropdown__days);
                });
                let objectOpenHoursDropdown__status = $('<div />', {
                    class: 'objectOpenHoursDropdown__status'
                });
                hours.forEach(function (element) {
                    element.appendTo(objectOpenHoursDropdown__status);
                });
                $(objectOpenHours__title).click(function () {
                    $(objectOpenHoursDropdown).toggleClass('active');
                });
                clock.prependTo(objectOpenHours__title);
                chevron.appendTo(objectOpenHours__title);
                objectOpenHours__title.appendTo(objectOpenHours);
                objectOpenHoursDropdown__days.appendTo(objectOpenHoursDropdown);
                objectOpenHoursDropdown__status.appendTo(objectOpenHoursDropdown);
                objectOpenHoursDropdown.appendTo(objectOpenHours);
                let objectAddressAndContacts = document.querySelector('.objectAddressAndContacts');
                objectOpenHours.appendTo(objectAddressAndContacts);
                }
            ).catch(function (reason) {
                JL().error("Error loading MapBoxGL", reason);
            });
        }
        return "";
    }
    show() {
        $(".geocoderInput").addClass('small');
        $(".objectCard")
            .attr("data-xid", this.properties.xid)
            .html(this.renderCard())
            .show();
        let self = this;
        RadioButton.add(
            "#objectRouteWalking",
            function () {
                self.emit('route', self.walking.geojson);
            }, "#objectRouteBicycle",
            function () {
                self.emit('route', self.bicycle.geojson);
            });
        this._renderOpeningHours();
        this._setOSMP();
        this._renderAddress();
        this.updateScrollbar();
        this.emit('open', {
            id: this.id,
            source: this.feature.source_name,
            sourceLayer: this.feature.source_layer
        });
    }
    _renderDescription() {
        if (!this.feature.properties.descr) {
            this.feature.properties.descr = this.feature.properties["descr:" + config.getLang()];
        }

        if (this.properties.descr) {
            return `<div class="objectDescription__title">${LOC.txt.description}</div>
                     <div class="objectDescription__text">${this.properties.descr}</div>`;
        }
        return "";
    }
    _renderPreview() {
        if (this.properties.preview) {
            //mapbox не понимает вложенный json
            if (typeof this.properties.preview === 'string' || this.properties.preview instanceof String){
                this.properties.preview = JSON.parse(this.properties.preview);
            }
            if (config.getWidth() <= 768) {
                let vh = window.innerHeight * 0.01;
                return `<img class="objectCard__image" style="max-height: ${vh * 50}px;" src="${this.properties.preview.source}">`;
            } else {
                return `<img class="objectCard__image" src="${this.properties.preview.source}">`;
            }
        }
        $(".geocoderInput").removeClass('small');
        return `<div class="objectCard__image_none"></div>`;
    }
    _inactiveRoute(id){
        let block = document.querySelector(id + " > div");
        block.innerHTML = LOC.txt.routing_error;
        block.classList.add('objectRoute__distanceInactive');
        block.classList.remove('active');
        block.classList.remove('objectRoute__distance');

        let block2 = document.querySelector(id);
        block2.classList.remove('active');
        block2.classList.add('objectRouteInactive');
        block2.classList.remove('objectRoute');

        let block3 = document.querySelector(id + " > i");
        block3.style.color = "#444";
        block3.style.fontSize = "17px";
        block3.style.padding = "8px 4px";//тольуо у пешего

        let block4 = block2.parentNode;
        block4.classList.add('objectRoutes_inactive');
    }
    setWalkingRoute(summary) {
        this.walking = summary;
        if (this.feature.id == config.getHomeFeature().id) {
            let objectRoutes = document.querySelector(".objectRoutes");
            objectRoutes.remove();
        } else {
            //код ниже просто ременное решение, пока нет дизайна
            if ((summary.time == "-") && (summary.dist == "-")) {
                this._inactiveRoute("#objectRouteWalking");
            } else {
                if (RadioButton.isActive("#objectRouteWalking")) {
                    //здесь строка ниже нужна, чтобы отрисовать пеший маршрут.
                    //Почему в аналогичном коде ниже для велосипедного маршрута эта строка не нужна?
                    this.emit('route', this.walking.geojson);
                }
                let block = document.querySelector("#objectRouteWalking > div");
                block.innerHTML = `${summary.time}<br>${summary.dist}`;
            }
        }
    }
    setBicycleRoute(summary) {
        if (this.feature.id != config.getHomeFeature().id) {
            this.bicycle = summary;
            if ((summary.time == "-") && (summary.dist == "-")) {
                this._inactiveRoute("#objectRouteBicycle");
            } else {
                //Вот здесь. Три строки ниже вообще лишние, а почему?
                // if (RadioButton.isActive("#objectRouteBicycle")) {
                //     this.emit('route', this.bicycle.geojson);
                // }
                let block = document.querySelector("#objectRouteBicycle > div");
                block.innerHTML = `${summary.time}<br>${summary.dist}`;
            }
        }
    }
    _setAddress(address) {
        let addressDiv = document.querySelector(".objectAddressAndContacts__address");
        if (!addressDiv) {
            return;
        }
        if (address) {
            addressDiv.innerHTML = `<i class="fas fa-map-marker-alt"></i>${address}`;
        } else {
            addressDiv.remove();
        }
    }
    _renderAddress() {
        if (!this.feature.properties.address){
            this.feature.properties.address = this.feature.properties["address:"+config.getLang()];
        }
        if (this.feature.properties.address){
            this._setAddress(this.feature.properties.address)
        }else{
            let that = this;
            getADDR(that.feature).then(function (addr) {
                if (addr) {
                    let address = '';
                    if (addr.road) {
                        address = `${addr.road}`;
                        if (addr.house_number) {
                            let houseNum = (addr.house_number).replace(/\s/g, '');
                            address += `,&nbsp;${houseNum}`;
                        }
                    }
                    that.feature.properties.address = address;
                }
                that._setAddress(that.feature.properties.address);
            });
        }
    }
    _setPhone(){
        let phoneDiv = document.querySelector(".objectAddressAndContacts__telephone");
        if (!phoneDiv) {
            return;
        }
        if (this.feature.properties.phone) {
            phoneDiv.innerHTML = `<i class="fas fa-phone"></i>${this.feature.properties.phone}`;
        } else {
            phoneDiv.remove();
        }
    }
    _setWebSite(){
        let websiteDiv = document.querySelector(".objectAddressAndContacts__website");
        if (!websiteDiv) {
            return;
        }
        if (!this.feature.properties.url) {
            this.feature.properties.url = this.feature.properties["url:" + config.getLang()];
        }
        if (this.feature.properties.url) {
            let siteName = this.feature.properties.url.replace(/(^\w+:|^)\/\//, '').split(/[/?#]/)[0];
            websiteDiv.innerHTML = `<i class="fas fa-globe"></i><a target="_blank" class="link logged_link" data-id="website" href="${this.feature.properties.url}">${siteName}</a>`;
        } else {
            websiteDiv.remove();
        }
    }
    _setOSMP() {
        if (this.feature.properties.phone && this.feature.properties.url){
            this._setWebSite();
            this._setPhone();
            return;
        }
        let that = this;
        getOSMP(this.feature).then(function () {
            that._setWebSite();
            that._setPhone();
        });
    }
    renderCard() {
        //let address = this.properties.address ? (`${this.properties.address.road}, ${this.properties.address.house_number}`) : "Ленинградское шоссе, 3";
        //let phone = this.properties.phone ? this.properties.phone : "+7-81379-37275";

        return `${this._renderPreview()}
        <a class="objectCard__backToCards"><i class="fas fa-arrow-left"></i></a>
        <h4 class="objectCard__title">${this.name}</h4>
        <h5 class="objectCard__subtitle">${this.layerName}</h5>
        <div class="objectRoutes objectRoutes_decor">
             <a id="objectRouteWalking" class="objectRoute active">
                <i class="fas fa-walking objectRoute__firstIcon active"></i>
                <div class="objectRoute__distance active">---<br>---</div>
             </a>
             <a id="objectRouteBicycle" class="objectRoute">
                <i class="fas fa-bicycle objectRoute__secondIcon"></i>
                <div class="objectRoute__distance">---<br>---</div>
             </a>        
        </div>
        <div class="objectAddressAndContacts">
            <div class="objectAddressAndContacts__address"></div>
            <div class="objectAddressAndContacts__telephone"></div>
            <div class="objectAddressAndContacts__website"></div>
        </div>
        <div class="objectDescription">
            ${this._renderDescription()}
            <a class="objectDescription__linkToOTM logged_link" data-id="otm" target="_blank"  href="${this.otmUrl}">${LOC.txt.view_details}</a>
        </div>
        <div id="showOnMap" class="objectCard__showOnMap">${LOC.txt.view_on_map}</div>`
    }
}
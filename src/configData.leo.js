export let configData = {
    basemaps: {
        'basemapMap_tiler_bright': {
            basemapNameRus: 'txt.map',
            beforeLayer: "place-other",
            iconPrefix: "otm_{icon}",
            style: './json/style.tiler.bright.json',
            text_paint : 'normal',
            text_font : ['Noto Sans Regular'],
            copyrights : ['otm','maptiler','osm']
        },
        'basemapHybrid': {
            basemapNameRus: 'txt.hybrid',
            iconPrefix: "{icon}",
            style: './json/style.hyb.json',
            text_paint : 'transparent',
            text_font : ['Noto Sans Regular'],
            copyrights : ['otm','maptiler','mapbox','osm']
        },
        'basemapSatellite': {
            basemapNameRus: 'txt.sat',
            iconPrefix: "{icon}",
            style: './json/style.sat.json',
            text_paint : 'transparent',
            text_font : ['Noto Sans Regular'],
            copyrights : ['otm','maptiler','mapbox','osm']
        }
    },
    tiles: [
        {
            z: 12,
            xmin : 2474,
            xmax: 2476,
            ymin: 1313,
            ymax: 1314
        },
        {
            z: 13,
            xmin : 4949,
            xmax: 4952,
            ymin: 2626,
            ymax: 2627
        },
        {
            z: 14,
            xmin : 9900,
            xmax: 9901,
            ymin: 5253,
            ymax: 5254
        }
    ],
    themeColor : '#b6804c',
    currentBaseMap : 'basemapMap_tiler_bright',
    layers :
        [
            {id:"101000000", enable: true},//'Природа'
            {id:"102010000", enable: true},//'Музеи'
            {id:"103000000", enable: true},//'История'
            {id:"104000000", enable: true},//'Религия'
            {id:"107000000", enable: true},//"Разное"
            {id:"200000000", enable: false},//'Развлечения'
            {id:"300000000", enable: true},//'Спорт'
            {id:"502000000", enable: true},//'Магазины'
            {id:"503000000", enable: true},//'Питание'
            //{name: 'Размещение', i: 'lodging-15', id: "600000000"}
        ],
    //tags : "101000000,102010000,103000000,104000000,107000000,300000000,502000000,503000000",
    homeFeatures : [
        {
            id : 780223,
            lat: 54.082824,
            lon: 37.555381,
            zoom: 13,
            type: "Feature",
            geometry: {type: "Point", coordinates: [37.555381, 54.082824]},
            properties:{
                icon: "lodging-15",
                id: 780223,
                ispt: true,
                lat_max: 54.082824,
                lat_min: 54.082824,
                lon_max: 37.555381,
                lon_min: 37.555381,
                mainkind: "699000000",
                kind: "600000000,699000000",
                name: "Загородный клуб «ЛевЪ»",
                osm_id: 6339260446,
                popular: 2,
                rating: 9,
                xid: "H2913759",
                burl: "https://www.booking.com/hotel/ru/tolstoi-zaghorodnyi-klub.html",
                preview: {
                    width: 400,
                    source: "https://data.opentripmap.com/images/user/leo_178136132/400.jpg",
                    height: 267
                },
                image: "https://data.opentripmap.com/images/user/leo_178136132/original.jpg",
                address : "Тульская область, п.Лесной, 9/1",
                phone : "+7&nbsp;(4872)&nbsp;33-82-42",
                url: "http://lev-club71.ru",
                descr :  "В самом сердце леса, в 2 км от Ясной поляны, расположен уникальный гостинично-ресторанный комплекс — Загородный клуб «ЛевЪ». Своим гостям «ЛевЪ» предоставляет уникальный шанс на полноценный загородный отдых, вдали от городского шума и суеты. Здесь, не далеко от города-героя Тулы, в окружении дикого русского леса, ваши выходные превратятся в настоящую зимнюю сказку!"
            }
        }
    ]
};

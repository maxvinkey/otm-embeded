import * as LANG from "./lang.ru";
import {layer_names, mega_names, base_layer_names, layer_catalog} from "./catalog.ru";

export default function set() {
    window.LOC = {txt : LANG.langd,
            layer_names : layer_names,
            base_layer_names : base_layer_names,
            mega_names : mega_names,
            layer_catalog :layer_catalog
    };
}

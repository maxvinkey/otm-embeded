export let configData = {
    basemaps: {
        'basemapMap_tiler_topo_lite': {
            basemapNameRus: "txt.map",
            beforeLayer: "place_other",
            iconPrefix: "otm_{icon}",
            style: 'json/style.tiler.topo.lite.json',
            text_paint : 'normal',
            text_font : ['Noto Sans Regular'],
            copyrights : ['otm','maptiler','osm']
        },
        'basemapHybrid': {
            basemapNameRus: "txt.hybrid",
            iconPrefix: "{icon}",
            style: 'json/style.hyb.json',
            text_paint : 'transparent',
            text_font : ['Noto Sans Regular'],
            copyrights : ['otm','maptiler','mapbox','osm']
        },
        'basemapSatellite': {
            basemapNameRus: "txt.sat",
            iconPrefix: "{icon}",
            style: 'json/style.sat.json',
            text_paint : 'transparent',
            text_font : ['Noto Sans Regular'],
            copyrights : ['otm','maptiler','mapbox','osm']
        }
    },
    tiles: [
        {
            z: 14,
            xmin: 9903,
            xmax: 9904,
            ymin: 5120,
            ymax: 5122
        }
    ],
    themeColor : '#3c5976',
    currentBaseMap : 'basemapMap_tiler_topo_lite',
    layers :
        [
            {id:"101000000", enable: true},//{name: 'Природа', i: 'park-15', id: "101000000"},
            {id:"102010000", enable: true},//{name: 'Музеи', i: 'town-hall-15', id: "102010000"},
            {id:"103000000", enable: true},//{name: 'История', i: 'castle-15', id: "103000000"},
            {id:"104000000", enable: true},//{name: 'Религия', i: 'place-of-worship-15', id: "104000000"},
            {id:"107000000", enable: true},//{name: "Разное", i: 'attraction-15', id: "107000000"},
            {id:"200000000", enable: false},//{name: 'Развлечения', i: 'amusement-park-15', id: "200000000"},
            {id:"300000000", enable: true},//{name: 'Спорт', i: 'tennis-15', id: "300000000"},
            {id:"502000000", enable: false},//{name: 'Магазины', i: 'grocery-15', id: "502000000"},
            {id:"503000000", enable: true},//{name: 'Питание', i: 'restaurant-15', id: "503000000"}
            //{name: 'Размещение', i: 'lodging-15', id: "600000000"}
        ],
    //tags : "101000000,102010000,103000000,104000000,107000000,300000000",
    homeFeatures : [
        {
            id: 3217157,
            lat: 55.74763107299805,
            lon: 37.627540588378906,
            type: "Feature",
            zoom: 15.6,
            geometry: {type: "Point", coordinates: [37.627540588378906, 55.74763107299805]},
            properties: {
                sizerank: 1,
                icon: "lodging-15",
                id: 3217157,
                ispt: true,
                "lat_max": 55.74763,
                "lon_max": 37.627542,
                "lon_min": 37.627542,
                "lat_min": 55.74763,
                mainkind: "699000000",
                name: "Barin Residence Balchug",
                popular: 2,
                rating: 9.2,
                xid: "N6487683842",
                "url:ru" : "https://www.barinresidence.com/ru/balchug",
                "url:en" : "https://www.barinresidence.com/en/balchug",
                burl: "https://www.booking.com/hotel/ru/barin-residence-balchug-moskva.html",
                preview: {
                    source: 'https://data.opentripmap.com/images/user/barin/balchug400.jpg'
                },
                "address:en": "Ulitsa Sadovnicheskaya 8, Moscow",
                "address:ru": "Москва, ул. Садовническая д.8",
                phone: "+7&nbsp;499&nbsp;490&nbsp;27&nbsp;10",
                "descr:ru" : "Отель Barin Residence Balchug расположен в Москве, в 5 минутах ходьбы от парка «Зарядье» и в 600 метрах от собора Василия Блаженного. По утрам подают континентальный завтрак. Персонал круглосуточной стойки регистрации говорит на русском и английском языках. Отель Barin Residence Balchug находится в 900 метрах от универмага ГУМ. Замоскворечье — отличный выбор, если вам интересны история, музеи и культура.",
                "descr:en" : "Barin Residence Balchug is located in Moscow, within a 5-minute walk of Zaryadye Park and 641 m of Saint Basil's Cathedral. Popular points of interest around the property include The Kremlin and Lenin Mausoleum. Staff on site can arrange airport transportation." +
                "Guests at the hotel can enjoy a continental breakfast." +
                "Speaking English and Russian, staff are ready to help around the clock at the reception." +
                "GUM Shopping Center is a 10-minute walk from Barin Residence Balchug." +
                "Zamoskvorechye is a great choice for travelers interested in history, museums and culture."
            }
        },
        {
            type: "Feature",
            lat: 55.7699228,
            lon: 37.6025118,
            zoom: 15.6,
            geometry: {"type": "Point", "coordinates": [37.6025118, 55.7699228]},
            id: 678344,
            properties: {
                id: 678344,
                icon: "lodging-15",
                "burl": "https://www.booking.com/hotel/ru/d3-4ndud-n-d-dddd.html",
                url: "https://www.barinresidence.com/ru/skver-hotel",
                "lat_min": 55.7699228,
                "lat_max": 55.7699228,
                "lon_max": 37.6025118,
                "lon_min": 37.6025118,
                mainkind: "699000000",
                "rating": 7.9,
                "stars": 3,
                "xid": "N5293826521",
                "name": "Skver Hotel Tverskaya",
                osm_id: 5293826521,
                "popular": 2,
                "ispt": true,
                preview: {
                    source: 'https://data.opentripmap.com/images/user/barin/skver400.jpg'
                },
                address: "Москва, Старопименовский пер., д.11/6, стр. 1",
                phone: "+7&nbsp;499&nbsp;490&nbsp;27&nbsp;10",
                descr: "Этот отель находится в центре Москвы, всего в 2 минутах ходьбы от Тверской улицы с магазинами и ресторанами. К услугам гостей отеля \"Сквер\" бесплатный Wi-Fi и круглосуточная стойка регистрации. " +
                "Светлые номера с декором в классическом стиле оформлены в теплых тонах и оснащены кондиционером. В числе удобств всех номеров телевизор с плоским экраном и собственная ванная комната с халатами и феном. В номерах приготовлены тапочки и бесплатные туалетно-косметические принадлежности. " +
                "В ресторане подают блюда средиземноморской кухни. В пределах 5 минут ходьбы от отеля расположены различные кафе и рестораны."
            }
        },
        {
            id: 7641397,
            lat: 55.7701839,
            lon: 37.6026151,
            zoom: 15.6,
            type: "Feature",
            geometry: {type: "Point", coordinates: [37.6026151, 55.7701839]},
            properties: {
                id: 7641397,
                sizerank: 1,
                icon: "lodging-15",
                burl: "https://www.booking.com/hotel/ru/tverskaya-suites.html",
                "url:ru": "https://www.barinresidence.com/ru/tverskaya",
                "url:en": "https://www.barinresidence.com/en/tverskaya",
                lat_min: 55.7701839,
                lat_max: 55.7701839,
                lon_max: 37.6026151,
                lon_min: 37.6026151,
                mainkind: "699000000",
                rating: 8.3,
                stars: 3,
                xid: "N6487719908",
                name: "Barin Residence Tverskaya",
                popular: 2,
                ispt: true,
                preview: {
                    source: 'https://data.opentripmap.com/images/user/barin/tverskaya400.jpg'
                },
                "address:ru" : "Москва, Старопименовский пер., д.11/6, стр. 2",
                "address:en" : "Staropimenovsky Pereulok 11/6 bldg 2, Moscow",
                phone: "+7&nbsp;499&nbsp;490&nbsp;27&nbsp;10",
                "descr:ru": "Этот отель расположен в историческом особняке постройки XVII века, в Москве, в 10 минутах ходьбы от Московского художественного театра и Большого театра. К услугам гостей бесплатный Wi-Fi и бесплатная парковка. Во всех номерах установлен телевизор с кабельными каналами и обеденный стол. В распоряжении гостей собственная ванная комната с ванной или душем, феном и банными халатами. Из номеров открывается вид на город. В числе прочих удобств рабочий стол, гладильные принадлежности и постельное белье.",
                "descr:en": "Offering free WiFi, Barin Residence is located in Moscow and occupies a historic 19th-century mansion. With free parking available to all guests, the Moscow Art Theater and Bolshoi Theater are both within a 10-minute stroll from the property." +
                            "Each room includes a TV and cable channels. There is also a dining table. Featuring a bath or a shower, the private bathroom also comes with a hairdryer and bathrobes. Guests can enjoy city view from the room. Extras include a desk, bed linen and ironing facilities. Fans are available upon request." +
                            "At Barin Residence Tverskaya there is a 24-hour front desk." +
                            "The hotel is a 7-minute walk from State Historical Museum and a 10-minute walk from Red Square. The Kremlin is 1.4km away."
            }
        },
        {
            id: 13623030,
            type: "Feature",
            lat: 55.76148986816406,
            lon: 37.631832122802734,
            zoom: 16,
            geometry: {type: "Point", "coordinates": [37.631832122802734, 55.76148986816406]},
            properties: {
                id: 13623030,
                sizerank: 1,
                icon: "lodging-15",
                lat_min: 55.761488,
                mainkind: "699000000",
                stars: 3,
                "url:ru": "https://www.barinresidence.com/ru/myasnitskaya",
                "url:en": "https://www.barinresidence.com/en/myasnitskaya",
                xid: "N6892083613",
                lat_max: 55.761488,
                lon_max: 37.631833,
                name: "Barin Residence Myasnitskaya",
                osm_id: 6892083613,
                lon_min: 37.631833,
                popular: 1,
                ispt: true,
                preview: {
                    source: 'https://data.opentripmap.com/images/user/barin/myasnitskaya400.jpg'
                },
                phone: "+7&nbsp;499&nbsp;490&nbsp;27&nbsp;10",
                "address:ru" : "Москва, Милютинский переулок, д.2/9",
                "address:en" : "Milyutinskiy pereulok, 2/9, Moscow",
                "descr:ru": "Отель расположен в Москве, в 1 км от Красной площади и в 500 м от Центрального Детского Магазина." +
                            "К услугам гостей бесплатная частная парковка и терраса. Также в отеле работает Sky Lounge с потрясающим видом на город." +
                            "Номера с кондиционером, бесплатным Wi-Fi и собственной ванной комнатой. Все номера отеля оснащены телевизором с плоским экраном. Есть семейные номера." +
                            "По утрам подают континентальный завтрак." +
                            "Круглосуточная стойка регистрации.",
                "descr:en": "Located within 1 km of Red Square and 500 m of Central Children’s Store. Among the facilities of this property are a Sky Lounge with amazing city view, a 24-hour front desk and room service, along with free WiFi throughout the property. Private parking is also available." +
                            "It offers air-conditioned rooms with free Wi-Fi and a private bathroom. All units in the hotel are equipped with a flat-screen TV. Family rooms are available." +
                            "A continental Breakfast is served daily." +
                            "24-hour front desk."
            }
        },
        {
            id: 13623029,
            lat: 55.7614860534668,
            lon: 37.63203811645508,
            zoom: 16,
            type: "Feature",
            geometry: {"type": "Point", "coordinates": [37.63203811645508, 55.7614860534668]},
            properties: {
                lat_min: 55.761487,
                mainkind: "699000000",
                sizerank: 1,
                icon: "lodging-15",
                "stars": 4,
                "url:ru": "https://www.barinresidence.com/ru/grand",
                "url:en": "https://www.barinresidence.com/en/grand",
                "xid": "N6892093313",
                "lat_max": 55.761487,
                "lon_max": 37.632039,
                "name": "Barin Residence Grand",
                "osm_id": 6892093313,
                "lon_min": 37.632039,
                "popular": 1,
                "ispt": true,
                preview: {
                    source: 'https://data.opentripmap.com/images/user/barin/grand400.jpg'
                },

                "address:ru" : "Москва, Милютинский переулок, д.2",
                "address:en" : "Milyutinskiy pereulok, 2, Moscow",
                "descr:ru": "Отель расположен в Москве, в 1 км от Красной площади и 500 м от Центрального Детского Магазина." +
                            "К услугам гостей терраса и бесплатная частная парковка." +
                            "Также в отеле работает Sky Lounge с потрясающим видом на город." +
                            "К услугам гостей номера с кондиционером, бесплатным Wi-Fi и собственной ванной комнатой. Также в отеле есть семейные номера." +
                            "Каждое утро в отеле подают континентальный завтрак." +
                            "Круглосуточная стойка регистрации.",
                "descr:en": "Barin Residence Grand hotel is located in Moscow, 1 km from Red Square and 500 m from Central Children’s Store." +
                            "It offers a terrace and free private Parking." +
                            "There is also a Sky Lounge with amazing city view." +
                            "It offers air-conditioned rooms with free Wi-Fi and a private bathroom. Family rooms are available." +
                            "A continental Breakfast is served daily." +
                            "24-hour front desk.",
                phone: "+7&nbsp;499&nbsp;490&nbsp;27&nbsp;10",
            }
        }
    ]
};

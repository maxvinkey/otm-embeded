import {
    config,
    sendUserEvent
} from "./config";

export default class OTMLogoControl {
    constructor(path) {
        this.path = path ? path : "";
    }

    formatHash() {
        var center = this._map.getCenter();
        var zoom = Math.round(this._map.getZoom() * 100) / 100;
        var precision = Math.ceil((zoom * Math.LN2 + Math.log(512 / 360 / 0.5)) / Math.LN10);
        var m = Math.pow(10, precision);
        var lng = Math.round(center.lng * m) / m;
        var lat = Math.round(center.lat * m) / m;
        return "#" + [zoom, lat, lng].join("/");
    }

    onAdd(map) {
        this._map = map;
        this._container = document.createElement('div');
        this._container.classList.add('mapboxgl-ctrl');
        const anchor = document.createElement('a');
        anchor.href = "https://opentripmap.com/" + config.getOTMLang() + "/" + this.formatHash();
        anchor.target = "_blank";
        anchor.title = LOC.txt.openat;
        var self = this;
        anchor.onclick = function () {
            this.href = "https://opentripmap.com/" + config.getOTMLang() + "/" + self.formatHash();
            sendUserEvent('href', 'otm',  'LOGO');
            return true;
        };
        const img = document.createElement('img');
        img.classList.add('map-brand-otm');
        img.setAttribute("src", this.path+"images/logo_small.svg");
        anchor.appendChild(img);
        this._container.appendChild(anchor);
        return this._container;
    }

    onRemove() {
        this._container.parentNode.removeChild(this._container);
    }
}


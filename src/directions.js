import { decode } from '@mapbox/polyline';
import directionsStyle from './directions_style';

export default class Directions {

    constructor(map) {
        const geojson = {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: []
            }
        };

        map.addSource('directions', geojson);
        directionsStyle.forEach((style) => {
            // only add the default style layer if a custom layer wasn't provided
            if (!map.getLayer(style.id))
                map.addLayer(style);
        });
    }
    static formatTime(t) { // секунды
        let un = {
            hours: LOC.txt.unitNames.hours,
            minutes: LOC.txt.unitNames.minutes,
            seconds: LOC.txt.unitNames.seconds
        };
        if (t > 86400) {
            return Math.round(t / 3600) + ' '+un.hours;
        } else if (t > 3600) {
            return Math.floor(t / 3600) + ' '+un.hours+' ' +
                Math.round((t % 3600) / 60) + ' '+un.minutes;
        } else if (t > 300) {
            return Math.round(t / 60) + ' '+un.minutes;
        } else if (t > 60) {
            let minutes = Math.floor(t / 60);
            let seconds = (t % 60);
            if (seconds > 10){
                minutes++;
            }
            return minutes + ' '+un.minutes;
        } else {
            return '< 1 '+un.minutes;
        }
    }
    static formatDistance(d, sensitivity){ // метры
        let dist = {
            value: d >= 1 ? d: d*1000,
            unit: d >= 1 ? LOC.txt.unitNames.kilometers : LOC.txt.unitNames.meters
        };
        let pow10 = Math.pow(10, -sensitivity);
        dist.value = Math.round(dist.value * pow10) / pow10;
        return `${dist.value} ${dist.unit}`;
    }
    getDirections(locations, costing) {

            function fetchDirections(json){
                return new Promise(function(resolve, reject) {
                    let xmlHttp = new XMLHttpRequest();
                    xmlHttp.onreadystatechange = function () {
                        if (xmlHttp.readyState !== 4) {
                            return;
                        }
                        if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                            reject(xmlHttp.response ? JSON.parse(xmlHttp.response) : {error: 'Unknown', error_code: xmlHttp.status});
                            return;
                        }
                        resolve(JSON.parse(xmlHttp.response));
                    };
                    //encodeURIComponent(
                    let api = 'https://optimization.opentripmap.com';
                    let url = `${api}/route?json=${JSON.stringify(json)}`;
                    xmlHttp.abort();
                    xmlHttp.open('GET', url, true);
                    xmlHttp.setRequestHeader('Accept', 'application/json');
                    xmlHttp.send(null);
                });
            }
            let params = {"locations":locations,"costing":costing};

            return fetchDirections(params).then(function(responce){
                let routeIndex = 0;
                let directions = responce.trip.legs;
                let origin = responce.trip.locations[0];
                let destination = responce.trip.locations[responce.trip.locations.length - 1];


                const geojson = {
                    type: 'FeatureCollection',
                    features: [
                        /*
                        {
                            type: "Feature",
                            geometry: {type: "Point", coordinates: [origin.lon, origin.lat]},
                            properties: {id: "origin", "marker-symbol": "A"}
                        },
                        {
                            type: "Feature",
                            geometry: {type: "Point", coordinates: [destination.lon, destination.lat]},
                            properties: {id: "destination", "marker-symbol": "B"}
                        }
                        */
                        //hoverMarker
                    ]
                };
/*
                responce.trip.locations.forEach((d) => {
                        geojson.features.push({
                            type: 'Feature',
                            geometry: {type: "Point", coordinates: [d.lon, d.lat]},
                            properties: {
                                id: 'waypoint',
                                name: d.name
                            }
                        });
                });*/

                if (directions.length) {
                    directions.forEach((feature, index) => {

                        const features = [];
                        const decoded = decode(feature.shape, 6).map(function(c) {
                            return c.reverse();
                        });
                        decoded.forEach(function(c, i) {
                            var previous = features[features.length - 1];
                            if (previous) {
                                previous.geometry.coordinates.push(c);
                            } else {
                                var segment = {
                                    type: "Feature",
                                    geometry: {
                                        type: 'LineString',
                                        coordinates: []
                                    },
                                    properties: {
                                        'route-index': index,
                                        route: (index === routeIndex) ? 'selected' : 'alternate',
                                    }
                                };
                                segment.geometry.coordinates.push(c);
                                features.push(segment);
                            }
                        });

                        geojson.features = geojson.features.concat(features);
/*
                        if (index === routeIndex) {
                            // Collect any possible waypoints from steps
                            feature.legs[0].steps.forEach((d) => {
                                if (d.maneuver.type === 'waypoint') {
                                    geojson.features.push({
                                        type: 'Feature',
                                        geometry: d.maneuver.location,
                                        properties: {
                                            id: 'waypoint'
                                        }
                                    });
                                }
                            });
                        }
*/
                    });
                }
                return {
                    dist: Directions.formatDistance(responce.trip.summary.length, -1),
                    time: Directions.formatTime(responce.trip.summary.time),
                    geojson: geojson
                };
                }).catch(function (reason) {
                console.log(reason);
                return {dist:'-',time:'-',geojson :{
                        type: 'geojson',
                        data: { type: 'FeatureCollection', features: []}
                    }
                };
            });
        }
}
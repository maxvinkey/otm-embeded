export let langd = {
    places_to_go: 'Куда сходить',
    openat : 'Нажмите, чтобы отобразить эту область в атласе OpenTripMap',
    view_details : 'Посмотреть подробнее в OpenTripMap',
    view_on: 'Смотреть на',
    view_on_map: 'Показать на карте',
    back_to_map : 'Вернуться к карте',
    map: 'Карта',
    sat: 'Спутник',
    hybrid: 'Гибрид',
    routing_error: 'Невозможно построить маршрут',
    view_more : 'Показать еще',
    description: 'описание',
    empty_places_list:  'В видимой области карты объектов не найдено. Попробуйте подвигать карту.',
    empty_directions_list:  'В видимой области карты маршрутов не найдено. Попробуйте подвигать карту.',
    places : 'Места',
    directions : 'Маршруты',
    review : {
        one: 'отзыв',
        two: 'отзыва',
        many: 'отзывов'
    },
    search:{
        placeholder : 'Место, адрес...'
    },
    'unitNames': {
        'meters': 'м',
        'kilometers': 'км',
        'yards': 'ярд.',
        'miles': 'мил.',
        'hours': 'ч',
        'minutes': 'мин',
        'seconds': 'с'
    },
    'tours' : {
        tour : "Прогулка",
        quest : "Квест",
        route_points : 'Точки маршрута',
        route_point :  'Точка маршрута'
    },
    'card' : {
        zoom_in: 'Приблизиться к объекту',
        add_to_route: 'Добавить к маршруту',
        isochrone_pedestrian: 'Зона пешей доступности',
        view_link: 'Смотреть',
        title: 'Название',
        score: 'Рейтинг',
        score5: 'Превосходно',
        score4: 'Очень хорошо',
        score3: 'Хорошо',
        score2: 'Удовлетворительно',
        rating: 'Класс',
        access: {"name": 'Доступ', "type": "strict_enum", "delim": /[,]/},
        aerialway: {"name": 'Тип подъемника', "type": "strict_enum"},
        amenity: {"name": 'Дополнительные услуги', "type": "strict_enum"},
        architect: 'Архитектор',
        artist_name: 'Автор',
        artwork_type: {"name": 'Тип произведения', "type": "strict_enum", "delim": /[,;]/},
        atm: {"name": 'Банкомат', "type": "bool"},
        automated: {"name": 'Автоматизированная', "type": "bool"},
        brand: 'Бренд',
        capacity: 'Количество мест',
        civilization: {"name": 'Культура', "type": "strict_enum", "delim": /[;]/},
        clearance: 'Глубина',
        cuisine: {"name": 'Кухня', "type": "strict_enum", "delim": /[,;]/},
        current: {"name": 'Течение', "type": "strict_enum"},
        dangers: {"name": 'Опасности', "type": "strict_enum", "delim": /[,;]/},
        date: 'Дата',
        date_sunk: 'Дата крушения',
        denomination: {"name": 'Вероисповедание', "type": "strict_enum", "delim": /[,;]/},
        depth: 'Глубина погружения',
        description: 'Описание',
        difficulty: {"name": 'Уровень сложности', "type": "strict_enum"},
        disused: {"name": 'Статус', "type": "bool"},
        dog: {"name": 'Доступность для собак', "type": "bool"},
        education: {"name": 'Обучение', "type": "bool"},
        ele: 'Высота',
        elevation_m: 'Высота (м)',
        end_date: 'Дата закрытия/уничтожения',
        fee: {"name": 'Плата', "type": "enum"},
        geyser_height: 'Высота',
        geyser_interval: 'Интервал',
        geyser_type: {"name": 'Тип', "type": "strict_enum", "delim": /[,]/},
        height: 'Высота',
        historic_civilization: {"name": 'Историческая культура', "type": "strict_enum"},
        historic_period: {"name": 'Исторический период', "type": "strict_enum"},
        iata: 'IATA',
        int_name: 'Международное название',
        leisure: {"name": 'Тип', "type": "strict_enum"},
        loc_name: 'Местное название',
        maxdepth: 'Макс. глубина',
        memorial_type: {"name": 'Тип', "type": "strict_enum"},
        name: 'Название',
        name_en: 'Название',
        name_ru: 'Название',
        network: 'Сеть',
        nudism: {"name": 'Нудизм', "type": "strict_enum", "delim": /[;]/},
        old_name: 'Старое название',
        old_name_en: 'Старое название',
        old_name_ru: 'Старое название',
        opening_hours: 'Часы работы',
        opening_hours_open : 'Открыто сейчас',
        opening_hours_closed : 'Закрыто сейчас',
        operator: 'Оператор',
        organization: 'Организация',
        person_date_of_birth: 'Дата рождения',
        person_date_of_death: 'Дата смерти',
        phone: 'Телефон',
        piste_difficulty: {
            "name": 'Уровень сложности',
            "type": "strict_enum",
            "delim": /[;+]/
        },
        piste_type: {"name": 'Тип спуска', "type": "strict_enum", "delim": /[;+]/},
        protection_title: {
            "name": 'Статус',
            "type": "strict_enum",
            "delim": /[;]/
        },
        ref: 'Номер/индекс',
        religion: {"name": 'Религия', "type": "strict_enum", "delim": /[;]/},
        rental: {"name": 'Прокат снаряжения', "type": "bool"},
        self_service: {"name": 'Самообслуживание', "type": "bool"},
        shower: {"name": 'Душ', "type": "strict_enum"},
        site_type: {"name": 'Тип', "type": "strict_enum", "delim": /[;]|\band\b/},
        spoken_languages: {
            "name": 'Языки',
            "type": "strict_enum",
            "delim": /[,;]/
        },
        sport: {"name": 'Виды спорта', "type": "strict_enum", "delim": /[,;]/},
        stars: 'Количество звезд',
        start_date: 'Дата основания/открытия',
        status: {"name": 'Статус', "type": "strict_enum", "delim": /[;]/},
        supervised: {"name": 'Спасатели', "type": "strict_enum", "delim": /[';']/},
        surface: {"name": 'Покрытие', "type": "strict_enum", "delim": /[,\\/;]/},
        takeaway: {"name": 'Еда на вынос', "type": "strict_enum"},
        temperature: 'Температура воды',
        toilets: {"name": 'Туалет', "type": "strict_enum"},
        type: {"name": 'Тип погружения', "type": "strict_enum", "delim": /[,;]/},
        wheelchair: {"name": 'Доступность для инвалидов', "type": "strict_enum"}
    }
};


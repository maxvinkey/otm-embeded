import * as turf from 'turf';
import directionsStyle from './isochrone_style';

export default class Isochrones {

    constructor(map) {
        this.geojson = {};
        this.render(map);
    }
    render(map) {
        const src = {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: []
            }
        };
        map.addSource('isochrones', src);
        directionsStyle.forEach((style) => {
            if (!map.getLayer(style.id))
                map.addLayer(style);
        });
    }
    getBBox(isochrones) {
        return turf.bbox(isochrones.features[0]);
    }
    getIsochrones(json) {
        return new Promise(function (resolve, reject) {
            let xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState !== 4) {
                    return;
                }
                if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                    reject('');
                    return;
                }
                resolve(JSON.parse(xmlHttp.response));
            };
            let api = 'https://optimization.opentripmap.com';
            let url = `${api}/isochrone?json=${escape(JSON.stringify(json))}`;
            xmlHttp.abort();
            xmlHttp.open('GET', url, true);
            xmlHttp.setRequestHeader('Accept', 'application/json');
            xmlHttp.send(null);
        });
    }
}
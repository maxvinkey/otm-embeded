import * as turf from 'turf'
import {getFeatureLonLat} from "./lib/utils";

function getWithin(features, polygons, dist, source_name, source_layer) {
    var results = [];
    for (var i = 0; i < features.length; i++) {
        var point = features[i];
        if (point.properties.hasOwnProperty('lat') && point.properties.hasOwnProperty('lon')) {
            point.geometry = {
                coordinates : [point.properties.lon, point.properties.lat],
                type : "Point"
            };
        }

        var contained = false;
        turf.geomEach(polygons[0], function (polygon) {
            if (turf.booleanPointInPolygon(point, polygon)) contained = true;
        });
        if (contained) {
            features[i].dist = features[i].dist ? Math.min(dist[dist.length - 1], features[i].dist) : dist[dist.length - 1];
            for (var j = 1; j < polygons.length; j++) {
                contained = false;
                turf.geomEach(polygons[j], function (polygon) {
                    if (turf.booleanPointInPolygon(point, polygon)) {
                        contained = true
                    }
                });
                if (contained) {
                    features[i].dist = Math.min(dist[dist.length - 1 - j], features[i].dist);
                } else
                    break;
            }
            features[i].source_name = source_name;
            features[i].source_layer = source_layer;
            results.push(features[i]);
        }
    }
    results.sort(function(a, b) {
        var pd = b.properties.popular && a.properties.popular ? b.properties.popular - a.properties.popular : 0;
        return pd == 0 ?  a.dist - b.dist : pd;
    });
    return results;
};

function inBounds(point, bounds) {
    var lng = (point.lng - bounds._ne.lng) * (point.lng - bounds._sw.lng) < 0;
    var lat = (point.lat - bounds._ne.lat) * (point.lat - bounds._sw.lat) < 0;
    return lng && lat;
}

function getWithinExtent(bounds, features, source_name, source_layer){

    var results = [];
    for (var i = 0; i < features.length; i++) {
        var point = features[i];
        if (!inBounds(getFeatureLonLat(point), bounds))
            continue;
        point.source_name = source_name;
        point.source_layer = source_layer;
        results.push(point);
    };

    function sortCluster(a, b) {
        return b.properties.count - a.properties.count;
    }
    function sortPOI(a, b) {
        return b.properties.popular - a.properties.popular;
    }

    results.sort(source_layer == 'clusters' ? sortCluster : sortPOI);
    return results;
};
/*
function getWithinExtent(){
    let features = map.querySourceFeatures('otmpoi', {
        sourceLayer: 'pois'
    });
    let source_name = 'otmpoi';
    let source_layer = 'vpois';
    if (features.length === 0){
        features = map.querySourceFeatures('clusters', {
            sourceLayer: 'clusters'
        });
        source_name = 'clusters';
        source_layer = 'clusters';
    }
    let bounds = map.getBounds();
    return getWithinExtent(bounds,features, source_name, source_layer);
}

function getWithin(polygons, dist){
    let features = map.querySourceFeatures('otmpoi', {
        sourceLayer: 'pois'
    });
    return getWithin(features, polygons, dist, 'otmpoi', 'vpois');
}
*/
export {getWithin, getWithinExtent}
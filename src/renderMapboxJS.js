import {
    config
} from "./config";
import L from "mapbox.js";

function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

export default function renderMapboxJS() {
    loadjscssfile("https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css", "css");
    $(".basemapsNavigation").hide();
    L.mapbox.accessToken = 'pk.eyJ1Ijoib3BlbnRyaXBtYXAiLCJhIjoiY2ppdmIwMjYyMGU2cTNxbGhibGRwMnE3ZCJ9.AX6Uu6Iqqj1qxLQzz5q8mw';
    L.mapbox.config.HTTPS_URL = 'https://api.mapbox.com';
    L.mapbox.config.FORCE_HTTPS = true;

    let map = L.mapbox.map('map', '', {
        scrollWheelZoom: true,
        minZoom: 1
    });

    L.mapbox.styleLayer('mapbox://styles/opentripmap/cjte7rbpp024i1fofskx9yqpj').addTo(map);
    map.setView([config.getHomeFeature().lat, config.getHomeFeature().lon], Math.round(15));
}
exports.pois_layers = {
    text_paint:{
        normal:{
            "text-color": "#666",
            "text-halo-width": 1,
            "text-halo-color": "rgba(255, 255, 255, 0)",
            "text-halo-blur": 0.5
        },
        transparent:{
            "text-color": "rgba(255, 255, 255, 1)",
            "text-halo-width": 3,
            "text-halo-color": "rgba(0, 0, 0, 0.5)",
            "text-halo-blur": 2
        }
    },
    pois_label:
        {
            "id": "all_pois_label",
            "type": "symbol",
            "source": "otmpoi",
            "source-layer": "pois",
            "layout": {
                "icon-size": 1,
                "icon-allow-overlap": true,
                "icon-image": "",
                "text-anchor": "top",
                "text-rotate": 0,
                "text-letter-spacing": 0,
                "text-line-height": 1.2,
                "visibility": "visible",
                "text-field": "{name}",
                "text-max-width": 9,
                "text-font": [
                    "Noto Sans Regular"
                ],
                "text-size": 14,
                "text-offset": [
                    0,
                    1.2
                ]
            },
            "paint": {}
        },
    pois_icons: {
        "id": "all_pois_icons",
        "type": "symbol",
        "source": "otmpoi",
        "source-layer": "pois",
        "minzoom": 14,
        "layout": {
            "icon-size": 1,
            "icon-allow-overlap": true,
            "icon-image": "otm_{icon}",
            "text-anchor": "top",
            "text-rotate": 0,
            "text-letter-spacing": 0,
            "text-line-height": 1.2,
            "visibility": "visible",
            "text-field": "",
            "text-max-width": 9,
            "text-font": [
                "Noto Sans Regular"
            ],
            "text-size": 14,
            "text-offset": [
                0,
                1.2
            ]
        },
        "paint": {
            "text-color": "#666",
            "text-halo-width": 1,
            "text-halo-color": "rgba(255, 255, 255, 0)",
            "text-halo-blur": 0.5
        }
    },
    shop_heatmap:     {
        "id": "hm_shopping",
        "type": "heatmap",
        "source": "heat",
        "source-layer": "heat",
        "minzoom": 10,
        "maxzoom": 20,
        "filter": [
            "all",
            [
                "==",
                "k",
                502
            ]

        ],
        "paint": {
            "heatmap-radius": {
                "stops": [
                    [
                        12,
                        20
                    ],
                    [
                        16,
                        100
                    ],
                    [
                        20,
                        300
                    ]
                ]
            },
            "heatmap-weight": 1,
            "heatmap-color": [
                "interpolate",
                [
                    "linear"
                ],
                [
                    "heatmap-density"
                ],
                0,
                "rgba(145,233,234,0)",
                0.3,
                "rgba(145,233,234,0.1)",
                0.6,
                "rgba(134,231,179,0.5)",
                1,
                "rgba(81,154,94,0.7)"
            ],
            "heatmap-intensity": {
                "stops": [
                    [
                        10,
                        0.1
                    ],
                    [
                        17,
                        0.7
                    ]
                ]
            },
            "heatmap-opacity": {
                "stops": [
                    [
                        11,
                        0.1
                    ],
                    [
                        14,
                        0.6
                    ]
                ]
            }
        }
    },
    food_heatmap:     {
        "id": "hm_food",
        "type": "heatmap",
        "source": "heat",
        "source-layer": "heat",
        "minzoom": 10,
        "maxzoom": 20,
        "filter": [
            "all",
            [
                "==",
                "k",
                503
            ]
        ],
        "paint": {
            "heatmap-radius": {
                "stops": [
                    [
                        13,
                        15
                    ],
                    [
                        15,
                        120
                    ]
                ]
            },
            "heatmap-weight": 2,
            "heatmap-color": [
                "interpolate",
                [
                    "linear"
                ],
                [
                    "heatmap-density"
                ],
                0,
                "rgba(248,231,28, 0)",
                0.3,
                "rgba(245, 175, 25, 1)",
                1,
                "rgba(241, 79, 17, 1)"
            ],
            "heatmap-intensity": {
                "stops": [
                    [
                        10,
                        0.2
                    ],
                    [
                        15,
                        0.4
                    ]
                ]
            },
            "heatmap-opacity": {
                "stops": [
                    [
                        11,
                        0
                    ],
                    [
                        14,
                        0.6
                    ]
                ]
            }
        }
    },
    pois_heatmap:     {
        "id": "hm_pois",
        "type": "heatmap",
        "source": "heat_pois",
        "source-layer": "heat",
        "minzoom": 10,
        "maxzoom": 20,
        "filter": [
            "all"
        ],
        "paint": {
            "heatmap-radius": {
                "stops": [
                    [
                        12,
                        20
                    ],
                    [
                        17,
                        330
                    ]
                ]
            },
            "heatmap-weight": 2,
            "heatmap-color": [
                "interpolate",
                [
                    "linear"
                ],
                [
                    "heatmap-density"
                ],
                0,
                "rgba(145, 234, 228, 0)",
                0.3,
                "rgba(145, 234, 228, 0.3)",
                0.6,
                "rgba(134, 168, 231, 0.6)",
                1,
                "rgba(127, 127, 213, 1)"
            ],
            "heatmap-intensity": {
                "stops": [
                    [
                        10,
                        0.4
                    ],
                    [
                        17,
                        0.7
                    ]
                ]
            },
            "heatmap-opacity": {
                "stops": [
                    [
                        11,
                        0.1
                    ],
                    [
                        14,
                        0.8
                    ]
                ]
            }
        }
    },
    pois_point:    {
        "id": "all_pois",
        "type": "circle",
        "source": "otmpoi",
        "source-layer": "pois",
        "minzoom": 8,
        "layout": {
            "visibility": "visible"
        },
        "paint": {
            "circle-color": [
                "match", ["get", "k"],
                101, "#27964C",//Natural
                10105, "#27964C",//Beaches
                102, "#2171B5",//Cultural
                103, "#EC7014",//Historical
                104, "#88419D",//Religion
                105, "#9E6246",//Architecture
                106, "#BDA628",//Industrial
                107, "#86C740",//Others
                200, "#EF3B2C",//amusements
                300, "#EF3B2C",//sport
                400, "#EF3B2C",//adult
                503, "#4EC9AC",//foods
                502, "#EB8A9C",//shops
                501, "#FABF8F",//transport
                504, "#0AB5FF",//bank
                600, "#BF2C69",//accomodations
                "#ccc"
            ],
            "circle-stroke-color": "#ffffff",
            "circle-stroke-width": [
                "case",
                [
                    "boolean",
                    [
                        "feature-state",
                        "hover"
                    ],
                    false
                ],
                3,
                0.8
            ],
            "circle-opacity": [
                "case",
                [
                    "boolean",
                    [
                        "feature-state",
                        "hover"
                    ],
                    false
                ],
                1,
                0.6
            ],
            "circle-radius": [
                "interpolate",
                [
                    "linear"
                ],
                [
                    "zoom"
                ],
                9,
                [
                    "case",
                    [
                        "boolean",
                        [
                            "feature-state",
                            "hover"
                        ],
                        false
                    ],
                    10,
                    [
                        "match",
                        [
                            "get",
                            "popular"
                        ],
                        0,
                        3,
                        1,
                        3,
                        2,
                        3,
                        3,
                        5,
                        6
                    ]
                ],
                12,
                [
                    "case",
                    [
                        "boolean",
                        [
                            "feature-state",
                            "hover"
                        ],
                        false
                    ],
                    10,
                    [
                        "match",
                        [
                            "get",
                            "popular"
                        ],
                        0,
                        3,
                        1,
                        3,
                        2,
                        4,
                        3,
                        5,
                        6
                    ]
                ],
                15,
                [
                    "case",
                    [
                        "boolean",
                        [
                            "feature-state",
                            "hover"
                        ],
                        false
                    ],
                    20,
                    [
                        "match",
                        [
                            "get",
                            "popular"
                        ],
                        0,
                        4,
                        1,
                        6,
                        2,
                        8,
                        3,
                        10,
                        12
                    ]
                ]
            ]
        }
    },
    clusters: {
        "id": "clusters",
        "type": "circle",
        "source": "clusters",
        "source-layer": "clusters",
        "minzoom": 2,
        "maxzoom": 8,
        "layout": {},
        "paint": {
            "circle-color": "rgb(55,144,144)",
            "circle-radius": [
                "case",
                ['<', ['number', ['get', "count"], 0], 100], 15,
                ['<', ['number', ['get', "count"], 0], 1000], 20,
                25
            ],
            "circle-stroke-color": "rgba(102,193,201, 0.6)",
            "circle-stroke-width": 5,
            "circle-opacity": 0.8
        }
    },
    clusters_label: {
        "id": "clusters_label",
        "type": "symbol",
        "source": "clusters",
        "source-layer": "clusters",
        "minzoom": 2,
        "maxzoom": 8,
        "layout": {
            "text-allow-overlap": true,
            "text-field": "{title}",
            "text-size": 12,
            "text-font": [
                "Open Sans SemiBold",
                "Arial Unicode MS Regular"
            ]
        },
        "paint": {
            "text-color": "rgb(100%, 100%, 100%)"
        }
    },
    "home_icons" : {
        "id": "home_icons",
        "type": "symbol",
        "source": "hotel",
        "minzoom": 0,
        "maxzoom": 24,
        "layout": {
            "icon-size": 1,
            "icon-allow-overlap": true,
            "icon-image": "otm_{icon}",
            "text-anchor": "top",
            "text-rotate": 0,
            "text-letter-spacing": 0.1,
            "text-line-height": 3
        }
    },
    "home-point-label":  {
        "id": "home-point-label",
        "type": "symbol",
        "source": "hotel",
        "minzoom": 6,
        "layout": {
            "text-font": [
                "Open Sans Bold",
                "Arial Unicode MS Regular"
            ],
            "text-transform": "uppercase",
            "text-letter-spacing": 0.1,
            "text-max-width": 9,
            "text-offset": [
                0,
                -2.5
            ],
            "text-size": {
                "base": 1.2,
                "stops": [
                    [
                        12,
                        10
                    ],
                    [
                        15,
                        16
                    ]
                ]
            },
            "text-field": [
                "get",
                "name"
            ]
        },
        "paint": {
            "text-halo-blur": 0.5,
            "text-color": "#271503",
            "text-halo-color": "rgba(255,255,255,0.8)",
            "text-halo-width": 1
        }
    }
};
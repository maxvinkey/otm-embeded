import {setLayout} from "./layout";

const EventEmitter = require('events');
import PerfectScrollbar from 'perfect-scrollbar';
import {
    config, Layouts
} from "./config";
import 'polyfill-array-includes';

export default class IziRouteCard extends EventEmitter {

    constructor(tour) {
        super();
        this.descr = this._parseDescription(tour.description);
        this.url = tour.url;
        this.imageSrc = tour.image;
        this.reviews = tour.reviews;
        this.routeTitle = tour.title;
        this.routeSubtitle = tour.layer_name;
        this.bounds = tour.bounds;
        this.points = this._getPoints(tour.geojson);
        let self = this;
        this.ps = new PerfectScrollbar('#card-content', {
            wheelSpeed: 2,
            wheelPropagation: true,
            scrollingThreshold: null,
            suppressScrollX: true,
            swipeEasing: false
        });

        $("#card-content").on("click", ".objectCard__backToCards", function () {
            $(".objectCard").hide();
            $("#list-content").show();
            $(".geocoderInput").removeClass('small');
        });

        $("#card-content").on("click", ".objectCard__zoomToObject", function () {
            self.emit('fitBounds', self.bounds);
        });
        $("#card-content").on("click", ".objectCard__showOnMap", function showOnMap() {
            setLayout(Layouts.MAP);
            $("#card-content").on("click", ".objectCard__backToCards", function () {
                $("#card-content").off("click", ".objectCard__showOnMap", showOnMap);
            });
            self.emit('fitBounds', self.bounds);
        });
    }
    show() {
        $(".geocoderInput").addClass('small');
        $(".objectCard")
            .html(this.renderCard())
            .show();
        this.updateScrollbar();
    }
    updateScrollbar() {
        let that = this;
        setTimeout(function () {
            that.ps.update();
        }, 1000);
    }
    _getPoints(tourGeojson) {
        let points = [];
        for (let i = 1; i < tourGeojson.features.length; i++) {
            let point = {
                name: tourGeojson.features[i].properties.name,
                title: tourGeojson.features[i].properties.title
            }
            points.push(point);
        }
        return points;
    }
    _renderRoutePoints(points) {
        let routePointsBlock = $('<div />', {
            class: 'routePointsBlock'
        });
        let routePointsBlockTitle = $('<div />', {
            class: 'routePointsBlock__title',
            text: LOC.txt.tours.route_points + ':'
        });
        routePointsBlockTitle.appendTo(routePointsBlock);
        for (let i = 0; i < points.length; i++) {
            let routePoint = $('<div />', {
                class: 'routePoint'
            });
            let routePointTitle = $('<div />', {
                class: 'routePoint__title',
                text: points[i].title
            });
            let routePointNumber = $('<span />', {
                class: 'routePoint__number',
                text: points[i].name
            });
            routePointNumber.prependTo(routePoint);
            routePointTitle.appendTo(routePoint);
            routePoint.appendTo(routePointsBlock);
        }
        return routePointsBlock[0].outerHTML;
    }
    _parseDescription(descr) {
        if (descr) {
            //делим весь текст на абзацы - исходный текст может быть разделе на абзацы с помощью br вместо p
            let parTags = new RegExp('<br />|<br>|<p>|</p>', 'i');
            if (descr.search(parTags) != -1) {
                var par = descr.split(parTags);
                for (let i = 0; i < par.length; i++) {
                    if (par[i].length >= 3) {
                        par[i] = '<p>' + par[i] + '</p>';
                    }
                }
                descr = par.join();
            } else {
                descr = '<p>' + descr + '</p>';
            }
            //убираем все абзацы со ссылками и пр.
            var el = document.createElement('html');
            el.innerHTML = descr;
            var description = '';
            var ps = el.getElementsByTagName('p');
            for (var j = 0; j < ps.length; j++) {
                var inner = ps[j].innerHTML.trim();
                let links = new RegExp('http|https|com|ru|org|href|Авторы|Автор|Редактор|Текст читают|Студия Audiogid|Маршрут прогулки|Рекомендуемый маршрут|Как пользоваться аудиогидом?|Откройте аудиогид и скачайте его|Скачайте приложение|Нажмите "Начать"|Далее приложение будет автоматически определять|Использованные в туре фотографии|Источники рассказов|File');
                if (inner.search(links) != -1) {
                    continue;
                } else {
                    description += '<p>' + inner + '</p>';
                }
            }
            //вставляем пробелы после знаков препинания
            description = description.replace(/,/g, ', ');
            description = description.replace(/\./g, '. ');
            description = description.replace(/\. \. \./g, '...');
            description = description.replace(/:/g, ': ');
            return description;
        }
    }
    _renderDescription() {
        if (this.descr) {
            return `<div class="objectDescription__title">${LOC.txt.description}</div>
                     <div class="objectDescription__text">${this.descr}</div>`;

        }
        return "";
    }
    _renderImage() {
        if (this.imageSrc) {
            if (config.getWidth() <= 768) {
                let vh = window.innerHeight * 0.01;
                return `<img class="objectCard__image" style="max-height: ${vh * 50}px;" src="${this.imageSrc}">`;
            } else {
                return `<img class="objectCard__image" src="${this.imageSrc}">`;
            }
        }
        return `<div class="objectCard__image_none"></div>`;
    }
    _getReviewStars() {
        for (let i = 9.5, rating = 10; i > 0; i--, rating--) {
            if (this.reviews.rating_average >= i) {
                return `rating${rating}`;
            }
        }
    }
    _getReviewCase(reviewsCount) {
        let gramCase;
        let str = String(reviewsCount);
        let lastSymbol = str[str.length - 1];
        let last2Symbols = str[str.length - 2] + lastSymbol;
        if ((lastSymbol == 1) && (+last2Symbols != 11)) {
            gramCase = LOC.txt.review.one;
        } else if (([2, 3, 4].includes(+lastSymbol)) && (+last2Symbols != 12) && (+last2Symbols != 13) && (+last2Symbols != 14)) {
            gramCase = LOC.txt.review.two;
        } else {
            gramCase = LOC.txt.review.many;;
        }
        return gramCase;
    }
    renderCard() {
        return `${this._renderImage(this.imageSrc)}
        <a class="objectCard__backToCards"><i class="fas fa-arrow-left"></i></a>
        <a class="objectCard__zoomToObject"><i class="fas fa-map-marker-alt"></i></a>
        <h4 class="objectCard__title">${this.routeTitle}</h4>
        <h5 class="objectCard__subtitle">${this.routeSubtitle}</h5>
        <div class="objectCard__reviewsStars ${this._getReviewStars()}"></div>
        <div class="objectCard__reviewsCount">${this.reviews.reviews_count} ${this._getReviewCase(this.reviews.reviews_count)}</div>
        <div class="objectDescription" style="margin: 0 10px"><a class="objectDescription__linkToOTM" target='_blank' href="${this.url}"><b>${LOC.txt.view_on} izi.TRAVEL</b></a></div>
        <div class="objectDescription">
            ${this._renderDescription()}
        </div>
        ${this._renderRoutePoints(this.points)}
        <div id="showOnMap" class="objectCard__showOnMap">${LOC.txt.view_on_map}</div>`
    }
}
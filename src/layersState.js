import routesStyle from './directions_style';
import isochronesStyle from './isochrone_style';
import {
    config, Layouts
} from "./config";

export default class LayersState {

    constructor(map) {
        this._map = map;
        this.route = {};
        this.isochrone = {};
        this.routeVisibility = false;
        this.isochroneVisibility = false;
        this.hoveredFeature = {};
        this.hoveredFeatureVisibility = false;
    }
    addRoute(geojson) {
        if (this._map.style && this._map.getSource('directions')) {
            this._map.getSource('directions').setData(geojson);
        }
        this.routeVisibility = true;
        this.route = geojson;
    }
    addIsochrone(isochrone) {
        this.isochroneVisibility = true;
        for (var i = 0; i < isochrone.features.length; i++) {
            var modified = isochrone.features[i];
            var seconds = modified.properties.contour * 60;
            modified.properties.time = seconds;
            modified.properties.minutes = seconds / 60;
            modified.properties.quantized = seconds % 600 === 0 ? 3600 : (seconds % 300 === 0 ? 1800 : (seconds % 300 === 0 ? 900 : 1));
        }
        var geojson = {
            features: isochrone.features,
            type: "FeatureCollection"
        };
        this.isochrone = geojson;
        if (this._map.style && this._map.getSource('isochrones')) {
            this._map.getSource('isochrones').setData(geojson);
        }
    }
    addHoveredFeature(hoveredFeature) {
        this.hoveredFeature = hoveredFeature;
        this.hoveredFeatureVisibility = true;
    }
    removeIsochrone() {
        if (this._map.style && this._map.getSource('isochrones')) {
            this._map.getSource('isochrones').setData({
                type: 'FeatureCollection',
                features: []
            });
        }
        this.isochroneVisibility = false;
    }
    removeRoute() {
        if (this._map.style && this._map.getSource('directions')) {
            this._map.getSource('directions').setData({
                type: 'FeatureCollection',
                features: []
            });
        }
        this.routeVisibility = false;
    }
    removeHoveredFeature() {
        this._map.setFeatureState(this.hoveredFeature, {
            hover: false
        });
        this.hoveredFeatureVisibility = false;
    }
    rebuild() {

       // if (($('.mapSideBar').is(':visible') && (config.getWidth() > 768)) || ((config.getWidth() <= 768))) {
            const geojson = {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: []
                }
            };
            if (!this._map.getSource('directions')) {
                this._map.addSource('directions', geojson);
                let map = this._map;
                routesStyle.forEach((style) => {
                    if (!map.getLayer(style.id))
                        map.addLayer(style);
                });
            }
            if (!this._map.getSource('isochrones')) {
                this._map.addSource('isochrones', geojson);
                let map = this._map;
                isochronesStyle.forEach((style) => {
                    if (!map.getLayer(style.id))
                        map.addLayer(style);
                });
            }
            if (this.routeVisibility) {
                if (this._map.style && this._map.getSource('directions')) {
                    this._map.getSource('directions').setData(this.route);
                }
            }
            if (this.isochroneVisibility) {
                if (this._map.style && this._map.getSource('isochrones')) {
                    this._map.getSource('isochrones').setData(this.isochrone);
                }
            }
            if (this.hoveredFeatureVisibility) {
                this._map.setFeatureState(this.hoveredFeature, {
                    hover: true
                });
            }
        /*} else {
            if (Object.keys(this.hoveredFeature).length > 0) {
                map.setFeatureState(this.hoveredFeature, {
                    hover: false
                });
            }
            if (map.style && map.getSource('directions')) {
                map.getSource('directions').setData({
                    type: 'FeatureCollection',
                    features: []
                });
            }
            if (map.style && map.getSource('isochrones')) {
                map.getSource('isochrones').setData({
                    type: 'FeatureCollection',
                    features: []
                });
            }
        }*/
    }
}
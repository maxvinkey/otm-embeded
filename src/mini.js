import './scss/mini.scss';
import mapboxgl from 'mapbox-gl';
import 'bootstrap';
import L from 'mapbox.js';
import Fingerprint2 from 'fingerprintjs2'
import OTMLogoControl from "./OTMLogoControl";
import CopyrightControl from "./copyrightControl";
import {
    config, serverHost
} from "./config";
import {JL} from 'jsnlog';
import * as LANG_EN from "./lang.en";
import {layer_names as layer_names_en, mega_names as mega_names_en, base_layer_names as base_layer_names_en, layer_catalog as layer_catalog_en} from "./catalog.en";
import * as LANG_RU from "./lang.ru";
import {layer_names as layer_names_ru, mega_names as mega_names_ru, base_layer_names as base_layer_names_ru, layer_catalog as layer_catalog_ru} from "./catalog.ru";
import {CategSet} from "./lib/categset";
import TileLayer from "./tileLayer";
import BasemapsMenu from "./basemapsMenu";
import initLanguageModule from "./openmaptiles-language";
import {global} from "./global";

function loadMap(map, container, path) {
    let categors = new CategSet();
    let tileLayer = new TileLayer(map, categors.toString());

    let elemMap = document.getElementById(container);
    elemMap.style.display = 'block';
    elemMap.style.width = '100%';
    elemMap.style.left = 0;
    let elemShowButton = document.querySelector('.mapSideBarCollapse__show');
    elemShowButton.style.display= 'block';
    elemShowButton.innerText = LOC.txt.places_to_go;

    document.querySelector('.mapboxgl-canvas').style.width = '100%';
    document.querySelector('.basemapsNavigation').style.display = 'block';

    document.querySelector('body').addEventListener('click', function(event) {
        if (event.target.classList.contains("mapSideBarCollapse__show")) {
            if (window.parent && document.referrer !== "") {
                //window.parent.dispatchEvent(new CustomEvent('opentripmap', {detail: {action: 'show_full'}}));
                let pathArray = document.referrer.split('/');
                let origin = pathArray[0] + '//' + pathArray[2];
                window.parent.postMessage('opentripmap:show_full', origin);
            }
            event.preventDefault();
        }
    });

    tileLayer.rebuild();

    document.querySelector('.basemapsNavigation__basemapsFilter').addEventListener('click', function(event) {
        let node = event.target.parentNode;
        node.classList.toggle('active');
        for (let n = 0; n < node.children.length; ++n){
            node.children[n].classList.toggle('active');
        }
        document.querySelector('.basemaps').classList.toggle('active');
    });

    let basemapsMenu = new BasemapsMenu();
    basemapsMenu.show();
    basemapsMenu.on('changed', (evt) => {
        let access_token = 'pk.eyJ1IjoibWF4a2V5IiwiYSI6ImEyNWQ3MGZlZjVmNmVhY2MxZWNjM2U2ZWY0OWNkZGJkIn0.GD8SIK1BQKJYIE4KlhptHw';
        mapboxgl.accessToken = access_token;
        map.setStyle(path + config.getBaseMaps()[evt.id].style, {
            diff: false
        });
        map.setLanguage(config.lang, true);
        config.currentBaseMap = evt.id;
    });

    map.on('style.load', function rebuildLayers() {
        tileLayer.rebuild();
    });
}

function renderMapboxGL(container, path) {

    if (global.map)
        return;

    initLanguageModule();
    let map = new mapboxgl.Map({
        container: container,
        attributionControl: false,
        hash: true,
        style: path + config.getBaseCurrentMap().style,
        center: [config.getHomeFeature().lon, config.getHomeFeature().lat],
        zoom: config.getHomeFeature().zoom
    });

    let logoControl = new OTMLogoControl(path);
    map.addControl(logoControl, 'bottom-left');

    let copyrightControl = new CopyrightControl();
    map.addControl(copyrightControl, 'bottom-right');

    global.map = map;
    map.on('load', function () {

        function addNavigatonControls() {

            let nav = new mapboxgl.NavigationControl();
            map.addControl(nav, 'bottom-right');

            nav._container.classList.add('navigationControl');
            nav._container.parentNode.classList.add('navigationAndCopyrightControls');

            let compassArrow = document.querySelector('.mapboxgl-ctrl-compass-arrow');
            compassArrow.innerHTML = '<div class="compassArrowEast"></div>'+
                                     '<div class="compassArrowWest"></div>'+
                                     '<div class="compass3DMode">3D</div>';

            let compass = document.getElementsByClassName('compass3DMode')[0];
            compass.addEventListener('click', function () {
                if (map.getPitch() != 0) {
                    map.setPitch(0);
                } else {
                    map.setPitch(60);
                }
            });
            document.querySelector('.mapboxgl-ctrl-zoom-in').innerHTML = '<i class="fa fa-plus"></i>';
            document.querySelector('.mapboxgl-ctrl-zoom-out').innerHTML = '<i class="fa fa-minus"></i>';
            loadMap(map, container, path);
        };
        addNavigatonControls();
    });
}

function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

function renderMapboxJS(container) {
    if (global.map)
        return;
    loadjscssfile("https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css", "css");
    document.querySelector('.basemapsNavigation').style.display = 'none';
    L.mapbox.accessToken = 'pk.eyJ1Ijoib3BlbnRyaXBtYXAiLCJhIjoiY2ppdmIwMjYyMGU2cTNxbGhibGRwMnE3ZCJ9.AX6Uu6Iqqj1qxLQzz5q8mw';
    L.mapbox.config.HTTPS_URL = 'https://api.mapbox.com';
    L.mapbox.config.FORCE_HTTPS = true;

    global.map = L.mapbox.map(container, '', {
        scrollWheelZoom: true,
        minZoom: 1
    });

    L.mapbox.styleLayer('mapbox://styles/opentripmap/cjte7rbpp024i1fofskx9yqpj').addTo(global.map);
    global.map.setView([config.getHomeFeature().lat, config.getHomeFeature().lon], Math.round(15));
}

function getParameterByName(name) {
    try {
        let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }catch(e){
        return null;
    }
}

function init() {

    let appender = JL.createAjaxAppender("main");
    appender.setOptions({
        "url": serverHost + "/jslog",
        "beforeSend": function (xhr, json) {
            if (config.fingerprint === 'unknown'){
                let env = {
                    ver : navigator.appVersion,
                    agent: navigator.userAgent,
                    browser: navigator.appName
                };
                if (json && json.lg){
                    for (let n = 0; n < json.lg.length; ++n){
                        json.lg[n].env = env;
                    }
                }
            }
            xhr.setRequestHeader('fingerprint', config.fingerprint);
        }
    });

    JL().setOptions({
        "appenders": [appender,JL.createConsoleAppender('console')]
    });

    let parent_url = (window.location != window.parent.location) ?
        document.referrer :
        document.location.href;

    config.info = {
        gl: mapboxgl.supported() ? 'true' : 'false',
        url: document.URL.replace("#", "$"),
        href: parent_url ? parent_url.replace("#", "$") : ""
    };

    if (window.requestIdleCallback) {
        requestIdleCallback(function () {
            Fingerprint2.get(function (components) {
                config.setFingerPrint(components);
            })
        })
    } else {
        setTimeout(function () {
            Fingerprint2.get(function (components) {
                config.setFingerPrint(components);
            })
        }, 500)
    }
}


export function createOTMMiniMap(lang, index, container, path) {

    config.lang = lang;

    window.LOC = {txt : LANG_RU.langd,
        layer_names : layer_names_ru,
        base_layer_names : base_layer_names_ru,
        mega_names : mega_names_ru,
        layer_catalog :layer_catalog_ru
    };

    config.homeFeatureIndex = index;

    init();
    let basemapsBlock = document.querySelector(container);
    try {
        if (mapboxgl.supported()) {
            JL().warn("Прошла инициализация GL");
            config.safety_mode = false;
            basemapsBlock.innerHTML =  `<div id="map-main-otm"></div>
                                <div class="mapSideBarCollapse">
                                    <div class="mapSideBarCollapse__show"></div>
                                </div>
                                <div class="basemapsNavigation">
                                    <a class="basemapsNavigation__basemapsFilter"><i class="fa fa-map"></i></a>
                                    <div class="basemaps"></div>
                                </div>`;
            window.onload = function()
            {
                renderMapboxGL("map-main-otm", path);
            };
            if (document.readyState === 'complete'){
                renderMapboxGL("map-main-otm", path);
            }
        } else {
            JL().warn("Прошла инициализация NOGL");
            config.safety_mode = true;
            basemapsBlock.innerHTML =  `<div id="map-main-otm"></div>`;
            window.onload = function() {
                renderMapboxJS("map-main-otm");
            };
            if (document.readyState === 'complete') {
                renderMapboxJS("map-main-otm");
            }
        }
    } catch(e) {
        JL().fatalException("Exception", e);
   }

};

const EventEmitter = require('events');
import {
    config,
} from "./config";

export default class CategorsMenu extends EventEmitter {

    constructor() {
        super();
    }
    show(categors) {
        let content = '';
        for (let i = 0; i <  config.getLayers().length; i++) {
            const item =  config.getLayers()[i];
            let name = LOC.layer_names.groups[item.id].n;
            let icon = LOC.layer_names.groups[item.id].i;
            let skind = ("" + item.id).substr(0, 3);
            let sclass = "categories__item";
            let sclassicon = "categories__icon";
            if (categors.contains(item.id)) {
                sclass += " active";
                sclassicon += " active";
            }
            content += `<a href="#" class="${sclass}" data-id="${item.id}">
                        <i class="maki maki-${icon} maki-${skind} ${sclassicon}"></i>
                        ${name}</a>`
        }
        $('.categories >.categories__item').remove();
        $(content).appendTo($('.categories'));
        var self = this;

        $('.categories__item')
            .click(function (evt) {
                let data_id = $(this).attr('data-id');
                $(`.categories__item[data-id='${data_id}']`).toggleClass("active");
                $(`.categories__item[data-id='${data_id}']`).children().toggleClass("active");
                self.emit('changed', {
                    id: data_id
                });
            });
    }
}
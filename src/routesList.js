import PerfectScrollbar from 'perfect-scrollbar';
import Directions from "./directions";
const EventEmitter = require('events');

export default class RoutesList extends EventEmitter {
    constructor(map) {
        super();
        this.map = map;
        let self = this;
        $("#profile").on("click", ".card", function () {
            let data_id = $(this).attr('data-id');
            self.emit('click', data_id);
        });
        this.ps = new PerfectScrollbar('#profile', {
            wheelSpeed: 2,
            wheelPropagation: true,
            scrollingThreshold: null,
            suppressScrollX: true,
            swipeEasing: false
        });
    }
    updateScrollbar() {
        this.ps.update();
    }
    isVisible() {
        return $("#profile").is(":visible");
    }
    addObjects(tours) {
        this.clear();
        let self = this;
        $("#profile-tab").text(LOC.txt.directions + " (" + tours.length + ")");

        let iconMap = {
            walk: 'fa-walking',
            bike: 'fa-bicycle',
            bus: 'fa-bus',
            car: 'fa-car',
            boat: 'fa-ship',
            train: 'fa-train',
            horseriding: 'fa-horse',
            running: 'fa-running'
        };

        //var sideBarWidth = $(".mapSideBar").outerWidth();
        let cList = $('#profile');

        function getHeight() {
            return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        }

        let maxNumberOfCards;
        if (getHeight() <= 576) {
            maxNumberOfCards = 5;
        }
        if ((getHeight() > 576) && (getHeight() <= 768)) {
            maxNumberOfCards = 7;
        }
        if (getHeight() > 768) {
            maxNumberOfCards = 10;
        }
        if (tours.length == 0) {
            let noObjectsDiv = document.createElement('div');
            noObjectsDiv.classList.add('noObjectsDiv');
            noObjectsDiv.innerText = LOC.txt.empty_directions_list;
            let parentDiv = document.querySelector('#profile');
            parentDiv.appendChild(noObjectsDiv);
        }
        for (let i = 0; i < Math.min(tours.length, maxNumberOfCards); i++) {
            let bookmark = tours[i];
            bookmark.distance = bookmark.distance != 0 ? Directions.formatDistance(bookmark.distance / 1000, -1) : ' ';
            bookmark.duration = bookmark.duration != 0 ? Directions.formatTime(bookmark.duration) : ' ';
            bookmark.icon = iconMap[bookmark.category];
            if (bookmark.playback_type == 'random') {
                bookmark.layer_name = LOC.txt.tours.tour;
            } else if (bookmark.playback_type == 'quest') {
                bookmark.layer_name = LOC.txt.tours.quest;
            } else {
                bookmark.layer_name = bookmark.placement + ' ' + bookmark.category + ' ' + bookmark.playback_type;
            }
            $(RoutesList._renderBookmarkItem(bookmark))
                .appendTo(cList);
            if ((i == (maxNumberOfCards - 1)) && (tours.length > maxNumberOfCards)) {
                let numberOfCards = i;

                function getNext(x) {
                    let restCards;
                    if ((tours.length - x - 1) > maxNumberOfCards) {
                        restCards = maxNumberOfCards;
                    } else {
                        restCards = (tours.length - x - 1);
                    }
                    cList.append(`<button class="nextTours">${LOC.txt.view_more} ${restCards}</button>`);

                    $(`.nextTours`).on("click", function () {
                        $(`.nextTours`).remove();
                        for (let k = x + 1; k < Math.min(tours.length, (x + maxNumberOfCards + 1)); k++) {
                            let bookmark = tours[k];
                            bookmark.distance = bookmark.distance != 0 ? Directions.formatDistance(bookmark.distance / 1000, -1) : ' ';
                            bookmark.duration = bookmark.duration != 0 ? Directions.formatTime(bookmark.duration) : ' ';
                            bookmark.icon = iconMap[bookmark.category];
                            if (bookmark.playback_type == 'random') {
                                bookmark.layer_name = LOC.txt.tours.tour;
                            } else if (bookmark.playback_type == 'quest') {
                                bookmark.layer_name = LOC.txt.tours.quest;
                            } else {
                                bookmark.layer_name = bookmark.placement + ' ' + bookmark.category + ' ' + bookmark.playback_type;
                            }
                            $(RoutesList._renderBookmarkItem(bookmark))
                                .appendTo(cList);
                            if ((k == (x + maxNumberOfCards)) && (tours.length > (x + maxNumberOfCards + 1))) {
                                x += maxNumberOfCards;
                                getNext(x);
                                break;
                            }
                        };
                        self.updateScrollbar();
                    });
                };
                getNext(numberOfCards);
            }
        }
        let profileTab = document.getElementById('profile-tab');
        profileTab.addEventListener('click', function () {
            setTimeout(function () {
                self.updateScrollbar();
            }, 1000);
        });
        this.updateScrollbar();
    }
    clear() {
        $('#profile >.card').remove();
        $('#profile >.noObjectsDiv').remove();
        $('.nextTours').remove();
    }
    static _renderBookmarkItem(bookmark) {
        let image = bookmark.image != '' ? `<img src="${bookmark.image}" alt="pic" class="iziRouteCard__objectImage">` :
            '<i class="fas fa-directions iziRouteCard__objectImage_none"></i>';

        return `<a class="card" data-id="${bookmark.id}"">
                    <div class="card-body iziRouteCard">
                        ${image}
                        <h5 class="card-title iziRouteCard__title">${bookmark.title}</h5>
                        <h6 class="card-subtitle mb-2 text-muted iziRouteCard__subtitle">${bookmark.layer_name}</h6>
                        <div class="iziRouteCardWays">
                            <i class="fas ${bookmark.icon} iziRouteCardWays__firstIcon"></i>
                            <div class="iziRouteCardWays__distance">${bookmark.duration}<br>${bookmark.distance}</div>
                        </div>
                    </div>
                </a>`
    }
};
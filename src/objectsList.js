import {
    getFeatureData,
    getFeatureLonLat
} from "./lib/utils";
import {
    getPreviewFeatures
} from "./lib/utils.promise";
import TileLayer from './tileLayer';
import {
    CategSet
} from "./lib/categset";

import PerfectScrollbar from 'perfect-scrollbar';
import * as turf from 'turf';
import {
    config,serverHost
} from "./config";
import {JL} from 'jsnlog';
import {getWithinExtent} from "./geometry";
import {global} from "./global";

const EventEmitter = require('events');

export default class ObjectsList extends EventEmitter {
    constructor(map) {
        super();
        this.map = map;
        let self = this;
        $("#home").on("click", ".card", function () {
            let data_id = $(this).attr('data-id');
            let mainkind = $(this).attr('data-mainkind');
            self.emit('click', {
                data_id,
                mainkind
            });
        }).on("mouseenter", ".card", function () {
            let data_id = $(this).attr('data-id');
            self.emit('mouseenter', {
                id: data_id,
                source: "otmpoi",
                sourceLayer: "pois"
            });
        }).on("mouseleave", ".card", function () {
            self.emit('mouseleave');
        });
        this.ps = new PerfectScrollbar('#home', {
            wheelSpeed: 2,
            wheelPropagation: true,
            scrollingThreshold: null,
            suppressScrollX: true,
            swipeEasing: false
        });
    }
    updateScrollbar() {
        this.ps.update();
    }
    isVisible() {
        return $("#home").is(":visible");
    }
    getFeatures() {
        let features = this.map.querySourceFeatures('otmpoi', {
            sourceLayer: 'pois'
        });
        let source_name = 'otmpoi';
        let source_layer = 'vpois';
        if (features.length === 0){
            features = this.map.querySourceFeatures('clusters', {
                sourceLayer: 'clusters'
            });
            source_name = 'clusters';
            source_layer = 'clusters';
        }
        let bounds = this.map.getBounds();
        return getWithinExtent(bounds,features, source_name, source_layer);
    }
    getClusterByID(id, kinds) {

        return new Promise(function (resolve, reject) {
            let xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState !== 4) {
                    return;
                }
                if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                    reject('');
                    return;
                }

                let c = JSON.parse(xmlHttp.response);
                c.source_name = 'clusters';
                c.source_layer = 'clusters';
                c.geometry = {
                    type: "Point",
                    coordinates: [c.lon, c.lat]
                };
                c.properties.catset = new CategSet();
                c.properties.catset.fromStats(c.stats);
                c.properties.kind = c.properties.catset.toArray();
                c.properties.xid = "REG" + c.properties.id;
                c.type = "Feature";
                resolve(c);
            };
            let url = serverHost + '/cluster/' + config.getOTMLang() + '/' + id + '/1/0/' + kinds;
            xmlHttp.open('GET', url, true);
            xmlHttp.setRequestHeader('Accept', 'application/json');
            xmlHttp.send(null);
        });
    }
    addObjects() {
        let idarray = [];
        let source_name = 'otmpoi';
        let features = this.getFeatures();
        for (let i = 0; i < features.length; i++) {
            idarray.push(features[i].properties.id);
            source_name = features[i].source_name;
        }

        if (idarray.length > 0) {
            let ids = idarray.join(",");
            let self = this;
            getPreviewFeatures(source_name, ids).then(function (previews) {
                for (let n = 0; n < config.getHomeFeatures().length; ++n){
                    previews[config.getHomeFeatures()[n].id] = config.getHomeFeatures()[n].properties.preview;
                }
                self.generateObjects(features, previews);
            }).catch(function (reason) {
                JL().error("Error", reason);
                self.generateObjects(features, {});
            });
        } else {
            this.generateObjects(features, {});
        }
    }
    generateObjects(features, previews) {
        this.clear();
        let self = this;
        $("#home-tab").text(LOC.txt.places+" (" + features.length + ")");

        //var sideBarWidth = $(".mapSideBar").outerWidth();
        let cList = $('#home');

        function getHeight() {
            return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        }

        let maxNumberOfCards;
        if (getHeight() <= 576) {
            maxNumberOfCards = 5;
        }
        if ((getHeight() > 576) && (getHeight() <= 768)) {
            maxNumberOfCards = 7;
        }
        if (getHeight() > 768) {
            maxNumberOfCards = 10;
        }

        //для возврата фильтра объектов на место после ресайза маленького экрана до большого
        if ((config.getWidth() > 768) && ($('.objectsFilter'))) {
            $('.categories.onCard').remove();
            $('.objectsFilter').remove();
            $('.routesNavigation__routesFilter.onCard').remove();
        }

        if (features.length == 0) {
            let noObjectsDiv = document.createElement('div');
            noObjectsDiv.classList.add('noObjectsDiv');
            noObjectsDiv.innerText = LOC.txt.empty_places_list;
            let parentDiv = document.querySelector('#home');
            parentDiv.appendChild(noObjectsDiv);
        } else {
            if ((config.getWidth() <= 768) && (features[0].source_name != 'clusters')) {
                if ($('.objectsFilter').length == 0) {
                    let objectsFilter = $('<div />', {
                        class: 'objectsFilter'
                    });
                    objectsFilter.addClass('onCard');
                    objectsFilter.prependTo($('#myTabContent'));

                    $('.routesNavigation__routesFilter').clone(true).addClass('onCard').appendTo($('.objectsFilter'));
                    $('.categories').clone(true).addClass('onCard').appendTo($('.objectsFilter'));
                    $('.categories.onCard').children().addClass('onCard');

                    $('.objectsFilter').css('display', 'block');
                    $('.routesNavigation__routesFilter.onCard').css('display', 'block');
                }
            }
            //не могу записать эти условия в setmapsidebar.js, т.к. список объектов подгружается не сразу
            if ((config.getWidth() <= 768) && (features[0].source_name == 'clusters')) {
                //это в принципе только для случая если изначально на карте уровень кластеров
                //(чтоб при перезагрузке содержимое списка адаптировалось под отсутствие фильтра)
                // $('#home').css('height', 'calc(100vh - 117px)');
                // $('#home').css('height', 'calc(var(--vh, 1vh) * 100 - 117px)');
                let vh = window.innerHeight * 0.01;
                $('#home').css('height', `${100*vh-117}px`);
                //это для случая если с уровня poi перешли на уровень кластеров при активной вкладке маршрутов
                $('.objectsFilter').css('display', 'none');
                $('.routesNavigation__routesFilter.onCard').css('display', 'none');
            }
            for (let i = 0; i < Math.min(features.length, maxNumberOfCards); i++) {
                let bookmark = getFeatureData(features[i],
                    this.map.getZoom(), getFeatureLonLat(features[i]));
                bookmark.list_id = i;
                bookmark.preview = previews[bookmark.id];
                $(ObjectsList._renderBookmarkItem(bookmark))
                    .appendTo(cList);
                if (bookmark.layer == "vpois") {
                    self.setDistanceAndTime(bookmark);
                }
                if ((i == (maxNumberOfCards - 1)) && (features.length > maxNumberOfCards)) {
                    let numberOfCards = i;

                    function getNext(x) {
                        let restCards;
                        if ((features.length - x - 1) > maxNumberOfCards) {
                            restCards = maxNumberOfCards;
                        } else {
                            restCards = (features.length - x - 1);
                        }
                        cList.append(`<button class="nextCards">${LOC.txt.view_more} ${restCards}</button>`);
                        $(`.nextCards`).on("click", function () {
                            $(`.nextCards`).remove();
                            for (let k = x + 1; k < Math.min(features.length, (x + maxNumberOfCards + 1)); k++) {
                                let bookmark = getFeatureData(features[k],
                                    self.map.getZoom(), getFeatureLonLat(features[k]));
                                bookmark.list_id = k;
                                bookmark.preview = previews[bookmark.id];
                                $(ObjectsList._renderBookmarkItem(bookmark))
                                    .appendTo(cList);
                                if (bookmark.layer == "vpois") {
                                    self.setDistanceAndTime(bookmark);
                                }
                                if ((k == (x + maxNumberOfCards)) && (features.length > (x + maxNumberOfCards + 1))) {
                                    x += maxNumberOfCards;
                                    getNext(x);
                                    break;
                                }
                            };
                            self.updateScrollbar();
                        });
                    };
                    getNext(numberOfCards);
                }
            }
        };
        //let sideBarHeight = $(".mapSideBar").outerHeight();
        //let h = $(document).height();
        let homeTab = document.getElementById('home-tab');
        homeTab.addEventListener('click', function () {
            setTimeout(function () {
                self.updateScrollbar();
            }, 1000);
        });
        this.updateScrollbar();
    }
    clear() {
        $('#home >.card').remove();
        $('#home >.noObjectsDiv').remove();
        $('.nextCards').remove();
    }
    static getIcon(kind) {
        let info = LOC.layer_names.items[kind];
        if (!info || !info.i)
            return undefined;
        let skind = ("" + kind).substr(0, 3);
        if (skind.startsWith("6")) {
            skind = "600";
        } else if (skind.startsWith("4")) {
            skind = "400";
        } else if (skind.startsWith("3")) {
            skind = "300";
        } else if (skind.startsWith("2")) {
            skind = "200";
        }

        return 'maki-' + info.i + ' maki-' + skind;
    }
    static setDistanceValues(summary, element) {
        //если расстояние меньше 1 км, округляем до 50 метров
        if (!/км/.test(summary.dist) && (summary.dist != '-')) {
            if (parseInt(summary.dist) < 50) {
                summary.dist = '< 50 ';
            } else {
                summary.dist = `> ${Math.floor(parseInt(summary.dist) / 50) * 50} `;
            }
            summary.dist += LOC.txt.unitNames.meters
        }
        //если расстояние больше 25 км, округляем часы
        if ((parseInt(summary.dist) > 25) && (/км/.test(summary.dist)) && (/ч/.test(summary.time))) {
            let timeInt = parseInt(summary.time); //так мы получим количество часов, т.к. всё после "ч" обрезается
            element.innerHTML = `> ${timeInt} ч<br>${summary.dist}`;
        } else {
            element.innerHTML = `${summary.time}<br>${summary.dist}`;
        }
        //для случая, когда, например, объект на острове
        if ((summary.time == '-') && (summary.dist == '-')) {
            //pedestrian.style('display', 'none');
            element.style.display = 'none';
            //$(`a[data-id="${bookmark.id}"]`).find('.placeCardWays__firstIcon').css('display', 'none');
        }
    }

    setDistanceAndTime(bookmark) {
        let coordinates = bookmark.feature.geometry.coordinates.slice();
        let pt1 = {
            "lat": config.getHomeFeature().lat,
            "lon": config.getHomeFeature().lon,
            "type": "break",
            "name": "Hotel"
        };
        let pt2 = {
            "lat": coordinates[1],
            "lon": coordinates[0],
            "type": "break",
            "name": "Attraction " + bookmark.id
        };
        let locations = [pt1, pt2];
        var linestring = {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [config.getHomeFeature().lon, config.getHomeFeature().lat],
                    [coordinates[0], coordinates[1]]
                ]
            }
        };
        let distanceKm = turf.lineDistance(linestring);

        if (distanceKm < 50) {
            global.directions.getDirections(locations, "pedestrian").then(summary => {
                let card = document.querySelector(`a[data-id="${bookmark.id}"]`);
                if (!card) {
                    //console.log("Error " + bookmark.id); // убрала пока, меня раздражает постоянно эта ошибка в консоли))
                    return;
                }

                let pedestrian = card.getElementsByClassName('placeCardWays__distance')[0];
                ObjectsList.setDistanceValues(summary, pedestrian);

                global.directions.getDirections(locations, "bicycle").then(summary => {

                    let bicycle = card.getElementsByClassName('placeCardWays__distance')[1];
                    ObjectsList.setDistanceValues(summary, bicycle);
                });
            });
        } else {
            //если расстояние по прямой больше 50 км, блок с расстояниями и временем вообще не отображаем
            //$(`a[data-id="${bookmark.id}"]`).find('.placeCardWays').css('display', 'none');
            var card = document.querySelector(`a[data-id="${bookmark.id}"]`);
            var placeCardWays = card.getElementsByClassName('placeCardWays')[0];
            placeCardWays.style.display = 'none';
        }
    }

    static _renderBookmarkItem(bookmark) {
        if (bookmark.layer == "clusters") {
            $('#home').attr('data-layer', 'clusters');
            if (bookmark.preview) {
                let image;
                if (/'/.test(bookmark.preview.source)) {
                    image = `"${bookmark.preview.source}"`;
                } else {
                    image = `'${bookmark.preview.source}'`;
                }
                return `<a class="card" data-id="${bookmark.id}">
                        <div class="card-body placeCard">
                            <img src=${image} alt="pic" class="placeCard__clusterImage">
                            <h5 class="card-title placeCard__title">${bookmark.name}</h5>
                        </div>
                    </a>`
            } else {
                return `<a class="card" data-id="${bookmark.id}">
                        <div class="card-body placeCard">
                            <h5 class="card-title placeCard__title">${bookmark.name}</h5>
                        </div>
                    </a>`
            }
        }
        if (bookmark.layer == "vpois") {
            $('#home').attr('data-layer', 'vpois');
            if (bookmark.preview) {
                let image;
                if (/'/.test(bookmark.preview.source)) {
                    image = `"${bookmark.preview.source}"`;
                } else {
                    image = `'${bookmark.preview.source}'`;
                }
                return `<a class="card" data-id="${bookmark.id}" data-mainkind="${bookmark.mainkind}">
                        <div class="card-body placeCard">
                            <img src=${image} onerror="this.src='./images/brokenPic.jpg'" alt="pic" class="placeCard__objectImage" width="65" height="75">
                            <i class="maki ${ObjectsList.getIcon(bookmark.mainkind)} placeCard__objectImageTag"></i>
                            <h5 class="card-title placeCard__title">${bookmark.name}</h5>
                            <h6 class="card-subtitle mb-2 text-muted placeCard__subtitle">${bookmark.layer_name}</h6>
                            <div class="placeCardWays">
                            <i class="fas fa-walking placeCardWays__firstIcon"></i>
                            <div class="placeCardWays__distance">-<br>-</div>
                            <i class="fas fa-bicycle placeCardWays__secondIcon"></i>
                            <div class="placeCardWays__distance">-<br>-</div>
                            </div>
                        </div>
                    </a>`
            } else {
                return `<a class="card" data-id="${bookmark.id}" data-mainkind="${bookmark.mainkind}">
                        <div class="card-body placeCard">
                            <i class="maki ${ObjectsList.getIcon(bookmark.mainkind)} placeCard__objectImage" width="65" height="75"></i>
                            <i class="maki ${ObjectsList.getIcon(bookmark.mainkind)} placeCard__objectImageTag"></i>
                            <h5 class="card-title placeCard__title">${bookmark.name}</h5>
                            <h6 class="card-subtitle mb-2 text-muted placeCard__subtitle">${bookmark.layer_name}</h6>
                            <div class="placeCardWays">
                                <i class="fas fa-walking placeCardWays__firstIcon"></i>
                                <div class="placeCardWays__distance">-<br>-</div>
                                <i class="fas fa-bicycle placeCardWays__secondIcon"></i>
                                <div class="placeCardWays__distance">-<br>-</div>
                            </div>
                        </div>
                    </a>`
            }
        }
    }
};
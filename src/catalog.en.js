let layer_catalog = [
    {
        id: 700000000,
        text: "Cameras",
        checked: false,
        children: []
    },
    {
        id: 100000000,
        text: "Interesting places",
        checked: false,
        children: [
            {
                id: 101000000,
                text: "Natural",
                checked: false,
                children: [
                    {
                        id: 101010000,
                        text: "Islands",
                        checked: false,
                        children: [
                            {
                                id: 101010100,
                                text: "tidal islands",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010200,
                                text: "inland islands",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010300,
                                text: "coral islands",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010400,
                                text: "desert islands",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101010500,
                                text: "high islands",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101019900,
                                text: "other islands",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101020000,
                        text: "Natural springs",
                        checked: false,
                        children: [
                            {
                                id: 101020100,
                                text: "hot springs",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101020200,
                                text: "geysers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101020300,
                                text: "other springs",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101030000,
                        text: "Geological formations",
                        checked: false,
                        children: [
                            {
                                id: 101030100,
                                text: "mountain peaks",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030200,
                                text: "volcanoes",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030300,
                                text: "caves",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030400,
                                text: "canyons",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101030500,
                                text: "rock formations",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101040000,
                        text: "Water",
                        checked: false,
                        children: [
                            {
                                id: 101040100,
                                text: "crater lakes",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040200,
                                text: "rift lakes",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040300,
                                text: "salt lakes",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040400,
                                text: "dry lakes",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040500,
                                text: "reservoirs",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040600,
                                text: "rivers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040700,
                                text: "canals",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040800,
                                text: "waterfalls",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101040900,
                                text: "lagoons",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101049900,
                                text: "other lakes",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101050000,
                        text: "Beaches",
                        checked: false,
                        children: [
                            {
                                id: 101050100,
                                text: "golden sand beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050200,
                                text: "white sand beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050300,
                                text: "black sand beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050400,
                                text: "shingle beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050500,
                                text: "rocks beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050600,
                                text: "urbans beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101050700,
                                text: "nude beaches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101059900,
                                text: "other beaches",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101060000,
                        text: "Nature reserves",
                        checked: false,
                        children: [
                            {
                                id: 101060100,
                                text: "aquatic protected areas",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060200,
                                text: "wildlife reserves",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060300,
                                text: "national parks",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060400,
                                text: "other nature reserves",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101060500,
                                text: "natural monuments",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 101069900,
                                text: "nature conservation areas",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 101070000,
                        text: "Glaciers",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 102000000,
                text: "Cultural",
                checked: false,
                children: [
                    {
                        id: 102010000,
                        text: "Museums",
                        checked: false,
                        children: [
                            {
                                id: 102010100,
                                text: "national museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010200,
                                text: "local museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010300,
                                text: "museums of science and technology",
                                checked: false,
                                children: [
                                    {
                                        id: 102010301,
                                        text: "maritime museums",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010302,
                                        text: "railway museums",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010303,
                                        text: "aviation museums",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010304,
                                        text: "automobile museums",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010305,
                                        text: "computer museums",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010306,
                                        text: "heritage railways",
                                        checked: false,
                                        children: []
                                    }
                                    , {
                                        id: 102010399,
                                        text: "other technology museums",
                                        checked: false,
                                        children: []
                                    }
                                ]
                            }
                            , {
                                id: 102010400,
                                text: "science museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010500,
                                text: "planetariums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010600,
                                text: "military museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010700,
                                text: "history museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010800,
                                text: "archaeological museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102010900,
                                text: "biographical museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011000,
                                text: "open-air museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011100,
                                text: "fashion museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011200,
                                text: "children museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011300,
                                text: "historic house museums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011400,
                                text: "art galleries",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011500,
                                text: "zoos",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102011600,
                                text: "aquariums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102019900,
                                text: "other museums",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 102020000,
                        text: "Theatres and concert halls",
                        checked: false,
                        children: [
                            {
                                id: 102020100,
                                text: "sylvan theatres",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020200,
                                text: "opera houses",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020300,
                                text: "music venues",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020400,
                                text: "concert halls",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020500,
                                text: "puppetries",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102020600,
                                text: "сhildren\u0027s theatres",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102029900,
                                text: "other theatres",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 102030000,
                        text: "Urban environment",
                        checked: false,
                        children: [
                            {
                                id: 102030100,
                                text: "wall painting",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030200,
                                text: "squares and streets",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030300,
                                text: "installation",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030400,
                                text: "gardens and parks",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030500,
                                text: "fountains",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 102030600,
                                text: "sculptures",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                ]
            }
            , {
                id: 103000000,
                text: "Historical",
                checked: false,
                children: [
                    {
                        id: 103010000,
                        text: "Historical places",
                        checked: false,
                        children: [
                            {
                                id: 103010100,
                                text: "historic districts",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103010200,
                                text: "historic settlements",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103010300,
                                text: "fishing villages",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103010400,
                                text: "battlefields",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103020000,
                        text: "Fortifications",
                        checked: false,
                        children: [
                            {
                                id: 103020100,
                                text: "castles",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020200,
                                text: "hillforts",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020300,
                                text: "fortified towers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020400,
                                text: "defensive walls",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020500,
                                text: "bunkers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103020600,
                                text: "kremlins",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103029900,
                                text: "other fortifications",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103030000,
                        text: "Monuments and memorials",
                        checked: false,
                        children: [
                            {
                                id: 103030100,
                                text: "milestones",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103030200,
                                text: "monuments",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103040000,
                        text: "Archaeology",
                        checked: false,
                        children: [
                            {
                                id: 103040100,
                                text: "megaliths",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040200,
                                text: "menhirs",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040300,
                                text: "roman villas",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040400,
                                text: "cave paintings",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040500,
                                text: "settlements",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103040600,
                                text: "rune stones",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103049900,
                                text: "archaeological sites",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 103050000,
                        text: "Burial places",
                        checked: false,
                        children: [
                            {
                                id: 103050100,
                                text: "cemeteries",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050200,
                                text: "war graves",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050300,
                                text: "necropolises",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050400,
                                text: "dolmens",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050500,
                                text: "tumuluses",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050600,
                                text: "mausoleums",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050700,
                                text: "war memorials",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103050800,
                                text: "crypts",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 103059900,
                                text: "other burial places",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                ]
            }
            , {
                id: 104000000,
                text: "Religion",
                checked: false,
                children: [
                    {
                        id: 104010000,
                        text: "churches",
                        checked: false,
                        children: [
                            {
                                id: 104010100,
                                text: "eastern orthodox churches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 104010200,
                                text: "catholic churches",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 104019900,
                                text: "other churches",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 104020000,
                        text: "cathedrals",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104030000,
                        text: "mosques",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104040000,
                        text: "synagogues",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104050000,
                        text: "buddhist temples",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104060000,
                        text: "hindu temples",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104070000,
                        text: "egyptian temples",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104990000,
                        text: "other temples",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 104090000,
                        text: "monasteries",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 105000000,
                text: "Architecture",
                checked: false,
                children: [
                    {
                        id: 105010000,
                        text: "Historic architecture",
                        checked: false,
                        children: [
                            {
                                id: 105010100,
                                text: "pyramids",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010200,
                                text: "amphitheatres",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010300,
                                text: "triumphal archs",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010400,
                                text: "palaces",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010500,
                                text: "manor houses",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010600,
                                text: "wineries",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010700,
                                text: "farms",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105019900,
                                text: "buildings and structures",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105010900,
                                text: "destroyed objects",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 105020000,
                        text: "Skyscrapers",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 105030000,
                        text: "Bridges",
                        checked: false,
                        children: [
                            {
                                id: 105030100,
                                text: "moveable bridges",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030200,
                                text: "stone bridges",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030300,
                                text: "viaducts",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030400,
                                text: "Roman bridges",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030500,
                                text: "footbridges",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030600,
                                text: "aqueducts",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105030700,
                                text: "suspension bridges",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105039900,
                                text: "other bridges",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 105040000,
                        text: "Towers",
                        checked: false,
                        children: [
                            {
                                id: 105040100,
                                text: "observation towers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040200,
                                text: "watchtowers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040300,
                                text: "water towers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040400,
                                text: "transmitter towers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040500,
                                text: "clock towers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040600,
                                text: "bell towers",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105040700,
                                text: "minarets",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 105049900,
                                text: "other towers",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                    , {
                        id: 105050000,
                        text: "Lighthouses",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 106000000,
                text: "Industrial facilities",
                checked: false,
                children: [
                    {
                        id: 106010000,
                        text: "Railway stations",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106020000,
                        text: "Factories",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106030000,
                        text: "Mints",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106040000,
                        text: "Power stations",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106050000,
                        text: "Dams",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106060000,
                        text: "Mills",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106070000,
                        text: "Abandoned railway stations",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106080000,
                        text: "Abandoned mineshafts",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106090000,
                        text: "Mineshafts",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 106990000,
                        text: "Other buildings",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 107000000,
                text: "Other",
                checked: false,
                children: [
                    {
                        id: 107010000,
                        text: "Sundials",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 107020000,
                        text: "View points",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 107030000,
                        text: "Red telephone boxes",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 107040000,
                        text: "unclassified attractions",
                        checked: false,
                        children: [
                            {
                                id: 107040100,
                                text: "tourist attractions",
                                checked: false,
                                children: []
                            }
                            , {
                                id: 107040200,
                                text: "historic attractions",
                                checked: false,
                                children: []
                            }
                        ]
                    }
                ]
            }
        ]
    }
    , {
        id: 200000000,
        text: "Amusements",
        checked: false,
        children: [
            {
                id: 201000000,
                text: "amusement parks",
                checked: false,
                children: []
            }
            , {
                id: 202000000,
                text: "miniature parks",
                checked: false,
                children: []
            }
            , {
                id: 203000000,
                text: "outdoor pools and water parks",
                checked: false,
                children: []
            }
            , {
                id: 204000000,
                text: "roller coasters",
                checked: false,
                children: []
            }
            , {
                id: 205000000,
                text: "ferris wheels",
                checked: false,
                children: []
            }
            , {
                id: 299000000,
                text: "other amusement rides",
                checked: false,
                children: []
            }
        ]
    }
    , {
        id: 300000000,
        text: "Sport",
        checked: false,
        children: [
            {
                id: 301000000,
                text: "winter sport",
                checked: false,
                children: [
                    {
                        id: 301010000,
                        text: "skiing",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 301020000,
                        text: "cross country skiing",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 301990000,
                        text: "other winter sports",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 302000000,
                text: "diving",
                checked: false,
                children: [
                    {
                        id: 302010000,
                        text: "dive centers",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 302020000,
                        text: "dive spots",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 302030000,
                        text: "wrecks",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 303000000,
                text: "climbing",
                checked: false,
                children: []
            }
            , {
                id: 304000000,
                text: "surfing",
                checked: false,
                children: []
            }
            , {
                id: 305000000,
                text: "kitesurfing",
                checked: false,
                children: []
            }
        ]
    }
    , {
        id: 400000000,
        text: "Adult",
        checked: false,
        children: [
            {
                id: 401000000,
                text: "strip clubs",
                checked: false,
                children: []
            }
            , {
                id: 402000000,
                text: "casino",
                checked: false,
                children: []
            }
            , {
                id: 403000000,
                text: "brothels",
                checked: false,
                children: []
            }
            , {
                id: 404000000,
                text: "nightclubs",
                checked: false,
                children: []
            }
            , {
                id: 405000000,
                text: "alcohol",
                checked: false,
                children: []
            }
            , {
                id: 406000000,
                text: "love hotels",
                checked: false,
                children: []
            }
            , {
                id: 407000000,
                text: "erotic shops",
                checked: false,
                children: []
            }
        ]
    }
    , {
        id: 500000000,
        text: "Tourist facilities",
        checked: false,
        children: [
            {
                id: 501000000,
                text: "Transport",
                checked: false,
                children: [
                    {
                        id: 501010000,
                        text: "Car rental",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501020000,
                        text: "Car sharing",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501030000,
                        text: "Car wash",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501040000,
                        text: "Charging stations",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501050000,
                        text: "Bicycle rental",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501060000,
                        text: "Boat sharing",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 501070000,
                        text: "Fuel",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 502000000,
                text: "Shops",
                checked: false,
                children: [
                    {
                        id: 502010000,
                        text: "Supermarkets",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502020000,
                        text: "Conveniences",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502030000,
                        text: "Outdoor",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502040000,
                        text: "Malls",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502050000,
                        text: "Marketplaces",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 502060000,
                        text: "Bakeries",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 503000000,
                text: "Foods",
                checked: false,
                children: [
                    {
                        id: 503010000,
                        text: "Restaurants",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503020000,
                        text: "Cafes",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503030000,
                        text: "Fast food",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503040000,
                        text: "Food courts",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503050000,
                        text: "Pubs",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503060000,
                        text: "Bars",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503070000,
                        text: "Biergartens",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 503080000,
                        text: "Picnic sites",
                        checked: false,
                        children: []
                    }
                ]
            }
            , {
                id: 504000000,
                text: "Banks",
                checked: false,
                children: [
                    {
                        id: 504010000,
                        text: "Banks",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 504020000,
                        text: "ATM",
                        checked: false,
                        children: []
                    }
                    , {
                        id: 504030000,
                        text: "Bureau de change",
                        checked: false,
                        children: []
                    }
                ]
            }
        ]
    }
    , {
        id: 600000000,
        text: "Accomodations",
        checked: false,
        children: [
            {
                id: 601000000,
                text: "Apartments",
                checked: false,
                children: []
            }
            , {
                id: 602000000,
                text: "Guest houses",
                checked: false,
                children: []
            }
            , {
                id: 603000000,
                text: "Campsites",
                checked: false,
                children: []
            }
            , {
                id: 604000000,
                text: "Resorts",
                checked: false,
                children: []
            }
            , {
                id: 605000000,
                text: "Motels",
                checked: false,
                children: []
            }
            , {
                id: 699000000,
                text: "Hotels",
                checked: false,
                children: []
            }
            , {
                id: 607000000,
                text: "Hostels",
                checked: false,
                children: []
            }
            , {
                id: 608000000,
                text: "Villas and chalet",
                checked: false,
                children: []
            }
            , {
                id: 609000000,
                text: "Alpine huts",
                checked: false,
                children: []
            }
            , {
                id: 610000000,
                text: "Love hotels",
                checked: false,
                children: []
            }
        ]
    }
];

let layer_names = {
    "items": {
        "101010100": {"n": "tidal island [s]", "i": 'park-15', "e": '🏝️'},
        "101010200": {"n": "inland island [s]", "i": 'park-15', "e": '🏝️'},
        "101010300": {"n": "coral island [s]", "i": 'park-15', "e": '🏝️'},
        "101010400": {"n": "desert island [s]", "i": 'park-15', "e": '🏝️'},
        "101010500": {"n": "high island [s]", "i": 'park-15', "e": '🏝️'},
        "101019900": {"n": "island[other islands]", "i": 'park-15', "e": '🏝️'},
        "101020100": {"n": "hot spring [s]", "i": 'park-15', "e": '💧'},
        "101020200": {"n": "geyser [s]", "i": 'park-15', "e": '💧'},
        "101020300": {"n": "spring[other springs]", "i": 'park-15', "e": '💧'},
        "101030100": {"n": "mountain peak [s]", "i": 'park-15', "e": '⛰️'},
        "101030200": {"n": "volcano  [es]", "i": 'park-15', "e": '⛰️'},
        "101030300": {"n": "cave [s]", "i": 'park-15', "e": '⛰️'},
        "101030400": {"n": "canyon [s]", "i": 'park-15', "e": '⛰️'},
        "101030500": {"n": "rock formation [s]", "i": 'park-15', "e": '⛰️'},
        "101040100": {"n": "crater lake [s]", "i": 'park-15', "e": '🌊'},
        "101040200": {"n": "rift lake [s]", "i": 'park-15', "e": '🌊'},
        "101040300": {"n": "salt lake [s]", "i": 'park-15', "e": '🌊'},
        "101040400": {"n": "dry lake [s]", "i": 'park-15', "e": '🌊'},
        "101040500": {"n": "reservoir [s]", "i": 'park-15', "e": '🌊'},
        "101040600": {"n": "river [s]", "i": 'park-15', "e": '🌊'},
        "101040700": {"n": "canal [s]", "i": 'park-15', "e": '🌊'},
        "101040800": {"n": "waterfall [s]", "i": 'park-15', "e": '🌊'},
        "101040900": {"n": "lagoon [s]", "i": 'park-15', "e": '🌊'},
        "101049900": {"n": "lake[other lakes]", "i": 'park-15', "e": '🌊'},
        "101050100": {"n": "golden sand beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101050200": {"n": "white sand beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101050300": {"n": "black sand beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101050400": {"n": "shingle beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101050500": {"n": "rocks beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101050600": {"n": "urbans beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101050700": {"n": "nude beach  [es]", "i": 'park-15', "e": '🏖️'},
        "101059900": {"n": "beach[other beaches]", "i": 'park-15', "e": '🏖️'},
        "101060100": {"n": "aquatic protected area [s]", "i": 'park-15', "e": '🏞️'},
        "101060200": {"n": "wildlife reserve [s]", "i": 'park-15', "e": '🏞️'},
        "101060300": {"n": "national park [s]", "i": 'park-15', "e": '🏞️'},
        "101060400": {"n": "nature reserve[other nature reserves]", "i": 'park-15', "e": '🏞️'},
        "101060500": {"n": "natural monument [s]", "i": 'park-15', "e": '🏞️'},
        "101069900": {"n": "nature conservation area [s]", "i": 'park-15', "e": '🏞️'},
        "101070000": {"n": "Glacier [s]", "i": 'park-15', "e": '🏔️'},
        "102010100": {"n": "national museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010200": {"n": "local museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010301": {"n": "maritime museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010302": {"n": "railway museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010303": {"n": "aviation museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010304": {"n": "automobile museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010305": {"n": "computer museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010306": {"n": "heritage railway [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010399": {"n": "technology museum[other technology museums]", "i": 'town-hall-15', "e": '🏛️'},
        "102010400": {"n": "science museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010500": {"n": "planetarium [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010600": {"n": "military museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010700": {"n": "history museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010800": {"n": "archaeological museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102010900": {"n": "biographical museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102011000": {"n": "open-air museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102011100": {"n": "fashion museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102011200": {"n": "children museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102011300": {"n": "historic house museum [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102011400": {"n": "art gallery  [ies]", "i": 'town-hall-15', "e": '🏛️'},
        "102011500": {"n": "zoo [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102011600": {"n": "aquarium [s]", "i": 'town-hall-15', "e": '🏛️'},
        "102019900": {"n": "museum[other museums]", "i": 'town-hall-15', "e": '🏛️'},
        "102020100": {"n": "sylvan theatre [s]", "i": 'town-hall-15', "e": '🎭'},
        "102020200": {"n": "opera house [s]", "i": 'town-hall-15', "e": '🎭'},
        "102020300": {"n": "music venue [s]", "i": 'town-hall-15', "e": '🎭'},
        "102020400": {"n": "concert hall [s]", "i": 'town-hall-15', "e": '🎭'},
        "102020500": {"n": "puppetry  [ies]", "i": 'town-hall-15', "e": '🎭'},
        "102020600": {"n": "сhildren\u0027s theatre [s]", "i": 'town-hall-15', "e": '🎭'},
        "102029900": {"n": "theatre[other theatres]", "i": 'town-hall-15', "e": '🎭'},
        "102030100": {"n": "wall painting", "i": 'town-hall-15', "e": '⛲'},
        "102030200": {"n": "street[squares and streets]", "i": 'town-hall-15', "e": '⛲'},
        "102030300": {"n": "installation", "i": 'town-hall-15', "e": '⛲'},
        "102030400": {"n": "park[gardens and parks]", "i": 'town-hall-15', "e": '⛲'},
        "102030500": {"n": "fountain [s]", "i": 'town-hall-15', "e": '⛲'},
        "102030600": {"n": "sculpture [s]", "i": 'town-hall-15', "e": '⛲'},
        "103010100": {"n": "historic district [s]", "i": 'castle-15', "e": '🛡️'},
        "103010200": {"n": "historic settlement [s]", "i": 'castle-15', "e": '🛡️'},
        "103010300": {"n": "fishing village [s]", "i": 'castle-15', "e": '🛡️'},
        "103010400": {"n": "battlefield [s]", "i": 'castle-15', "e": '🛡️'},
        "103020100": {"n": "castle [s]", "i": 'castle-15', "e": '🏰'},
        "103020200": {"n": "hillfort [s]", "i": 'castle-15', "e": '🏰'},
        "103020300": {"n": "fortified tower [s]", "i": 'castle-15', "e": '🏰'},
        "103020400": {"n": "defensive wall [s]", "i": 'castle-15', "e": '🏰'},
        "103020500": {"n": "bunker [s]", "i": 'castle-15', "e": '🏰'},
        "103020600": {"n": "kremlin [s]", "i": 'castle-15', "e": '🏰'},
        "103029900": {"n": "fortification[other fortifications]", "i": 'castle-15', "e": '🏰'},
        "103030100": {"n": "milestone [s]", "i": 'castle-15', "e": '🗿'},
        "103030200": {"n": "monument [s]", "i": 'castle-15', "e": '🗿'},
        "103040100": {"n": "megalith [s]", "i": 'castle-15', "e": '🏺'},
        "103040200": {"n": "menhir [s]", "i": 'castle-15', "e": '🏺'},
        "103040300": {"n": "roman villa [s]", "i": 'castle-15', "e": '🏺'},
        "103040400": {"n": "cave painting [s]", "i": 'castle-15', "e": '🏺'},
        "103040500": {"n": "settlement [s]", "i": 'castle-15', "e": '🏺'},
        "103040600": {"n": "rune stone [s]", "i": 'castle-15', "e": '🏺'},
        "103049900": {"n": "archaeological site [s]", "i": 'castle-15', "e": '🏺'},
        "103050100": {"n": "cemetery  [ies]", "i": 'castle-15', "e": '⚱️'},
        "103050200": {"n": "war grave [s]", "i": 'castle-15', "e": '⚱️'},
        "103050300": {"n": "necropolis  [es]", "i": 'castle-15', "e": '⚱️'},
        "103050400": {"n": "dolmen [s]", "i": 'castle-15', "e": '⚱️'},
        "103050500": {"n": "tumulus  [es]", "i": 'castle-15', "e": '⚱️'},
        "103050600": {"n": "mausoleum [s]", "i": 'castle-15', "e": '⚱️'},
        "103050700": {"n": "war memorial [s]", "i": 'castle-15', "e": '⚱️'},
        "103050800": {"n": "crypt [s]", "i": 'castle-15', "e": '⚱️'},
        "103059900": {"n": "burial place[other burial places]", "i": 'castle-15', "e": '⚱️'},
        "104010100": {"n": "eastern orthodox church  [es]", "i": 'place-of-worship-15', "e": '⛪'},
        "104010200": {"n": "catholic church  [es]", "i": 'place-of-worship-15', "e": '⛪'},
        "104019900": {"n": "church[other churches]", "i": 'place-of-worship-15', "e": '⛪'},
        "104020000": {"n": "cathedral [s]", "i": 'place-of-worship-15', "e": '🛐'},
        "104030000": {"n": "mosque [s]", "i": 'place-of-worship-15', "e": '🕌'},
        "104040000": {"n": "synagogue [s]", "i": 'place-of-worship-15', "e": '🕍'},
        "104050000": {"n": "buddhist temple [s]", "i": 'place-of-worship-15', "e": '☸️'},
        "104060000": {"n": "hindu temple [s]", "i": 'place-of-worship-15', "e": '🕉️'},
        "104070000": {"n": "egyptian temple [s]", "i": 'place-of-worship-15', "e": '🛐'},
        "104990000": {"n": "temple[other temples]", "i": 'place-of-worship-15', "e": '🛐'},
        "104090000": {"n": "monastery  [ies]", "i": 'place-of-worship-15', "e": '🛐'},
        "105010100": {"n": "pyramid [s]", "i": 'building-alt1-15', "e": '🏡'},
        "105010200": {"n": "amphitheatre [s]", "i": 'building-alt1-15', "e": '🏡'},
        "105010300": {"n": "triumphal arch [s]", "i": 'building-alt1-15', "e": '🏡'},
        "105010400": {"n": "palace [s]", "i": 'building-alt1-15', "e": '🏡'},
        "105010500": {"n": "manor house [s]", "i": 'building-alt1-15', "e": '🏡'},
        "105010600": {"n": "wineries", "i": 'building-alt1-15', "e": '🏡'},
        "105010700": {"n": "farms", "i": 'building-alt1-15', "e": '🏡'},
        "105019900": {"n": "structure[buildings and structures]", "i": 'building-alt1-15', "e": '🏡'},
        "105010900": {"n": "destroyed object [s]", "i": 'building-alt1-15', "e": '🏡'},
        "105020000": {"n": "Skyscraper [s]", "i": 'building-alt1-15', "e": '🏙️'},
        "105030100": {"n": "moveable bridge [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105030200": {"n": "stone bridge [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105030300": {"n": "viaduct [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105030400": {"n": "Roman bridge [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105030500": {"n": "footbridge [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105030600": {"n": "aqueduct [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105030700": {"n": "suspension bridge [s]", "i": 'building-alt1-15', "e": '🌉'},
        "105039900": {"n": "bridge[other bridges]", "i": 'building-alt1-15', "e": '🌉'},
        "105040100": {"n": "observation tower [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105040200": {"n": "watchtower [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105040300": {"n": "water tower [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105040400": {"n": "transmitter tower [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105040500": {"n": "clock tower [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105040600": {"n": "bell tower [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105040700": {"n": "minaret [s]", "i": 'building-alt1-15', "e": '🗼'},
        "105049900": {"n": "tower[other towers]", "i": 'building-alt1-15', "e": '🗼'},
        "105050000": {"n": "Lighthouse [s]", "i": 'building-alt1-15', "e": '🗼'},
        "106010000": {"n": "Railway station [s]", "i": 'industry-15', "e": '🏭'},
        "106020000": {"n": "Factory  [ies]", "i": 'industry-15', "e": '🏭'},
        "106030000": {"n": "Mint [s]", "i": 'industry-15', "e": '🏭'},
        "106040000": {"n": "Power station [s]", "i": 'industry-15', "e": '🏭'},
        "106050000": {"n": "Dam [s]", "i": 'industry-15', "e": '🏭'},
        "106060000": {"n": "Mill [s]", "i": 'industry-15', "e": '🏭'},
        "106070000": {"n": "Abandoned railway station [s]", "i": 'industry-15', "e": '🏭'},
        "106080000": {"n": "Abandoned mineshaft [s]", "i": 'industry-15', "e": '🏭'},
        "106090000": {"n": "Mineshaft [s]", "i": 'industry-15', "e": '🏭'},
        "106990000": {"n": "Building[Other buildings]", "i": 'industry-15', "e": '🏭'},
        "107010000": {"n": "Sundial [s]", "i": 'attraction-15', "e": '📸'},
        "107020000": {"n": "View point [s]", "i": 'attraction-15', "e": '📸'},
        "107030000": {"n": "Red telephone box  [es]", "i": 'attraction-15', "e": '📸'},
        "107040100": {"n": "tourist attraction [s]", "i": 'attraction-15', "e": '📸'},
        "107040200": {"n": "historic attraction [s]", "i": 'attraction-15', "e": '📸'},
        "201000000": {"n": "amusement park [s]", "i": 'amusement-park-15', "e": '📸'},
        "202000000": {"n": "miniature park [s]", "i": 'amusement-park-15', "e": '📸'},
        "203000000": {"n": "outdoor pools and water park [s]", "i": 'amusement-park-15', "e": '📸'},
        "204000000": {"n": "roller coaster [s]", "i": 'amusement-park-15', "e": '📸'},
        "205000000": {"n": "ferris wheel [s]", "i": 'amusement-park-15', "e": '📸'},
        "299000000": {"n": "amusement ride[other amusement rides]", "i": 'amusement-park-15', "e": '📸'},
        "301010000": {"n": "skiing", "i": 'tennis-15', "e": '📸'},
        "301020000": {"n": "cross country skiing", "i": 'tennis-15', "e": '📸'},
        "301990000": {"n": "winter sport[other winter sports]", "i": 'tennis-15', "e": '📸'},
        "302010000": {"n": "dive center [s]", "i": 'tennis-15', "e": '📸'},
        "302020000": {"n": "dive spot [s]", "i": 'tennis-15', "e": '📸'},
        "302030000": {"n": "wreck [s]", "i": 'tennis-15', "e": '📸'},
        "303000000": {"n": "climbing", "i": 'tennis-15', "e": '📸'},
        "304000000": {"n": "surfing", "i": 'tennis-15', "e": '📸'},
        "305000000": {"n": "kitesurfing", "i": 'tennis-15', "e": '📸'},
        "401000000": {"n": "strip club [s]", "i": 'heart-15', "e": '📸'},
        "402000000": {"n": "casino", "i": 'heart-15', "e": '📸'},
        "403000000": {"n": "brothel [s]", "i": 'heart-15', "e": '📸'},
        "404000000": {"n": "nightclub [s]", "i": 'heart-15', "e": '📸'},
        "405000000": {"n": "alcohol", "i": 'alcohol-shop-15', "e": '📸'},
        "406000000": {"n": "love hotel [s]", "i": 'heart-15', "e": '📸'},
        "407000000": {"n": "erotic shop [s]", "i": 'heart-15', "e": '📸'},
        "501010000": {"n": "Car rental", "i": 'car-15', "e": '📸'},
        "501020000": {"n": "Car sharing", "i": 'car-15', "e": '📸'},
        "501030000": {"n": "Car wash", "i": 'car-15', "e": '📸'},
        "501040000": {"n": "Charging station [s]", "i": 'fuel-15', "e": '📸'},
        "501050000": {"n": "Bicycle rental", "i": 'bicycle-15', "e": '📸'},
        "501060000": {"n": "Boat sharing", "i": 'harbor-15', "e": '📸'},
        "501070000": {"n": "Fuel", "i": 'fuel-15', "e": '📸'},
        "502010000": {"n": "Supermarket [s]", "i": 'grocery-15', "e": '📸'},
        "502020000": {"n": "Convenience [s]", "i": 'shop-15', "e": '📸'},
        "502030000": {"n": "Outdoor", "i": 'commercial-15', "e": '📸'},
        "502040000": {"n": "Mall [s]", "i": 'commercial-15', "e": '📸'},
        "502050000": {"n": "Marketplace [s]", "i": 'commercial-15', "e": '📸'},
        "502060000": {"n": "Bakery  [ies]", "i": 'bakery-15', "e": '📸'},
        "503010000": {"n": "Restaurant [s]", "i": 'restaurant-15', "e": '📸'},
        "503020000": {"n": "Cafe [s]", "i": 'cafe-15', "e": '📸'},
        "503030000": {"n": "Fast food", "i": 'fast-food-15', "e": '📸'},
        "503040000": {"n": "Food court [s]", "i": 'fast-food-15', "e": '📸'},
        "503050000": {"n": "Pub [s]", "i": 'beer-15', "e": '📸'},
        "503060000": {"n": "Bar [s]", "i": 'bar-15', "e": '📸'},
        "503070000": {"n": "Biergarten [s]", "i": 'beer-15', "e": '📸'},
        "503080000": {"n": "Picnic site [s]", "i": 'beer-15', "e": '📸'},
        "504010000": {"n": "Bank [s]", "i": 'bank-15', "e": '📸'},
        "504020000": {"n": "ATM", "i": 'bank-15', "e": '📸'},
        "504030000": {"n": "Bureau de change", "i": 'bank-15', "e": '📸'},
        "601000000": {"n": "Apartment [s]", "i": 'lodging-15', "e": '📸'},
        "602000000": {"n": "Guest house [s]", "i": 'lodging-15', "e": '📸'},
        "603000000": {"n": "Campsite [s]", "i": 'lodging-15', "e": '📸'},
        "604000000": {"n": "Resort [s]", "i": 'lodging-15', "e": '📸'},
        "605000000": {"n": "Motel [s]", "i": 'lodging-15', "e": '📸'},
        "699000000": {"n": "Hotel [s]", "i": 'lodging-15', "e": '📸'},
        "607000000": {"n": "Hostel [s]", "i": 'lodging-15', "e": '📸'},
        "608000000": {"n": "Villa [Villas and chalet]", "i": 'lodging-15', "e": '📸'},
        "609000000": {"n": "Alpine hut [s]", "i": 'lodging-15', "e": '📸'},
        "610000000": {"n": "Love hotel [s]", "i": 'lodging-15', "e": '📸'},
    },
    "groups": {
        "100000000": {"n": "Interesting places", "i": 'park-15', "e": '🏝️'},
        "101000000": {"n": "Natural", "i": 'park-15', "e": '🏝️'},
        "101010000": {"n": "Islands", "i": 'park-15', "e": '🏝️'},
        "101020000": {"n": "Natural springs", "i": 'park-15', "e": '💧'},
        "101030000": {"n": "Geological formations", "i": 'park-15', "e": '⛰️'},
        "101040000": {"n": "Water", "i": 'park-15', "e": '🌊'},
        "101050000": {"n": "Beaches", "i": 'park-15', "e": '🏖️'},
        "101060000": {"n": "Nature reserves", "i": 'park-15', "e": '🏞️'},
        "102000000": {"n": "Cultural", "i": 'town-hall-15', "e": '🏛️'},
        "102010000": {"n": "Museums", "i": 'town-hall-15', "e": '🏛️'},
        "102010300": {"n": "museums of science and technology", "i": 'town-hall-15', "e": '🏛️'},
        "102020000": {"n": "Theatres and concert halls", "i": 'town-hall-15', "e": '🎭'},
        "102030000": {"n": "Urban environment", "i": 'town-hall-15', "e": '⛲'},
        "103000000": {"n": "Historical", "i": 'castle-15', "e": '🛡️'},
        "103010000": {"n": "Historical places", "i": 'castle-15', "e": '🛡️'},
        "103020000": {"n": "Fortifications", "i": 'castle-15', "e": '🏰'},
        "103030000": {"n": "Monuments and memorials", "i": 'castle-15', "e": '🗿'},
        "103040000": {"n": "Archaeology", "i": 'castle-15', "e": '🏺'},
        "103050000": {"n": "Burial places", "i": 'castle-15', "e": '⚱️'},
        "104000000": {"n": "Religion", "i": 'place-of-worship-15', "e": '⛪'},
        "104010000": {"n": "churches", "i": 'place-of-worship-15', "e": '⛪'},
        "105000000": {"n": "Architecture", "i": 'building-alt1-15', "e": '🏡'},
        "105010000": {"n": "Historic architecture", "i": 'building-alt1-15', "e": '🏡'},
        "105030000": {"n": "Bridges", "i": 'building-alt1-15', "e": '🌉'},
        "105040000": {"n": "Towers", "i": 'building-alt1-15', "e": '🗼'},
        "106000000": {"n": "Industrial facilities", "i": 'industry-15', "e": '🏭'},
        "107000000": {"n": "Other", "i": 'attraction-15', "e": '📸'},
        "107040000": {"n": "unclassified attraction [s]", "i": 'attraction-15', "e": '📸'},
        "200000000": {"n": "Amusements", "i": 'amusement-park-15', "e": '📸'},
        "300000000": {"n": "Sport", "i": 'tennis-15', "e": '📸'},
        "301000000": {"n": "winter sport", "i": 'tennis-15', "e": '📸'},
        "302000000": {"n": "diving", "i": 'tennis-15', "e": '📸'},
        "400000000": {"n": "Adult", "i": 'heart-15', "e": '📸'},
        "500000000": {"n": "Tourist facilities", "i": 'car-15', "e": '📸'},
        "501000000": {"n": "Transport", "i": 'car-15', "e": '📸'},
        "502000000": {"n": "Shops", "i": 'grocery-15', "e": '📸'},
        "503000000": {"n": "Foods", "i": 'restaurant-15', "e": '📸'},
        "504000000": {"n": "Banks", "i": 'bank-15', "e": '📸'},
        "600000000": {"n": "Accomodations", "i": 'lodging-15', "e": '📸'},
        "700000000": {"n": 'camera [s]'}
    }
};

let mega_names = {
    'interesting_places' : '100000000',
    'unclassified_objects' : '107040000',
    'view_points' : '107020000',
    'amusements' : '200000000',
    'sport' : '300000000',
    'winter_sports' : '301000000',
    'adult' : '400000000',
    'tourist_facilities' : '500000000',
    'foods' : '503000000',
    'shops' : '502000000',
    'accomodations' : '600000000',
    'cameras' : '700000000',
    'beaches' : '101050000',
    'waterfalls': '101040800',
    'caves': '101030300',
    'canyons': '101030400',
    'volcanoes': '101030200'
};

let base_layer_names = {

    aerodrome_label :{
        'airport': {n: 'Airport [s]', i:'airport-15'},
        'international_airport': {n: 'International [е] airport [s]', i:'airport-15'}
    },
    poi : {
        'airport': {n: 'Airport [s]', i:'airport-15'},
        'train_station': {n: 'Train station [s]', i:'rail-15'},
        'bus_stop': {n: 'Bus stop [s]', i:'bus-15'},
        'tram_stop': {n: 'Tram stop [s]', i:'rail-light-15'},
        'subway_entrance': {n: 'Subway entrance [s]', i:'rail-metro-15'},
        'harbor_marina': {n: 'harbor marina [s]', i:'harbor-15'},
        'post_office': {n: 'Post Office [s]', i:'post-15'},
        'information': {n: 'Information [s]', i:'information-15'},
        'police': {n: 'Police [s]', i:'police-15'},
        'hospital': {n: 'Hospital [s]', i:'doctor-15'},
        'pharmacy': {n: 'Pharmacy  [ies]', i:'hospital-15'},
        'shower': {n: 'Shower [s]', i:'water-15'},
        'toilets': {n: 'Toilet [s]', i:'toilets-15'},
        'parking': {n: 'Parking [s]', i:'parking-15'}
    },
    pois : {
        'airport': {n: 'Airport [s]', i:'airport-15'},
        'train_station': {n: 'Train station [s]', i:'rail-15'},
        'bus_stop': {n: 'Bus stop [s]', i:'bus-15'},
        'tram_stop': {n: 'Tram stop [s]', i:'rail-light-15'},
        'subway_entrance': {n: 'Subway entrance [s]', i:'rail-metro-15'},
        'harbor_marina': {n: 'harbor marina [s]', i:'harbor-15'},
        'post_office': {n: 'Post Office [s]', i:'post-15'},
        'information': {n: 'Information [s]', i:'information-15'},
        'police': {n: 'Police [s]', i:'police-15'},
        'hospital': {n: 'Hospital [s]', i:'doctor-15'},
        'pharmacy': {n: 'Pharmacy  [ies]', i:'hospital-15'},
        'shower': {n: 'Shower [s]', i:'water-15'},
        'toilets': {n: 'Toilet [s]', i:'toilets-15'},
        'parking': {n: 'Parking [s]', i:'parking-15'}
    },
    place:{
        'city': {n: 'City  [ies]', i:'building-alt1-15'},
        'town': {n: 'Town [s]', i:'building-alt1-15'},
        'village': {n: 'Village [s]', i:'building-alt1-15'},
        'hamlet': {n: 'Hamlet [s]', i:'building-alt1-15'},
        'locality': {n: 'Locality  [ies]', i:'building-alt1-15'}
    }
};

exports.mega_names = mega_names;
exports.base_layer_names = base_layer_names;
exports.layer_names = layer_names;
exports.layer_catalog = layer_catalog;
const EventEmitter = require('events');
import {
    config
} from "./config";

export default class BasemapsMenu extends EventEmitter {

    constructor() {
        super();
    }
    //Передаем в метод массив объектов вида {basemapNameRus: '', basemapNameEng: ''}
    //У подложки по умолчанию должно быть также в наличии свойство 'default'
    show() {
        var self = this;
        let anchors = "";
        for (let key in config.getBaseMaps()) {
            let name = config.getBaseMaps()[key].basemapNameRus;
            if (name.startsWith("txt.")){
                name = LOC.txt[name.substring(4)];
            }
            let anchorClass = 'basemaps__item';
            let iconClass = 'basemaps__icon';
            if (config.currentBaseMap === key) {
                anchorClass += ' active';
                iconClass += ' active';
            }
            anchors += `<a id="${key}" class="${anchorClass}"><i class="fa fa-check ${iconClass}"></i>${name}</a>`;

        }
        let basemapsBlock = document.querySelector('.basemaps');
        basemapsBlock.innerHTML = anchors;

        basemapsBlock.addEventListener('click', function(event) {
            let item = config.getBaseMaps()[event.target.id];
            if (item){
                basemapsBlock.classList.toggle('active');
                let children = basemapsBlock.querySelectorAll('*');
                for (let n = 0; n < children.length; ++n){
                    children[n].classList.remove("active");
                }
                let node = event.target;
                node.classList.toggle('active');
                for (let n = 0; n < node.children.length; ++n){
                    node.children[n].classList.toggle('active');
                }

                let basemapsFilter = document.querySelector('.basemapsNavigation__basemapsFilter');
                basemapsFilter.classList.toggle('active');
                for (let n = 0; n < basemapsFilter.children.length; ++n){
                    basemapsFilter.children[n].classList.toggle('active');
                }

                self.emit('changed', {
                    id: event.target.id
                });
            }
        });
/*
        $(basemapEl).click(function () {
            $('.basemaps').toggleClass("active");
            $('.basemaps').find('*').removeClass("active");
            $(this).toggleClass("active");
            $(this).children().toggleClass("active");
            $('.basemapsNavigation__basemapsFilter').toggleClass("active");
            $('.basemapsNavigation__basemapsFilter').children().toggleClass("active");
            let basemapId = $(this).attr('id');
            self.emit('changed', {
                id: basemapId
            });
        });*/
    }
}
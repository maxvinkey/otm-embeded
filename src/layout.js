import {
    config,
    Layouts
} from "./config";
import {global} from "./global";

function setMapSideBarEventsForMobDevice() {
    if (($('#profile-tab').hasClass('active'))) {
        //два условия ниже для случае если при активных маршрутах (profile) на открытой карте
        //переместились с уровня кластеров до уровня пои
        $('.objectsFilter').css('display', 'none');
        $('.routesNavigation__routesFilter.onCard').css('display', 'none');
        //обработчик этого клика остается после каждого вызова setmapsidebar, обработчик удаляется только после фактического клика.
        //А если так и не кликнули, количество обработчиков может перегрузить очередь, как этого избежать?(
        $('#home-tab').one('click', function showFilter() {
            if ($('#home').attr('data-layer') != 'clusters') {
                $('.objectsFilter').css('display', 'block');
                $('.routesNavigation__routesFilter.onCard').css('display', 'block');
                let vh = window.innerHeight * 0.01;
                $('#home').css('height', `${100*vh-161}px`);
                //кроме того, если тут опять переключились на profile то фильтр остается(
                //если опять вешать обработчик клика, получится опять какая-то жуткая замкнутость
            }
        });
    }

    //map.click.unbound

    $('#backToMap').one('click', function showMap() {
        setLayout(Layouts.MAP);
    });
}

function updateLayout(layout) {
    let vh = window.innerHeight * 0.01;
    if (layout === Layouts.MAP_WITH_SIDEBAR) {
        $('#home').css('height', `${100*vh-85}px`);
        $('#profile').css('height', `${100*vh-85}px`);
        $('.objectCard').css('height', `${100*vh-54}px`);
        $('.mapSideBar').css('height', `${100*vh-38}px`);
    }
    if (layout === Layouts.MAP) {
        $('#home').css('height', `${100*vh-85}px`);
        $('#profile').css('height', `${100*vh-85}px`);
        $('.objectCard').css('height', `${100*vh-78}px`);
    }
    if (layout === Layouts.SIDEBAR) {
        $('.objectCard').css('height', `${100*vh-78}px`);
        $('.mapSideBar').css('height', `${100*vh-38}px`);
        $('#profile').css('height', `${100*vh-117}px`);
        $('#home').css('height', `${100*vh-161}px`);
        if ($('#home').attr('data-layer') === 'clusters') {
            $('#home').css('height', `${100*vh-117}px`);
        }
    }
}

export function setLayout(layout) {
    let vh = window.innerHeight * 0.01;
    if (layout === Layouts.MAP_WITH_SIDEBAR) {
        $('#home').css('height', `${100*vh-85}px`);
        $('#profile').css('height', `${100*vh-85}px`);
        $('.objectCard').css('height', `${100*vh-54}px`);
        $('.mapSideBarCollapse__show').css('display', 'none');
        $('.mapSideBar').css('display', 'block')
            .css('width', '258px')
            .css('height', `${100*vh-38}px`);
        $('#geocoder').css('display', 'block');
        $('#map').css('display', 'block')
            .css('width', 'calc(100% - 258px)')
            .css('left', '258px');
        $('.routesNavigation__routesFilter').css('display', 'block');
        $('.objectsFilter.onCard').css('display', 'none');
        $('.mapboxgl-canvas').css('width', 'calc(100% - 258px)');
        $('#backToMap').css('display', 'none');
        $('.basemapsNavigation').css('display', 'block');
    }

    if (layout === Layouts.MAP) {
        $('#home').css('height', `${100*vh-85}px`);
        $('#profile').css('height', `${100*vh-85}px`);

        $("#map").css('display', 'block')
            .css('width', '100%')
            .css('left', '0');
        $('.mapSideBarCollapse__show')
            .css('display', 'block')
            .text(LOC.txt.places_to_go);
        $('.mapSideBar').css('display', 'none');
        $('#geocoder').css('display', 'none');
        $('.routesNavigation__routesFilter').css('display', 'block');
        $('.mapboxgl-canvas').css('width', '100%');
        $('.basemapsNavigation').css('display', 'block');
        $('.objectCard').css('height', `${100*vh-78}px`);
    }

    if (layout === Layouts.SIDEBAR) {
        $('.objectCard').css('height', `${100*vh-78}px`);
        $('.mapSideBarCollapse__show').css('display', 'none');
        $('.mapSideBar').css('display', 'block')
            .css('width', '100%')
            .css('height', `${100*vh-38}px`);
        $('#geocoder').css('display', 'block');
        $("#map").css('display', 'none');
        $('#home-tab').css('width', '100%');
        $('#profile-tab').css('width', '100%');
        $('.nav-tabs .nav-item').css('width', '50%');
        $('#myTabContent').css('width', '100%');
        $('.basemapsNavigation').css('display', 'none');
        $('#backToMap').css('display', 'block')
            .text(LOC.txt.back_to_map);

        $('#profile').css('height', `${100*vh-117}px`);
        $('#home').css('height', `${100*vh-161}px`);


        $('.mapboxgl-canvas').css('width', 'calc(100% - 258px)');

        $('.routesNavigation__routesFilter').css('display', 'none')
        $('.routesNavigation__routesFilter').removeClass("active")
            .children().removeClass("active");
        $('.categories').removeClass("active");

        if ($('#home').attr('data-layer') === 'clusters') {
            $('#home').css('height', `${100*vh-117}px`);
            $('.objectsFilter').css('display', 'none');
            $('.routesNavigation__routesFilter.onCard').css('display', 'none');
        }

        if (($('#home').attr('data-layer') === 'vpois') && ($('#home-tab').hasClass('active'))) {
            $('.objectsFilter').css('display', 'block');
            $('.routesNavigation__routesFilter.onCard').css('display', 'block');
        }
        setMapSideBarEventsForMobDevice();
    }

    config.layout = layout;
    global.map.resize();
}

export function showMapSideBar() {
    if (config.getWidth() <= 768) {
        setLayout(Layouts.SIDEBAR);
    } else {
        setLayout(Layouts.MAP_WITH_SIDEBAR);
        //window.layersState.rebuild();
    }
}

export function resizeLayout() {
    if (config.getWidth() <= 768) {
        if (config.layout === Layouts.MAP_WITH_SIDEBAR) {
            setLayout(Layouts.MAP);
            $('.mapSideBarCollapse__hide').css('display', 'none'); //это если сайдбар был виден перед тем как экран уменьшили
        }
        updateLayout(config.layout);
    } else {
        setLayout(Layouts.MAP_WITH_SIDEBAR); //это режим у нас устанавливается, даже если есть minimap. это ок?
    }
}
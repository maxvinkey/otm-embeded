import {
    CategSet
} from "./categset";
import {
    extractWiki,
    getOSMIdentificator,
    intPaddingZero,
    loadOSMProps,
    isTypeOf,
    postJSON
} from "./utils";
import {
    cyrillicToTranslit
} from './CyrillicToTranslit';
import 'whatwg-fetch';
import {
    config, serverHost
} from "../config";

//const wdk = require('wikidata-sdk')

function getPreviewFeatures(source_name, ids) {
    return new Promise(function (resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState !== 4) {
                return;
            }
            if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                reject('');
                return;
            }
            resolve(JSON.parse(xmlHttp.response));
        };
        let url = serverHost + '/feature/' + config.getOTMLang() + '/' + source_name;
        xmlHttp.open('POST', url, true);
        xmlHttp.setRequestHeader('Content-Type', 'text/plain; charset=UTF-8');
        xmlHttp.send(ids);
    });
}

function getShortFeature(id, mainkind, preview) {
    return new Promise(function (resolve, reject) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState !== 4) {
                return;
            }
            if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                reject('');
                return;
            }

            let c = JSON.parse(xmlHttp.response);
            if (c.properties.id) {
                c.properties.osm_id = c.properties.id;
            }
            c.id = id;
            c.properties.catset = new CategSet();
            c.properties.catset.fromString(c.properties.kind);
            c.properties.kind = c.properties.catset.getIntersection(c.properties.catset, LOC.layer_names.items);
            c.properties.mainkind = mainkind;
            if (c.properties.extended && c.properties.extended.schedule) {
                c.properties['opening_hours'] = c.properties.extended.schedule;
            }
            c.source_layer = "pois";
            c.source_name = "otmpoi";
            resolve(c);
        };
        let url = serverHost + '/feature/' + config.getOTMLang() + '/' + id + '?ext=1&preview='+preview;
        xmlHttp.open('GET', url, true);
        xmlHttp.setRequestHeader('Accept', 'application/json');
        xmlHttp.send(null);
    });

}

function getFeatureP(id, mainkind) {
    return new Promise(function (resolve, reject) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState !== 4) {
                return;
            }
            if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                reject('');
                return;
            }

            var c = JSON.parse(xmlHttp.response);
            if (c.properties.id) {
                c.properties.osm_id = c.properties.id;
            }
            c.id = id;
            c.properties.catset = new CategSet();
            c.properties.catset.fromString(c.properties.kind);
            c.properties.kind = c.properties.catset.getIntersection(c.properties.catset, LOC.layer_names.items);
            c.properties.mainkind = mainkind;
            if (c.properties.extended && c.properties.extended.schedule) {
                c.properties['opening_hours'] = c.properties.extended.schedule;
            }
            resolve(c);
        };
        var url = serverHost + '/feature/' + config.getOTMLang() + '/' + id;
        xmlHttp.open('GET', url, true);
        xmlHttp.setRequestHeader('Accept', 'application/json');
        xmlHttp.send(null);
    });
};

function getADDR(feature) {
    return new Promise(function (resolve, reject) {
        var nominatimAPI = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${feature.geometry.coordinates[1]}&lon=${feature.geometry.coordinates[0]}&zoom=18&addressdetails=1&accept-language=${config.getLangPriority()}`;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState !== 4) {
                return;
            }
            if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                resolve(null);
                return;
            }
            resolve(JSON.parse(xmlHttp.response).address);
            return;
        };
        xmlHttp.open('POST', nominatimAPI, true);
        xmlHttp.setRequestHeader('Accept', 'application/json');
        xmlHttp.send(null);
    });
}

function getOSMP(feature) {
    return new Promise(function (resolve, reject) {
        var osmid = getOSMIdentificator(feature);
        if (!osmid)
            return resolve(feature);
        var url = "https://www.openstreetmap.org" + "/api/0.6/" + osmid.osm_type + "/" + osmid.osm_id;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState !== 4) {
                return;
            }
            if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
                resolve(feature);
                return;
            }
            var data = xmlHttp.responseXML;
            if (data.hasChildNodes()) {
                var itemOSM = data.childNodes.item(0);
                for (var i = 0; i < itemOSM.childNodes.length; i++) {
                    var item = itemOSM.childNodes.item(i);
                    if (item.id && item.id == osmid.osm_id) {
                        var osm = loadOSMProps(item);
                        for (var prop in osm) {
                            if (feature.properties[prop] === undefined) {
                                if (prop == 'phone') {
                                    feature.properties[prop] = osm[prop];
                                    feature.osm = true;
                                }
                                if (prop == 'url') {
                                    feature.properties[prop] = osm[prop];
                                    feature.osm = true;
                                }
                            }
                        }
                    }
                }
            }
            resolve(feature);
            return;
        };
        xmlHttp.open('GET', url, true);
        //xmlHttp.setRequestHeader('X-Requested-With','opentripmap');
        xmlHttp.setRequestHeader('Accept', 'text/xml');
        xmlHttp.send(null);
    });
}
/*
function getWikiDataP(lang, feature) {
    return new Promise(function (resolve, reject) {
        var wikiAPI = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids=Q" + feature.properties.wikidata;
        $.ajax({
            url: wikiAPI,
            jsonp: "callback",
            dataType: "jsonp",
            success: function (data) {
                if (data.entities && data.entities["Q" + feature.properties.wikidata]) {
                    var wikidata = data.entities["Q" + feature.properties.wikidata];
                    var useful_wikidata = {
                        descr: getWikiDataDescription(lang, wikidata.descriptions)
                    };
                    if (wikidata.claims) {
                        var urlSnack = getStringSnackValue(wikidata.claims, "P856");
                        if (urlSnack) {
                            useful_wikidata.url = urlSnack;
                        }
                        var imageSnack = getStringSnackValue(wikidata.claims, "P18");
                        if (imageSnack) {
                            if (!imageSnack.endsWith("location_map.svg")) {
                                useful_wikidata.image = imageSnack
                            }
                        }
                        var inceptionSnack = getStringSnackValue(wikidata.claims, "P571");
                        if (inceptionSnack) {
                            useful_wikidata.start_date = wdk.wikidataTimeToSimpleDay(inceptionSnack);
                        }
                    }
                    for (var prop in useful_wikidata) {
                        if (useful_wikidata[prop]) {
                            feature.wikidata = useful_wikidata;
                            break;
                        }
                    }
                }
            },
            complete: function () {
                resolve(feature);
            }
        });


    });
}
*/
function parseWikipediaResponseP(data, pageid, feature) {
    if (data.query && data.query.pages) {
        var page = null;
        if (!pageid) {
            for (pageid in data.query.pages) {
                page = data.query.pages[pageid];
                if (page)
                    break;
            }
        } else
            page = data.query.pages[pageid];

        if (!page)
            return feature;

        if (page.pageimage && page.pageimage.endsWith("location_map.svg")) {
            page.pageimage = null;
            page.thumbnail = null;
        }
        page.extract = page.extract ? extractWiki(page) : null;
        feature.wikipage = {
            extract: page.extract,
            image: page.pageimage
        };
        if (page.thumbnail) {
            feature.wikipage.thumbnail = {
                source: page.thumbnail.source,
                height: page.thumbnail.height,
                width: page.thumbnail.width
            }
        }
    }
}

function getWikiExtractByTitleP(thumbsize, wiki_lang, title, feature, extractText) {
    var wikiAPI = "//" + wiki_lang + ".wikipedia.org/w/api.php?action=query&format=json&utf8=1&pithumbsize=" + thumbsize + "&prop=pageimages";
    if (extractText) {
        wikiAPI += "%7Cextracts&exintro=1";
    }
    wikiAPI += "&redirects&titles=" + encodeURIComponent(title);

    return new Promise(function (resolve, reject) {
        $.ajax({
            url: wikiAPI,
            jsonp: "callback",
            dataType: "jsonp",
            success: function (data) {
                parseWikipediaResponseP(data, null, feature);
            },
            complete: function () {
                resolve(feature);
            }
        });
    });
}

function createImageDiv(thumbnail, href, height, width) {
    var div_img = $('<div class="image" id="tag-thumbnail">' +
        '<a target="_blank" href="' + href + '"><img itemprop="photo"></></a></div>');
    div_img.find("img").attr("src", thumbnail)
        .css("margin-left", "auto").css("margin-right", "auto").css("display", "block")
        .height(height).width(width);
    div_img.find("a").attr("href", href);
    return div_img.html();
}

function getImageUrl(feature, w, h) {
    if (feature.wikipage && feature.wikipage.thumbnail) {
        var thumbnail = feature.wikipage.thumbnail;
        return thumbnail.source;
    }
    if (isTypeOf('winter_sports', feature) && feature.properties.extended.id) {
        var sid = intPaddingZero(feature.properties.extended.id, 4);
        var url = 'https://data.opentripmap.com/images/snow/' + sid.substr(0, 2) + '/' + feature.properties.extended.id + '.png';
        return url;
    }
    if (feature.properties.extended && feature.properties.extended.image) {
        if (feature.properties.extended.image.startsWith("culture.ru:")) {
            var image = feature.properties.extended.image.substr(11);
            var prefix = "https://pro.culture.ru/uploads/";
            var splits = image.split('.');
            var preview = prefix + splits[0] + '_w' + w + '_h' + h + '.' + splits[1];
            return preview;
        } else if (feature.properties.extended.src && feature.properties.extended.src == 'belozersk') {
            var image = feature.properties.extended.image;
            var prefix = 'https://data.opentripmap.com/images/user/' + image + "/";
            var preview = prefix + w + ".png";
            return preview;
        }
    }
    return "";
}

function getImageTag(feature, w, h) {
    if (feature.wikipage && feature.wikipage.thumbnail) {
        var thumbnail = feature.wikipage.thumbnail;
        var url = 'http://commons.wikipedia.org/wiki/';
        return createImageDiv(thumbnail.source, url + "File:" + image, thumbnail.height, thumbnail.width);
    }
    if (isTypeOf('winter_sports', feature) && feature.properties.extended.id) {
        var sid = intPaddingZero(feature.properties.extended.id, 4);
        var url = 'https://data.opentripmap.com/images/snow/' + sid.substr(0, 2) + '/' + feature.properties.extended.id + '.png';
        return createImageDiv(url, url, w, h);
    }
    if (feature.properties.extended && feature.properties.extended.image) {
        if (feature.properties.extended.image.startsWith("culture.ru:")) {
            var image = feature.properties.extended.image.substr(11);
            var prefix = "https://pro.culture.ru/uploads/";
            var splits = image.split('.');
            var preview = prefix + splits[0] + '_w' + w + '_h' + h + '.' + splits[1];
            return createImageDiv(preview, prefix + image, w, h);
        } else if (feature.properties.extended.src && feature.properties.extended.src == 'belozersk') {
            var image = feature.properties.extended.image;
            var prefix = 'https://data.opentripmap.com/images/user/' + image + "/";
            var preview = prefix + w + ".png";
            return createImageDiv(preview, prefix + "original.jpg");
        }
    }
    return "";
}


function bindAll(fns, context) {
    fns.forEach(function (fn) {
        if (!context[fn]) {
            return;
        }
        context[fn] = context[fn].bind(context);
    });
}

var Hash = function Hash() {
    bindAll([
        '_onHashChange',
        '_updateHash'
    ], this);

    // Mobile Safari doesn't allow updating the hash more than 100 times per 30 seconds.
    this._updateHash = throttle(this._updateHashUnthrottled.bind(this), 30 * 1000 / 100);
};

/*
 * Map element to listen for coordinate changes
 *
 * @param {Object} map
 * @returns {Hash} `this`
 */
Hash.prototype.addTo = function addTo(map) {
    this._map = map;
    window.addEventListener('hashchange', this._onHashChange, false);
    this._map.on('moveend', this._updateHash);
    return this;
};

/*
 * Removes hash
 *
 * @returns {Popup} `this`
 */
Hash.prototype.remove = function remove() {
    window.removeEventListener('hashchange', this._onHashChange, false);
    this._map.off('moveend', this._updateHash);
    clearTimeout(this._updateHash());

    delete this._map;
    return this;
};

Hash.prototype.getHashString = function getHashString(mapFeedback) {
    var center = this._map.getCenter(),
        zoom = Math.round(this._map.getZoom() * 100) / 100,
        // derived from equation: 512px * 2^z / 360 / 10^d < 0.5px
        precision = Math.ceil((zoom * Math.LN2 + Math.log(512 / 360 / 0.5)) / Math.LN10),
        m = Math.pow(10, precision),
        lng = Math.round(center.lng * m) / m,
        lat = Math.round(center.lat * m) / m,
        bearing = this._map.getBearing(),
        pitch = this._map.getPitch();
    var hash = '';
    if (mapFeedback) {
        // new map feedback site has some constraints that don't allow
        // us to use the same hash format as we do for the Map hash option.
        hash += "#/" + lng + "/" + lat + "/" + zoom;
    } else {
        hash += "#" + zoom + "/" + lat + "/" + lng;
    }

    if (bearing || pitch) {
        hash += (("/" + (Math.round(bearing * 10) / 10)));
    }
    if (pitch) {
        hash += (("/" + (Math.round(pitch))));
    }
    return hash;
};

Hash.prototype._onHashChange = function _onHashChange() {
    var loc = window.location.hash.replace('#', '').split('/');
    if (loc.length >= 3) {
        this._map.jumpTo({
            center: [+loc[2], +loc[1]],
            zoom: +loc[0],
            bearing: +(loc[3] || 0),
            pitch: +(loc[4] || 0)
        });
        return true;
    }
    return false;
};

Hash.prototype._updateHashUnthrottled = function _updateHashUnthrottled() {
    var hash = this.getHashString();
    window.history.replaceState(window.history.state, '', hash);
};


function getTours(bounds) {

    let bbox = ([bounds._sw.lat, bounds._sw.lng, bounds._ne.lat, bounds._ne.lng]).join(',');
    let url = serverHost + '/mtg/objects/search?languages='+config.getLang()+'&version=1.8&except=city,country,publisher,locations,route&type=tour&sort_by=rating:desc';
    url += "&bbox=" + bbox;
    return fetch(url)
        .then(response => response.json())
        .then(tours => {
            let tourList = [];
            for (let k = 0; k < tours.length; ++k) {
                let tour = tours[k];
                let image = '';
                if (tour.images && tour.images[0] && tour.images[0].type == 'story') {
                    image = `https://media.izi.travel/${tour.content_provider.uuid}/${tour.images[0].uuid}_120x90.jpg`;
                }
                tourList.push({
                    id: tours[k].uuid,
                    title: tours[k].title,
                    playback_type: tours[k].playback_type,
                    category: tours[k].category,
                    duration: tours[k].duration,
                    distance: tours[k].distance,
                    placement: tours[k].placement,
                    image: image
                });
            }
            return tourList;
        })
        .catch(error => console.error(error))
}

function getTour(uid) {
    let url = serverHost + "/mtgobjects/" + uid + "?languages="+config.getLang()+"&version=1.8&except=city,country,publisher,news";
    return fetch(url)
        .then(response => response.json())
        .then(json => {
            let data = json[0];
            let title = data.content[0].title;
            let titleTranslit = cyrillicToTranslit({
                preset: "izi"
            }).transform(title.toLowerCase(), '-').replace(/[-]+/g, '-');
            let tour = {
                url: 'https://izi.travel/'+config.getLang()+'/' + data.uuid.substring(0, 4) + '-' + titleTranslit + '/'+config.getLang(),
                title: title,
                playback_type: data.playback_type,
                placement: data.placement,
                category: data.category,
                bounds: data.map.bounds,
                reviews: data.reviews,
                geojson: {
                    type: 'FeatureCollection',
                    features: []
                }
            };
            if (data.content[0].images) {
                tour.image = `https://media.izi.travel/${data.content_provider.uuid}/${data.content[0].images[0].uuid}_480x360.jpg`;
            }
            if (data.content[0].desc) {
                tour.description = data.content[0].desc;
            }
            if (tour.playback_type == 'random') {
                tour.layer_name = LOC.txt.tours.tour;
            } else if (tour.playback_type == 'quest') {
                tour.layer_name = LOC.txt.tours.quest;
            } else {
                tour.layer_name = tour.placement + ' ' + tour.category + ' ' + tour.playback_type;
            }

            if (data.map.route) {
                let route = {
                    type: "Feature",
                    properties: {
                        'route-index': 0,
                        'route': 'selected'
                    },
                    geometry: {
                        type: 'LineString',
                        coordinates: []
                    }
                };

                let points = data.map.route.split(';');
                for (let n = 0; n < points.length; ++n) {
                    let coords = points[n].split(',');
                    route.geometry.coordinates.push([parseFloat(coords[1]), parseFloat(coords[0])]);
                }

                tour.geojson.features.push(route);
            } else if (data.map.bounds) {
                let coords = data.map.bounds.split(',');
                tour.geojson.features.push({
                    type: "Feature",
                    properties: {
                        'playback_type': data.playback_type
                    },
                    geometry: {
                        type: 'Polygon',
                        coordinates: [
                            [
                                [
                                    parseFloat(coords[1]),
                                    parseFloat(coords[0])
                                ],
                                [
                                    parseFloat(coords[1]),
                                    parseFloat(coords[2])
                                ],
                                [
                                    parseFloat(coords[3]),
                                    parseFloat(coords[2])
                                ],
                                [
                                    parseFloat(coords[3]),
                                    parseFloat(coords[0])
                                ],
                                [
                                    parseFloat(coords[1]),
                                    parseFloat(coords[0])
                                ]
                            ]
                        ]
                    }
                });
            }


            let childrens = {};
            let k = 1;
            for (let n = 0; n < data.content[0].children.length; ++n) {
                let children = data.content[0].children[n];
                if (children.type == 'tourist_attraction' && children.location) {
                    childrens[children.uuid] = {
                        type: 'Feature',
                        geometry: {
                            type: "Point",
                            coordinates: [children.location.longitude, children.location.latitude]
                        },
                        properties: {
                            id: 'izi_waypoint',
                            name: '' + (k++),
                            title: children.title
                        }
                    };
                }
            }

            k = 1;
            if (data.content[0].playback.type == 'random') {
                for (let n = 0; n < data.content[0].playback.order.length; ++n) {
                    let child = childrens[data.content[0].playback.order[n]];
                    if (child) {
                        child.properties.name = '' + (k++);
                        tour.geojson.features.push(child);
                    }
                }
            } else {
                for (let property in childrens) {
                    tour.geojson.features.push(childrens[property]);
                }
            }
            return tour;
        })
        .catch(error => console.error(error))
}

export {
    getOSMP,
    getTours,
    getTour,
    getShortFeature,
    getPreviewFeatures,
    getADDR
};
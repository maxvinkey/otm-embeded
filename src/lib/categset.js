exports.CategSet = function () {

    var dictionary = {
        table: {},
        nElements: 0,

        clear: function () {
            this.table = {};
            this.nElements = 0;
        },

        size: function () {
            return this.nElements;
        },

        isEmpty: function () {
            return this.nElements <= 0;
        },
        toString: function (){
            var str = '',
                name;
            for (name in this.table) {
                if (this.table.hasOwnProperty(name)) {
                    str += name + ",";
                }
            }
            return str.length == 0 ? '' : str.substring(0, str.length-1);
        },
        fromTable: function (val) {
            var cat;
             for (cat in  val) {
                if (this.table[cat] === undefined) {
                    this.nElements += 1;
                }
                this.table[cat] = true;
            }
        },
        fromString: function (str) {
            var arr = [];
            if( Object.prototype.toString.call( str ) === '[object Array]' ) {
                arr = str;
            }
            else{
                arr = str.split(',');
            }
            for (var i = 0; i < arr.length; i++) {
                var key = arr[i].toString().trim();
                if (key == '')
                    continue;
                if (this.table[key] === undefined) {
                    this.nElements += 1;
                }
                this.table[key] = true;
            }
        },
        fromStats: function (stats) {
            for (var prop in stats) {
                this.table[prop] = true;
                this.nElements++;
            }
        },
        getIntersection : function (otherSet, filter) {
            var array = [];
            for (var prop in this.table) {
                if (otherSet.contains(prop) && filter[prop] !== undefined) {
                    array.push(prop);
                }
            }
            return array;
        },

        remove : function (key) {
            if (this.table[key] !== undefined) {
                delete this.table[key];
                this.nElements -= 1;
            }
        },

        add: function (key) {
            if (key === undefined)
                return;
            key = '' + key;
            if (this.table[key] === undefined) {
                this.nElements += 1;
            }
            this.table[key] = true;
        },

        contains: function (element) {
            if (this.table[element] === undefined)
                return false;
            return true;
        },

        smartContains: function (element) {
            if (this.table[element] === undefined) {
                for (var prop in this.table) {
                    if (isCategorsParent(+prop, +element)) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        },

        haveIntersection: function (otherSet) {
            if (this.size() < otherSet.size()) {
                for (var prop in this.table) {
                    if (otherSet.contains(prop)) {
                        return true;
                    }
                }
            } else {
                for (var otherProp in otherSet.table) {
                    if (this.contains(otherProp)) {
                        return true;
                    }
                }
            }
            return false;
        },

        toArray: function () {
            var array = [],
                name;
            for (name in this.table) {
                if (this.table.hasOwnProperty(name)) {
                    array.push(name >> 0);
                }
            }
            return array;
        }
    };

    return dictionary;
};
import {cardfilter} from "../cardfilter";
import mapboxgl from 'mapbox-gl';
import {
    config
} from "../config";

function kindAero2poi(kind) {
    switch (kind) {
        case 'international':
            return 'international_airport';
        case 'other':
        case 'airport':
        case 'aerodrome':
            return 'airport';
    }
    return 'airport';
};

function kind2poi(kind){
    switch (kind) {
        case 'other'://aerodrome
            return 'other';
        case 'international'://aerodrome
            return 'international';
        case 'airport':
        case 'aerodrome':
            return 'airport';
        case 'railway':
        case 'station':
        case 'train_station':
            return 'train_station';
        case 'subway':
            return 'subway_entrance';
        case 'subway_entrance':
        case 'tram_stop':
        case 'post_office':
        case 'police':
        case 'information':
        case 'pharmacy':
        case 'shower':
        case 'toilets':
        case 'parking':
        case 'city':
        case 'town':
        case 'village':
        case 'hamlet':
        case 'locality':
            return kind;
        case 'bus_stop':
        case 'bus_station':
            return 'bus_stop';
        case 'marina':
        case 'harbor':
        case 'harbor-marina':
        case 'harbor_marina':
        case 'dock':
        case 'mooring':
            return 'harbor_marina';
        case 'hospital':
        case 'doctors':
        case 'clinic':
            return 'hospital';
        case 'view_point':
        case 'viewpoint':
        case 'view-point':
        case 'vista':
            return 'view_point';
    }
    return "";
};

function getLocalizedFeatureName(prop){
    var lang = config.getLang();
    var name = "";
    var name_en = prop.name_en ? prop.name_en : prop['name:en'];
    var name_ru = prop.name_ru ? prop.name_ru : prop['name:ru'];
    if (lang == 'en'){
        name = name_en ? name_en : prop.name;
    }
    else {
        name = name_ru ? name_ru : (name_en ? name_en : prop.name);
    }
    return name;
};

function isStandartPOI(feature){
    return feature.source_name == 'otmpoi' && feature.source_layer == 'poi';
};

function isVectorPOI(feature){
    return (feature.source_name == 'hotel')
        || (feature.source_name == 'otmpoi' && (feature.source_layer == 'pois' || feature.source_layer == 'vpois'));
};

function isVectorCluster(feature){
    return  (feature.source_name == 'clusters' && feature.source_layer == 'cluster');
}

function _isTypeOf(ftype, feature){
    var id = LOC.mega_names[ftype];
    if (feature.properties.catset) {
        return feature.properties.catset.contains(id);
    }else{
        var n = id.length - 1;
        for (; n > 0; --n){
          if (id.charAt(n) != '0')
              break;
        }
        id = id.slice(0, -(id.length-1-n));
        return feature.properties.mainkind.startsWith(id);
    }
}


function getBaseType(feature){
    if (isStandartPOI(feature) || isVectorPOI(feature)){
        if (_isTypeOf('accomodations', feature)){
            return 'accomodations';
        }
        if (_isTypeOf('winter_sports', feature)){
            return 'winter_sports';
        }
        if (_isTypeOf('sport', feature)){
            return 'sport';
        }
        if (_isTypeOf('amusements', feature)){
            return 'amusements';
        }
        if (_isTypeOf('adult', feature)){
            return 'adult';
        }
        if (_isTypeOf('foods', feature)){
            return 'foods';
        }
        if (_isTypeOf('shops', feature)){
            return 'shops';
        }
        if (_isTypeOf('interesting_places', feature)){
            return 'interesting_places';
        }
        if (_isTypeOf('tourist_facilities', feature)){
            return 'tourist_facilities';
        }
    }
    else {
        return feature.source_layer;
    }
    return 'unknown';
}

function isTypeOf(ftype, feature){
    if (isStandartPOI(feature) || isVectorPOI(feature)){
        if (!feature.properties || !feature.properties.catset || !feature.properties.catset.contains){
            console.dir(feature);
        }
        return _isTypeOf(ftype, feature);
    }
    return false;
}

function is_touch_device() {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function(query) {
        return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

function getFeatureLayerName(feature){
    var prop = feature.properties;
    var layer_name = feature.source_layer;
    if (isStandartPOI(feature)){
        if (LOC.layer_names.items[prop.kind[0]] === undefined){
            console.dir(feature);
        }
        layer_name = parsePlural(LOC.layer_names.items[prop.kind[0]].n).single;
        if (prop.kind.length > 1){
            layer_name += " (";
            for (var i = 1; i < prop.kind.length; ++i){
                layer_name += parsePlural(LOC.layer_names.items[prop.kind[i]].n).single;
                if (i != prop.kind.length - 1){
                    layer_name += ", ";
                }
            }
            layer_name += ")";
        }
        return layer_name;
    }else if (isVectorPOI(feature)){
        if (LOC.layer_names.items[prop.mainkind] === undefined){
            console.dir(feature);
        }
        layer_name = parsePlural(LOC.layer_names.items[prop.mainkind].n).single;
        return layer_name;
    }else if (isVectorCluster(feature)){
        layer_name = LOC.txt.region;
        return layer_name;
    }

    var layer_src = LOC.base_layer_names[layer_name];
    if (layer_src){
        var kind = kind2poi(prop.kind);
        if (layer_name == 'aerodrome_label'){
            kind = kindAero2poi(prop.kind);
        }
        if (kind != "")
            layer_name = parsePlural(layer_src[kind].n).single;
    }
    return layer_name;
}

function getWebsites(prop){
    let urls = [];
    if (prop.website && prop.website.length > 0)
        urls.push(prop.website);
    if (prop.url && prop.url.length > 0)
        urls.push(prop.url);
    if (prop.contact_website && prop.contact_website.length > 0)
        urls.push(prop.contact_website);
    if (prop.extended && prop.extended.web)
        urls.push(prop.extended.web);
    if (prop.extended && prop.extended.url && !prop.extended.url.startsWith('http://www.culture.ru'))
        urls.push(prop.extended.url);

    let osm_urls = [];
    for (let n = 0; n < urls.length; ++n){
        let splited =  urls[n].split(/(http|https):\/\//);
        for (let i = 0; i < splited.length;++i){
            if (splited[i] == "http" || splited[i] == "https")
                continue;
            splited[i] = splited[i].trim();
            if (splited[i].length < 2)
                continue;
            let last =  splited[i].charAt(splited[i].length - 1);
            if (last == ';' || last == '/'){
                splited[i] = splited[i].substring(0, splited[i].length - 1);
            }
            osm_urls.push(splited[i]);
        }
    }
    return osm_urls;
}

function superTrim(str) {
    return str.replace(/^[_\s\uFEFF\xA0]+|[_\s\uFEFF\xA0]+$/g, '');
};


function getFeatureLonLat(feature){
    if (feature.properties.hasOwnProperty('lat') && feature.properties.hasOwnProperty('lon'))
        return new mapboxgl.LngLat(feature.properties.lon, feature.properties.lat);
    else
        return new mapboxgl.LngLat(feature.geometry.coordinates[0], feature.geometry.coordinates[1]);
}

function getFeatureID(feature){
    var id = feature.properties.__id__ ? feature.properties.__id__ : feature.properties.id;
    if (id === undefined)
        id = feature.id;
    return id;
}

function containsStr(arr, obj) {
    var i = arr.length;
    while (i--) {
        if (arr[i] == obj) {
            return true;
        }
    }
    return false;
}

function removeIfContainsStr(arr, obj) {
    for(var i = arr.length - 1; i >= 0; i--) {
        if(arr[i] == obj) {
            arr.splice(i, 1);
        }
    }
}

function getOSMTime(workingSchedule){
    function toTime(unix_timestamp){
        function div(val, by){
            return (val - val % by) / by;
        }
        var sec = unix_timestamp / 1000;
        var minutes =  sec / 60;
        var hours = div(minutes,60);
        minutes = '0' + minutes % 60;
        return hours + ':' + minutes.substr(0, 2);
    };
    var days = ['Mo','Tu','We','Th','Fr','Sa','Su'];
    var res = '';
    var prev = '';
    var first = '';
    var last = '';
    var val = '';
    for (var i = 0; i < 7; ++i){
        if (workingSchedule[i] === undefined){
            workingSchedule[i] = {"from":-1, "to":-1};
        }
    }
    for (var day in  workingSchedule){
        val = workingSchedule[day].from != -1 ? toTime(workingSchedule[day].from) + "-" + toTime(workingSchedule[day].to) : 'off';
        if (val == prev){
            last = days[day];
        }else{
            if (prev != '' && last !=''){
                res += first +'-'+ last + ' ' + prev +';';
                last = '';
            }else if (first != ''){
                res += first + ' ' + prev + ';';
            }
            first = days[day];
        }
        prev = val;
    };
    if (prev != '' && last !=''){
        res += first +'-'+ last + ' ' + prev +';';
    }
    return res;
};

function intPaddingZero(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}

function updateFeatureData(feature_data){
        var name_local = getLocalizedFeatureName(feature_data.feature.properties);
        feature_data.name =  feature_data.feature.properties.name ? feature_data.feature.properties.name : name_local;
        if (name_local != feature_data.name) {
            feature_data.name_local = name_local;
        }
        if (!feature_data.name){
            if (feature_data.layer_name){
                feature_data.name = feature_data.layer_name;
            }
            else if (feature_data.feature.properties.lat)
                feature_data.name = feature_data.feature.properties.lat.toFixed(3) +", " + feature_data.feature.properties.lon.toFixed(3);
            else
                feature_data.name = feature_data.latlng.lat.toFixed(3) +", " + feature_data.latlng.lng.toFixed(3);
        }
}

function getFeatureData(feature, zoom, latlng){
    var feature_data=  {
        src : feature.source_name,
        layer: feature.source_layer,
        zoom: zoom,
        latlng: latlng,
        layer_name: getFeatureLayerName(feature),
        id: getFeatureID(feature),
        feature: feature,
        basetype: getBaseType(feature),
        mainkind : feature.properties.mainkind,
        xid : feature.properties && feature.properties.xid ? feature.properties.xid : null
    };
    updateFeatureData(feature_data);
    return feature_data;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function parsePlural(label){
        var sb_single = new Array("");
        var sb_plural = new Array("");
        var spaces = 0;
        for (var i = 0; i < label.length; i++) {
            var ch = label.charAt(i);
            if (ch != '[') {
                sb_single.push(ch);
                sb_plural.push(ch);
                if (ch == ' '){
                    spaces++;
                }
                else {
                    spaces = 0;
                }
            } else {
                var sb = new Array("");
                var j = i + 1;
                for (; j < label.length; j++) {
                    ch = label.charAt(j);
                    if (ch == ']')
                        break;
                    sb.push(ch);
                }
                var len = j - i - 1;
                i = sb_plural.length;
                sb_plural.splice(Math.max(0, i - len), len);
                sb_plural.push(sb.join(""));
                if (spaces != 0){
                    i = sb_single.length;
                    sb_single.splice(Math.max(0, i - spaces), spaces);
                }
                i = j;
            }
        }
        return {
            single : capitalizeFirstLetter(sb_single.join("")),
            plural : sb_plural.join("")
        };
}

function throttle (func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options) options = {};
    var later = function () {
        previous = options.leading === false ? 0 : new Date().getTime();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
    };
    return function () {
        var now = new Date().getTime();
        if (!previous && options.leading === false) previous = now;
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };
}

function wiki2Object(wikipedia){
    if (!wikipedia)
        return null;
    if (wikipedia.toString() === '[object Object]')
        return wikipedia;
    return {
        lang : wikipedia.substring(0, 2),
        title : wikipedia.substring(3)
    }
}
/*
 https://ru.wikipedia.org/w/api.php?action=query&format=json&utf8=1&pithumbsize=200&prop=pageimages%7Cextracts&titles=%D0%9C%D1%83%D0%B7%D0%B5%D0%B9%20%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85%20%D0%BA%D0%BE%D0%BB%D0%BB%D0%B5%D0%BA%D1%86%D0%B8%D0%B9&exsentences=1
 */
function extractContent(html) {
    return (new DOMParser).parseFromString(html, "text/html").documentElement.textContent;
}

function removeRusHeritage(inner){
    if (inner.startsWith('&nbsp;<span title="Памятник') ||
        inner.startsWith('&nbsp;<span title="Объект культурного наследия')
    ) {
        inner = inner.substr(inner.indexOf('</span>') + 7);
        if (inner.length < 3){
            return "";
        }
        inner = removeRusHeritage(inner);
    }
    return inner;
}

function extractWiki(page) {
    var el = document.createElement('html');
    el.innerHTML = page.extract;
    var ps = el.getElementsByTagName('p');
    if (ps) {
        var len = 0;
        page.extract = '';
        for (var j = 0; j < ps.length; ++j) {
            var inner = ps[j].innerHTML.trim();
            if (inner == '' || extractContent(inner) == '')
                continue;
            if (inner.startsWith('<span>f0</span>'))
                continue;
            var inner = removeRusHeritage(inner);
            if (inner == ''){
                continue;
            }
            if (len + ps[j].textContent.length > 700){
                if (len == 0){
                    page.extract = '<p>'+inner+'</p>';
                }
                break;
            }
            page.extract += '<p>'+inner+'</p>';
            len += ps[j].textContent.length;
        }
    }
    return page.extract;
}

function parseWikipediaResponse(data, pageid, feature, callback){
    if (data.query && data.query.pages) {
        var page = null;
        if (!pageid){
            for (pageid in data.query.pages) {
                page = data.query.pages[pageid];
                if (page)
                    break;
            }
        }else
            page = data.query.pages[pageid];

        if (!page)
            return

        if (page.pageimage && (page.pageimage.indexOf("location_map") != -1)){
            page.pageimage = null;
            page.thumbnail = null;
        }

        page.extract = page.extract ? extractWiki(page) : null;
        if (page.thumbnail && page.imageinfo && page.imageinfo[0] && page.imageinfo[0].extmetadata){
            if (page.imageinfo[0].extmetadata.ImageDescription) {
                page.thumbnail.descr = extractContent(page.imageinfo[0].extmetadata.ImageDescription.value);
            }

            if (page.imageinfo[0].extmetadata.Attribution) {
                page.thumbnail.attribution = extractContent(page.imageinfo[0].extmetadata.Attribution.value);
            }
            if (page.imageinfo[0].extmetadata.Artist) {
                page.thumbnail.author = extractContent(page.imageinfo[0].extmetadata.Artist.value);
            }
            if (page.imageinfo[0].extmetadata.DateTimeOriginal) {
                page.thumbnail.date = page.imageinfo[0].extmetadata.DateTimeOriginal.value;
            }
            if (!page.thumbnail.descr) {
                page.thumbnail.descr = page.title;
            }
        }
        callback(feature, page.thumbnail, page.extract, page.pageimage);
    }
}
//https://pt.wikipedia.org/w/api.php?&action=query&format=json&utf8=1&pithumbsize=400&prop=pageimages|extracts&exintro=1&redirects&piprop=thumbnail|name|original&titles=Casa%20Grande%20(Braga)
//https://commons.wikimedia.org/w/api.php?action=query&format=json&utf8=1&pithumbsize=400&prop=pageimages|imageinfo&iiprop=extmetadata&redirects&titles=File%3AVillaZeno%202007%2007%2012%202.jpg
function getWikiExtractByTitle(thumbsize, wiki_lang, title, feature, extractText, callback){
    //console.log("extract :" + wiki_lang);
    var wikiAPI = "//"+wiki_lang+".wikipedia.org/w/api.php?action=query&format=json&utf8=1" +
        "&piprop=thumbnail|name"+
        "&pithumbsize="+thumbsize+"&iiprop=extmetadata" +
        "&prop=pageimages%7Cimageinfo";
    if (extractText){
        wikiAPI += "%7Cextracts&exintro=1";
    }
    wikiAPI += "&redirects&titles=" + encodeURIComponent(title);
    $.ajax({
        url: wikiAPI,
        jsonp: "callback",
        dataType: "jsonp",
        success: function( data ) {
          parseWikipediaResponse(data, null, feature, callback);
        },
        complete : function() {
            //callback();
        }
    });
}

function getWikiExtract(thumbsize, wiki_lang, pageid, feature, callback){
    var wikiAPI = "//"+wiki_lang+".wikipedia.org/w/api.php?action=query&format=json&utf8=1" +
        "&pithumbsize="+thumbsize+
        "&prop=pageimages%7Cextracts%7Cimageinfo" +
        "&piprop=thumbnail|name"+
        "&iiprop=extmetadata&exintro=1&redirects&pageids=";
    wikiAPI += pageid;
    $.ajax({
        url: wikiAPI,
        jsonp: "callback",
        dataType: "jsonp",
        success: function( data ) {
            parseWikipediaResponse(data, pageid, feature, callback);
        },
        complete : function() {
            //callback();
        }
    });
}

function getExtendedProperties(feature, callback){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState !== 4){
            return;
        }
        if (xmlHttp.status !== 200 && xmlHttp.status !== 304){
            callback(feature);
            return;
        }
        feature.properties.extended = JSON.parse(xmlHttp.response);
        callback(feature);
    };
    var url = document.location.origin + '/feature/'+config.getOTMLang()+'/'+feature.id+'?ext=1';
    xmlHttp.open('GET', url, true);
    xmlHttp.setRequestHeader('Accept', 'application/json');
    xmlHttp.send(null);
}

function makeUserAction(action, callback){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState !== 4){
            return;
        }
        if (xmlHttp.status !== 200 && xmlHttp.status !== 304){
            callback({});
            return;
        }
        callback(JSON.parse(xmlHttp.response));
    };
    xmlHttp.open('GET', "/user?action=" + action, true);
    xmlHttp.setRequestHeader('Accept', 'application/json');
    xmlHttp.send(null);
}

function postJSON(path, data, callback){
    var xhr = new XMLHttpRequest();
    xhr.onloadend = function () {
        if (xhr.readyState !== 4){
            return;
        }
        if (callback)
            callback(xhr.status == 200 || xhr.status == 304);
    };
    xhr.open('POST', path, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.send(JSON.stringify(data));
}

function isSimilar(one, title) {
    if (one == title)
        return true;
    var aone = one.split(/[-\",;\s]+/);
    var atwo = title.split(/[-\",;\s]+/);
    var cnt = 0;
    for (var i = 0; i < aone.length; ++i){
        for (var j = 0; j < atwo.length; ++j){
            if (aone[i] == atwo[j])
                cnt++;
        }
    }
    return (cnt > 2) || (cnt == aone.length) || (cnt == atwo.length);
};

function isFoundWiki(feature, title, wiki_lang){
    if (feature.properties.name) {
        if (isSimilar(feature.properties.name.toLowerCase(), title))
            return true;
    }
    if (wiki_lang == 'ru'){
        var name_ru = feature.properties.name_ru ? feature.properties.name_ru : feature.properties['name:ru'];
        if (name_ru && isSimilar(name_ru.toLowerCase(), title)){
            return true;
        }
    }else if (wiki_lang == 'en'){
        var name_en = feature.properties.name_en ? feature.properties.name_en : feature.properties['name:en'];
        if (name_en && isSimilar(name_en.toLowerCase(), title)){
            return true;
        }
    }
    if (isTypeOf('view_points', feature))
    {
        if (wiki_lang == 'ru'){
            return (title.indexOf('мост') != -1);
        }else if (wiki_lang == 'en') {
            return (title.indexOf('bridge') != -1);
        }
    }
    return false;
};

function loadOSMProps(xml) {
    var osm = {};
    for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        if (item.nodeType == 1) { // element
            if (item.attributes.length > 1) {
                    var name = item.attributes.item(0).nodeValue;
                    var val = item.attributes.item(1).nodeValue;
                    name = name.replace(":","_");
                    if (name == "contact_phone")
                        name = "phone";
                    else if (name == "contact_website" || name == "website")
                        name = "url";
                    osm[name] = val;
            }
        }
    }
    return osm;
};

function getOSMIdentificator(feature) {

    let osm_type = 'node';
    let prop = feature.properties;
    let id = feature.id;
    let osm_id = prop.osm_id;

    if (prop.xid === "POI") {
        if (prop.osm_id) {
            osm_id = prop.osm_id;
            osm_type = 'way';
            if (osm_id < 0) {
                osm_type = 'relation';
                osm_id = Math.abs(osm_id);
            }
        }
        else {
            let lastDigit = id % 10;
            osm_id = (id - lastDigit) / 10;
            switch (lastDigit) {
                case 0:
                    osm_type = 'node';
                    break;
                case 1:
                case 2:
                    osm_type = 'way';
                    break;
                case 3:
                case 4:
                    osm_type = 'relation';
                    break;
            }
        }
    }
    else if (typeof prop.ispt !== 'undefined') {
            if (prop.ispt) {
                osm_type = 'node'
            } else if (osm_id < 0) {
                osm_type = 'relation';
                osm_id = Math.abs(osm_id);
            } else {
                osm_type = 'way'
            }
     }
     return osm_id === undefined ? undefined : {osm_type: osm_type, osm_id: osm_id};
};

function getSnackValue(snak){
    if (!snak.mainsnak)
        return "";
    return snak.mainsnak.datavalue.value;
};

function getStringSnackValue(claims, tag){
    if (claims[tag] && claims[tag].length > 0){
        for (var i = 0; i < claims[tag].length; i++) {
            var valSnack =  getSnackValue(claims[tag][i]);
            if (valSnack == "") {
                continue;
            }
            return valSnack;
        }
    }
    return null;
};

function getWikiDataDescription(lang, descriptions){
    if (descriptions === undefined || descriptions == null)
        return undefined;
    if (descriptions[lang]){
        return descriptions[lang].value;
    }else if (descriptions['en']){
        return descriptions['en'].value;
    }else{
        for (var prop in descriptions) {
            if (descriptions[prop].value && descriptions[prop].value != '') {
                return descriptions[prop].value;
            }
        }
    }
    return undefined;
};

function getWikiData(lang, feature, callback){
    var wikiAPI = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids=Q"+feature.properties.wikidata;
    //var wikiAPI = "https://www.wikidata.org/wiki/Special:EntityData/Q"+feature.properties.wikidata+".json";
    $.ajax({
        url: wikiAPI,
        jsonp: "callback",
        dataType: "jsonp",
        success: function( data ) {
            if (data.entities && data.entities["Q"+feature.properties.wikidata]) {
                var wikidata = data.entities["Q"+feature.properties.wikidata];
                var useful_wikidata = {
                    descr : getWikiDataDescription(lang,  wikidata.descriptions)
                };
                if (wikidata.claims){
                    var urlSnack =  getStringSnackValue(wikidata.claims, "P856");
                    if (urlSnack) {
                        useful_wikidata.url = urlSnack;
                    }
                    var imageSnack =  getStringSnackValue(wikidata.claims, "P18");
                    if (imageSnack) {
                        if (imageSnack.indexOf("location_map") == -1){
                            useful_wikidata.image = imageSnack
                        }
                    }
                    var inceptionSnack =  getStringSnackValue(wikidata.claims, "P571");
                    if (inceptionSnack) {
                        useful_wikidata.start_date = wdk.wikidataTimeToSimpleDay(inceptionSnack);
                    }
                }
                for (var prop in useful_wikidata) {
                    if (useful_wikidata[prop]) {
                        feature.wikidata = useful_wikidata;
                        break;
                    }
                }
            }
        },
        complete : function() {
            callback(feature);
        }
    });
};

function getAllowedFields(child){
    var res = cardfilter.getOTMFilter('000000000');
    for (var n = 1; n <=9; n +=2){
        var full = cardfilter.getOTMFilter(child.substr(0, n)+Array(10 - n).join("0"));
        if (full !== undefined){
            res = res.concat(full);
        }
    }
    return res;
};

function getAllAllowedFields(feature){
    var res = [];
    if (feature.source_name == 'otmpoi'){
        for (var n = 0; n < feature.properties.kind.length; ++n){
            var allowed = getAllowedFields(feature.properties.kind[n]);
            res = res.concat(allowed);
        }
    }
    else
    {
        var kind = feature.properties.kind;
        if (feature.source_layer == 'aerodrome_label'){
            kind = kindAero2poi(feature.properties.kind);
        }else
            kind = kind2poi(feature.properties.kind);
        res = res.concat(cardfilter[feature.source_name].all);
        if (cardfilter[feature.source_name][kind]) {
            res = res.concat(cardfilter[feature.source_name][kind]);
        }
    }
    return res;

};

function getFeatureByID(id, callback) {

    if (init_feature.source_layer != 'center') {
        if (id == init_feature.properties.xid || id == init_feature.id) {
            if (!init_feature.properties.catset) {
                init_feature.properties.catset = new CategSet();
                init_feature.properties.catset.fromString(init_feature.properties.kind);
            }
            init_feature.properties.kind = init_feature.properties.catset.getIntersection(init_feature.properties.catset, LOC.layer_names.items);
            callback(init_feature);
            return;
        }
    }

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState !== 4){
            return;
        }
        if (xmlHttp.status !== 200 && xmlHttp.status !== 304){
            callback('');
            return;
        }

        var c = JSON.parse(xmlHttp.response);
        var all_nodes = window.tree.getCheckedNodes(LOC.layer_names.groups);
        c.properties.catset = new CategSet();
        c.properties.catset.fromString(c.properties.kind);
        var allkinds = all_nodes.getIntersection(c.properties.catset, LOC.layer_names.items);
        c.properties.mainkind = allkinds.length > 0 ? allkinds[0] :  c.properties.kind[0];
        c.properties.kind = c.properties.catset.getIntersection(c.properties.catset, LOC.layer_names.items);
        callback(c);
    };
    var url = document.location.origin + '/feature/'+config.getOTMLang()+'/'+id;
    xmlHttp.open('GET', url, true);
    xmlHttp.setRequestHeader('Accept', 'application/json');
    xmlHttp.send(null);
};

function getClusterByID(id, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState !== 4){
            return;
        }
        if (xmlHttp.status !== 200 && xmlHttp.status !== 304){
            callback('');
            return;
        }

        var c = JSON.parse(xmlHttp.response);
        c.source_name = 'clusters';
        c.source_layer = 'clusters';
        c.geometry = {
            type: "Point",
            coordinates: [c.lon, c.lat]
        };
        c.properties.catset = new CategSet();
        c.properties.catset.fromStats(c.stats);
        c.properties.kind = c.properties.catset.toArray();
        c.properties.xid = "REG" + c.properties.id;
        c.type = "Feature";
        callback(c);
    };
    var url = document.location.origin + '/cluster/'+config.getOTMLang()+'?id='+id +"&r="+config.poi_main_filter.value+"&h="+config.poi_main_filter.herit;
    xmlHttp.open('GET', url, true);
    xmlHttp.setRequestHeader('Accept', 'application/json');
    xmlHttp.send(null);
};


function getOSM(feature, callback){
    var osmid = getOSMIdentificator(feature);
    if (!osmid)
        return;
    var osmAPI = "//api.openstreetmap.org/api/0.6/"+osmid.osm_type + "/"+osmid.osm_id;
    //var osmAPI = "http://overpass.osm.rambler.ru/cgi";
    //var request_headers = {};
    //request_headers["X-Requested-With"] = 'opentripmap';

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState !== 4){
            return;
        }
        if (xmlHttp.status !== 200 && xmlHttp.status !== 304){
            callback();
            return;
        }

        var data = xmlHttp.responseXML;
        if (data.hasChildNodes()){
            var allowed_filelds = getAllAllowedFields(feature);
            var itemOSM = data.childNodes.item(0);
            for(var i = 0; i < itemOSM.childNodes.length; i++) {
                var item = itemOSM.childNodes.item(i);
                if (item.id && item.id == osmid.osm_id){
                    var osm = loadOSMProps(item);
                    for (var prop in osm) {
                        if (feature.properties[prop] === undefined) {
                            if (prop == 'website' || ($.inArray( prop,  allowed_filelds) != -1)){
                                feature.properties[prop] = osm[prop];
                                feature.osm = true;
                            }
                        }
                    }
                    break;
                }
            }
        }
        callback();
    };
    xmlHttp.open('GET', osmAPI, true);
    xmlHttp.setRequestHeader('X-Requested-With','opentripmap');
    xmlHttp.setRequestHeader('Accept', 'text/xml');
    xmlHttp.send(null);
};

function encode58(num) {
    var str = "";
    var modulus;
    var alphabet = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
    var base = alphabet.length;

    var alphabetLookup = alphabet.split("").reduce(function(lookup, char, index) {
        lookup[char] = index;
        return lookup;
    }, {});


    num = Number(num);

    while (num >= base) {
        modulus = num % base;
        str = alphabet[modulus] + str;
        num = Math.floor(num / base);
    }
    return alphabet[num] + str;
};

function getFlickrImageUrl(card, photo){
    var FLICKR_API_KEY = 'b262dab358ba1197b0a053d6c7e7de6b';
    var self = card;

    var FLICKR_API_KEY = 'b262dab358ba1197b0a053d6c7e7de6b';
    var searchUrl = "//api.flickr.com/services/rest/?method=flickr.photos.getInfo";
    searchUrl+='&api_key='+FLICKR_API_KEY;
    searchUrl+='&photo_id='+photo.id;
    searchUrl+='&secret='+photo.secret;
    searchUrl+='&format=json';

    $.getJSON(searchUrl+'&jsoncallback=?',
        function(data) {
          console.dir(data);
        }
        /*
            var preview = 'https://farm'+photo.farm+'.staticflickr.com/'+photo.server+'/'+photo.id+'_'+photo.secret+'_z.jpg';
            var href = preview;
            var urls = data.photo.urls.url[0];
            if (urls){
                href = urls._content;
            }
            href =  "https://flic.kr/p/" + encode58(photo.id);
            self.createImageDiv(preview, href);
        }
        */
    );
};

function addFlickrImage(searchLat, searchLon, feature, card){
    var text = '';
    var radius = 0.5;
    if (!isStandartPOI(feature) && !isVectorPOI(feature)){
        return
    }
    if (_isTypeOf('beaches', feature)){
        text = 'beach'
    }else if (_isTypeOf('waterfalls', feature)){
        text = 'waterfall';
        radius = 0.3;
    }else if (_isTypeOf('caves', feature)){
        text = 'cave';
        radius = 0.3;
    }else if (_isTypeOf('canyons', feature)){
        text = 'canyon';
        radius = 0.5;
    }else if (_isTypeOf('volcanoes', feature)){
        text = 'volcano';
        radius = 0.3;
    }else if (_isTypeOf('view_points', feature)){
        radius = 0.15;
    }
    else return;

    var FLICKR_API_KEY = 'b262dab358ba1197b0a053d6c7e7de6b';
    var searchUrl = "//api.flickr.com/services/rest/?method=flickr.photos.search";
    searchUrl+='&api_key='+FLICKR_API_KEY;
    searchUrl+='&text='+text+' -people -model -portrait -monument -pet -dog -fish -boat -macro -feet -flower -surreal -vehicle -artwork -children -indoor -food';
    searchUrl+='&has_geo=true';

    if (Math.abs(feature.properties.lon_max - feature.properties.lon_min) + Math.abs(feature.properties.lat_max - feature.properties.lat_min) > 0.0001) {
        searchUrl+='&bbox='+feature.properties.lon_min+","+feature.properties.lat_min+","+feature.properties.lon_max+","+feature.properties.lat_max;
    }else{
        searchUrl+='&lat='+searchLat;
        searchUrl+='&lon='+searchLon;
        searchUrl+='&radius='+radius;
    }

    searchUrl+='&accuracy=16';
    searchUrl+='&format=json';
    searchUrl+='&safe_search=1';
    searchUrl+='&privacy_filter=1';
    searchUrl+='&extras=date_taken,owner_name,license,geo,tags,machine_tags';
    //searchUrl+='&tags=-people,-groupshot,-food,-portrait,-self-portrait';
    //searchUrl+='&tags=sea,ocean,water,sand,playa,beach,coast,coastline,shore,mar,seaside';
    searchUrl+='&per_page=15';
//landscape, waterfall, wood,hill,landscape, hill, mountainside, mountains, creek, water, foothill,stones,rock
    var self = card;
    $.getJSON(searchUrl+'&jsoncallback=?',
        function(data) {
            if (data.photos.photo.length > 0) {
                getAndMarkPhotos(self, data.photos);
            }
        }
    );
    var tagset = new CategSet();
    tagset.fromString("groupshot,self-portrait,child,bikini,car,family");

    function getAndMarkPhotos(self, photos) {
        var numPhotos = photos.photo.length;
        for(var i=0; i<numPhotos; i++) {
            var photo = photos.photo[i];
            //getFlickrImageUrl(self, photo);
            /*
           if (!photo.tags || photo.tags =='')
               continue;*/
            var tags = photo.tags.split(' ');
            var catset = new CategSet();
            catset.fromString(tags);
            if (catset.haveIntersection(tagset))
                continue;
            var preview = 'https://farm'+photo.farm+'.staticflickr.com/'+photo.server+'/'+photo.id+'_'+photo.secret+'_z.jpg';
            //searchUrl+='&tags=-people,-groupshot,-food,-portrait,-self-portrait';
            href =  "https://flic.kr/p/" + encode58(photo.id);
            var title = '';
            if (photo.title && !photo.title.startsWith('DSC') && (photo.title.indexOf("#") == -1)){
                title = photo.title + ' ';
            }
            title += "by "+photo.ownername;
            //console.log(title + ' ' + photo.machine_tags);
            if (photo.datetaken) {
                title += ", ";
                var dt = new Date(photo.datetaken);
                var months = [];
                if (config.getLang() == 'ru'){
                    months  = 'января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря'.split('_')
                    title += dt.getDate() + ' ' + months[dt.getMonth()];
                }else{
                    months  = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
                    title += months[dt.getMonth()] + ' ' + dt.getDate();
                }
                title += ", " + dt.getFullYear();
            }
            self.createImageDiv(title, preview, href);
        }
    }
};


function getWiki(wiki_lang, feature, latlng, callback){
    var wikiAPI = "//"+wiki_lang+".wikipedia.org/w/api.php?action=query&utf8=1&gsradius=300&format=json&list=geosearch&gscoord=";
    wikiAPI += latlng.lat + "|" + latlng.lng;
    $.ajax({
        url: wikiAPI,
        jsonp: "callback",
        dataType: "jsonp",
        success: function( data ) {
            if (data.query && data.query.geosearch) {
                for (var i = 0; i < data.query.geosearch.length; i++) {
                    var title = data.query.geosearch[i].title.toLowerCase();
                    if (!isFoundWiki(feature, title, wiki_lang)) {
                          continue;
                    }
                    feature.properties.wikipedia = {
                        lang : wiki_lang,
                        title : data.query.geosearch[i].title,
                        pageid: data.query.geosearch[i].pageid
                    }
                    return;
                }
            }
        },
        complete : function() {
             callback();
        }
    });
};

function corslite(url, callback, cors) {
    var sent = false;

    if (typeof window.XMLHttpRequest === 'undefined') {
        return callback(Error('Browser not supported'));
    }

    if (typeof cors === 'undefined') {
        var m = url.match(/^\s*https?:\/\/[^\/]*/);
        cors = m && (m[0] !== location.protocol + '//' + location.hostname +
            (location.port ? ':' + location.port : ''));
    }

    var x = new window.XMLHttpRequest();

    function isSuccessful(status) {
        return status >= 200 && status < 300 || status === 304;
    }

    if (cors && !('withCredentials' in x)) {
        // IE8-9
        x = new window.XDomainRequest();

        // Ensure callback is never called synchronously, i.e., before
        // x.send() returns (this has been observed in the wild).
        // See https://github.com/mapbox/mapbox.js/issues/472
        var original = callback;
        callback = function() {
            if (sent) {
                original.apply(this, arguments);
            } else {
                var that = this, args = arguments;
                setTimeout(function() {
                    original.apply(that, args);
                }, 0);
            }
        }
    }

    function loaded() {
        if (
            // XDomainRequest
        x.status === undefined ||
        // modern browsers
        isSuccessful(x.status)) callback.call(x, null, x);
        else callback.call(x, x, null);
    }

    // Both `onreadystatechange` and `onload` can fire. `onreadystatechange`
    // has [been supported for longer](http://stackoverflow.com/a/9181508/229001).
    if ('onload' in x) {
        x.onload = loaded;
    } else {
        x.onreadystatechange = function readystate() {
            if (x.readyState === 4) {
                loaded();
            }
        };
    }

    // Call the callback with the XMLHttpRequest object as an error and prevent
    // it from ever being called again by reassigning it to `noop`
    x.onerror = function error(evt) {
        // XDomainRequest provides no evt parameter
        callback.call(this, evt || true, null);
        callback = function() { };
    };

    // IE9 must have onprogress be set to a unique function.
    x.onprogress = function() { };

    x.ontimeout = function(evt) {
        callback.call(this, evt, null);
        callback = function() { };
    };

    x.onabort = function(evt) {
        callback.call(this, evt, null);
        callback = function() { };
    };

    // GET is the only supported HTTP Verb by XDomainRequest and is the
    // only one supported here.
    x.open('GET', url, true);

    // Send the request. Sending data is not supported.
    x.send(null);
    sent = true;

    return x;
};

function renderPOIListItem(bookmark, options) {
    var li = $('<li/>')
        .addClass('bookmark-item')
        .attr('data-src', bookmark.src)
        .attr('data-id', bookmark.list_id !== undefined ? bookmark.list_id : bookmark.id)
        .click(function(evt){
            var src_id = $(this).attr('data-src');
            var data_id = $(this).attr('data-id');
            options.onShow(src_id, data_id);
        });
    if (options.onHover) {
        li.hover(function () {
            var src_id = $(this).attr('data-src');
            var data_id = $(this).attr('data-id');
            options.onHover(src_id, data_id);
        });
    }
    if (options.onRemove) {
        var eremove = $('<span/>')
            .addClass('bookmark-remove')
            .html("&times;")
            .click(function () {
                var src_id = $(this).parent().attr('data-src');
                var data_id = $(this).parent().attr('data-id');
                options.onRemove(src_id, data_id);
            })
            .appendTo(li);
    }

    var name_prefix = "";
    if (bookmark.feature && bookmark.feature.properties.popular && bookmark.feature.properties.popular > 3){
        name_prefix = '<i class="fa fa-bank"></i>&nbsp;';
    }
    if (bookmark.feature && bookmark.feature.properties.kind && bookmark.feature.properties.kind == 'subway_entrance' &&
        bookmark.feature.properties.colour){
        name_prefix = '<i class="fa fa-circle blue" style="margin-right: 5px; color: '+bookmark.feature.properties.colour+';"></i>&nbsp;';
    }


    if (bookmark.name_local){
        $('<span/>')
            .addClass('bookmark-name')
            .html(name_prefix + bookmark.name_local)
            .appendTo(li);
        $('<span/>')
            .addClass('bookmark-name-locale')
            .text(bookmark.name)
            .appendTo(li);
    }
    else{
        $('<span/>')
            .addClass('bookmark-name')
            .html(name_prefix + bookmark.name)
            .appendTo(li);
    }
    if (bookmark.place) {
        $('<p/>')
            .addClass('bookmark-place')
            .text(bookmark.place)
            .appendTo(li);
    }
    if (options.isDist && bookmark.feature && bookmark.feature.dist){
        $('<p/>')
            .addClass('bookmark-place')
            .text(' <'+bookmark.feature.dist + ' '+LOC.txt.unitNames.minutes)
            .appendTo(li);
    }
    $('<p/>')
        .addClass('bookmark-layer')
        .text(bookmark.layer_name)
        .appendTo(li);

    /*
     var ecoord = $('<textarea/>')
     .attr('name', 'textarea')
     .css('height','50px')
     .appendTo(li);
     */
    return li;
};

function isCategorsParent(parent, n){

    if (parent > n)
        return false;

    function getParentShift(val) {
        var count = (val == 0) ? 1 : 0;
        while (val != 0 && val % 10 == 0) {
            count++;
            val = (val - val % 10) / 10;
        }
        return count;
    }
    function  getCountsOfDigits(val) {
        var count = (val == 0) ? 1 : 0;
        while (val != 0) {
            count++;
            val = (val - val % 10) / 10;
        }
        return count;
    }

    var nd = getCountsOfDigits(n - parent);
    var np = getParentShift(parent);
    return nd -  np < 0;
};

function getMapBoxGLHashString(map) {
    const center = map.getCenter(),
        zoom = Math.round(map.getZoom() * 100) / 100,
        // derived from equation: 512px * 2^z / 360 / 10^d < 0.5px
        precision = Math.ceil((zoom * Math.LN2 + Math.log(512 / 360 / 0.5)) / Math.LN10),
        m = Math.pow(10, precision),
        lng = Math.round(center.lng * m) / m,
        lat = Math.round(center.lat * m) / m,
        bearing = map.getBearing(),
        pitch = map.getPitch();
    let hash = '';
    hash += `#${zoom}/${lat}/${lng}`;
    if (bearing || pitch) hash += (`/${Math.round(bearing * 10) / 10}`);
    if (pitch) hash += (`/${Math.round(pitch)}`);
    return hash;
}

export {getFeatureLayerName,getAllAllowedFields,getWikiDataDescription,getStringSnackValue,extractWiki,
 getOSMIdentificator,intPaddingZero,loadOSMProps, isTypeOf, getWebsites, getMapBoxGLHashString,
    getFeatureByID, getClusterByID, throttle, getFeatureLonLat, getFeatureData, is_touch_device, postJSON};


